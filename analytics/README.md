# Finance Planner Analytics Microservice

Link: (http://financial-planner-analytics.herokuapp.com)

This module handles everything about data analytics. Currently it is being run using Spring Boot Framework.

Main contributor:
1. Muhamad Yoga Mahendra (1806205256)