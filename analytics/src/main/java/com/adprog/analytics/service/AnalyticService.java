package com.adprog.analytics.service;

import java.io.IOException;
import java.net.http.HttpResponse;
import java.util.HashMap;

public interface AnalyticService {
  public HashMap<String, String> getBasicInfo(String auth, long pid)
      throws IOException, InterruptedException;

  public String createGraph(String type, String chartType, long pid, String auth)
      throws IOException, InterruptedException;

  public HttpResponse<String> getProjectHttpResponse(String auth, long pid)
      throws IOException, InterruptedException;
}
