package com.adprog.analytics.service;

import com.adprog.analytics.analyzer.Analyzer;
import com.adprog.analytics.analyzer.factory.CategoryGraphFactory;
import com.adprog.analytics.analyzer.factory.GraphFactory;
import com.adprog.analytics.analyzer.factory.TimesGraphFactory;
import com.adprog.analytics.analyzer.factory.fetcher.DataFetcher;
import com.adprog.analytics.analyzer.factory.fetcher.DataFetcherImpl;
import com.adprog.analytics.analyzer.factory.parser.CategoryParser;
import com.adprog.analytics.analyzer.factory.parser.DataParser;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

@Service
public class AnalyticServiceImpl implements AnalyticService {
  private final Analyzer analyzer = new Analyzer();
  private GraphFactory categoryGraphFactory = new CategoryGraphFactory();
  private GraphFactory timesGraphFactory = new TimesGraphFactory();
  private DataFetcher fetcher = new DataFetcherImpl();
  private DataParser parser = new CategoryParser();
  private static final HttpClient httpClient =
      HttpClient.newBuilder()
          .version(HttpClient.Version.HTTP_1_1)
          .connectTimeout(Duration.ofSeconds(12))
          .build();

  /**
   * Example data:
   *
   * <p>{ // api/projects "1":{ "description": "First project created", "id":1, "href":
   * "/api/projects/1", "title": "My Project" } } { // api/projects/1 "lastUpdated": datetime,
   * "description": "F", "id":1, "title": "My Project" "creationDate": datetime } { //
   * api/projects/1/categories/ "4":{ "name": "Salary", "id":4, "href":
   * "/api/projects/1/categories/4", "type": "I" } "5":{ "name": "Food", "id":5, "href":
   * "/api/projects/1/categories/5", "type": "E" } } { // api/projects/1/transactions "amount":
   * 30000000, "description": "Our first salary", "project": {"id": 1, "href":"/api/projects/1"},
   * "id": 6, "title": "WFH Salary", "transactionDate": "1970-01-01", "category": { "name":
   * "Salary", "id": 4, "href": "/api/projects/1/categories/4", } }
   */
  @Override
  public HashMap<String, String> getBasicInfo(String auth, long pid)
      throws IOException, InterruptedException {
    HttpResponse<String> response = getProjectHttpResponse(auth, pid);
    HashMap<String, String> result = new HashMap<>();
    JSONObject resp = new JSONObject(response.body());
    if (response.statusCode() != 200) {
      result.put("Error", resp.toString());
      return result;
    }
    result.put("Project Title", resp.getString("title"));
    ArrayList<JSONObject> dataC = this.fetcher.getCategoryData(pid, auth);
    ArrayList<JSONObject> dataT = this.fetcher.getTransactionData(pid, auth);
    Map<String, Long> parsedData = this.parser.parse(dataC, dataT);
    result.putAll(analyzer.getBasicInfo(parsedData, dataC));
    return result;
  }

  @Override
  public String createGraph(String type, String chartType, long pid, String auth)
      throws IOException, InterruptedException {
    HttpResponse<String> response = getProjectHttpResponse(auth, pid);
    if (response.statusCode() != 200) return response.body();
    JSONObject jar = new JSONObject(response.body());
    switch (type) {
      case ("categories"):
        return categoryGraphFactory.createGraph(jar.getString("title"), pid, auth, chartType);
      case ("times"):
        return timesGraphFactory.createGraph(jar.getString("title"), pid, auth, chartType);
      default:
        return "Unknown type";
    }
  }

  @Override
  public HttpResponse<String> getProjectHttpResponse(String auth, long pid)
      throws IOException, InterruptedException {
    HttpRequest request =
        HttpRequest.newBuilder()
            .GET()
            .uri(
                URI.create(
                    "http://financial-planner-datastorage.herokuapp.com/api/projects/"
                        + Long.toString(pid)))
            .setHeader("Authorization", auth) // add request header
            .build();
    return httpClient.send(request, HttpResponse.BodyHandlers.ofString());
  }
}
