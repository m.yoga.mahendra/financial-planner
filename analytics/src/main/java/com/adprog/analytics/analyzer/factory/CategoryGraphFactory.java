package com.adprog.analytics.analyzer.factory;

import com.adprog.analytics.analyzer.factory.drawer.BarChartDrawer;
import com.adprog.analytics.analyzer.factory.drawer.GraphDrawer;
import com.adprog.analytics.analyzer.factory.drawer.PieChartDrawer;
import com.adprog.analytics.analyzer.factory.fetcher.DataFetcher;
import com.adprog.analytics.analyzer.factory.fetcher.DataFetcherImpl;
import com.adprog.analytics.analyzer.factory.parser.CategoryParser;
import com.adprog.analytics.analyzer.factory.parser.DataParser;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import org.json.JSONObject;

public class CategoryGraphFactory implements GraphFactory {
  private DataFetcher fetcher = new DataFetcherImpl();
  private DataParser parser = new CategoryParser();
  private GraphDrawer barChartDrawer = new BarChartDrawer();
  private GraphDrawer pieChartDrawer = new PieChartDrawer();

  public CategoryGraphFactory() {}

  public String createGraph(String projectTitle, long pid, String auth, String type)
      throws IOException, InterruptedException {
    ArrayList<JSONObject> dataC = this.fetcher.getCategoryData(pid, auth);
    ArrayList<JSONObject> dataT = this.fetcher.getTransactionData(pid, auth);
    Map<String, Long> parsedData = this.parser.parse(dataC, dataT);
    System.out.println("DataC = " + dataC.toString());
    System.out.println("DataT = " + dataT.toString());
    System.out.println("parsedData = " + parsedData.toString());
    switch (type) {
      case ("bar"):
        return this.barChartDrawer.draw(projectTitle, parsedData);
      case ("pie"):
        return this.pieChartDrawer.draw(projectTitle, parsedData);
      default:
        return "Wrong graph type!";
    }
  }
}
