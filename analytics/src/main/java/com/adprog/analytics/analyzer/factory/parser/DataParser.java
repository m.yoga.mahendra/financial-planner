package com.adprog.analytics.analyzer.factory.parser;

import java.util.ArrayList;
import java.util.Map;
import org.json.JSONObject;

public interface DataParser {
  public Map<String, Long> parse(ArrayList<JSONObject> dataC, ArrayList<JSONObject> dataT);
}
