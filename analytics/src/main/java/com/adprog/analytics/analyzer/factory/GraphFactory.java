package com.adprog.analytics.analyzer.factory;

import java.io.IOException;

public interface GraphFactory {
  public String createGraph(String projectTitle, long pid, String auth, String type)
      throws IOException, InterruptedException;
}
