package com.adprog.analytics.analyzer.factory.fetcher;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

public class TimeDataFetcherImpl implements DataFetcher {
  private static final HttpClient httpClient =
      HttpClient.newBuilder()
          .version(HttpClient.Version.HTTP_1_1)
          .connectTimeout(Duration.ofSeconds(12))
          .build();

  @Override
  public ArrayList<JSONObject> getCategoryData(long pid, String auth)
      throws IOException, InterruptedException {
    ArrayList<JSONObject> result = new ArrayList<>();
    HttpRequest requestCategory =
        HttpRequest.newBuilder()
            .GET()
            .uri(
                URI.create(
                    "http://financial-planner-datastorage.herokuapp.com/api/projects/"
                        + Long.toString(pid)
                        + "/categories"))
            .setHeader("Authorization", auth) // add request header
            .build();
    HttpResponse<String> responseC =
        httpClient.send(requestCategory, HttpResponse.BodyHandlers.ofString());
    if (responseC.statusCode() != 200) return result;
    JSONArray jar = new JSONArray(responseC.body());
    for (Object value : jar) {
      JSONObject jvalue = new JSONObject(value);
      result.add(jvalue);
    }
    return result;
  }

  @Override
  public ArrayList<JSONObject> getTransactionData(long pid, String auth)
      throws IOException, InterruptedException {
    ArrayList<JSONObject> result = new ArrayList<>();
    HttpRequest requestTransaction =
        HttpRequest.newBuilder()
            .GET()
            .uri(
                URI.create(
                    "http://financial-planner-datastorage.herokuapp.com/api/projects/"
                        + Long.toString(pid)
                        + "/transactions"))
            .setHeader("Authorization", auth) // add request header
            .build();
    HttpResponse<String> responseT =
        httpClient.send(requestTransaction, HttpResponse.BodyHandlers.ofString());
    if (responseT.statusCode() != 200) return result;
    JSONArray jar = new JSONArray(responseT.body());
    for (Object value : jar) {
      JSONObject jvalue = new JSONObject(value);
      result.add(jvalue);
    }
    return result;
  }
}
