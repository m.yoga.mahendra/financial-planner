package com.adprog.analytics.analyzer.factory;

import com.adprog.analytics.analyzer.factory.drawer.BarChartDrawer;
import com.adprog.analytics.analyzer.factory.drawer.GraphDrawer;
import com.adprog.analytics.analyzer.factory.drawer.LineChartDrawer;
import com.adprog.analytics.analyzer.factory.fetcher.DataFetcher;
import com.adprog.analytics.analyzer.factory.fetcher.DataFetcherImpl;
import com.adprog.analytics.analyzer.factory.parser.DataParser;
import com.adprog.analytics.analyzer.factory.parser.TimeParser;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import org.json.JSONObject;

public class TimesGraphFactory implements GraphFactory {
  private DataFetcher fetcher = new DataFetcherImpl();
  private DataParser parser = new TimeParser();
  private GraphDrawer barChartDrawer = new BarChartDrawer();
  private GraphDrawer lineChartDrawer = new LineChartDrawer();

  public TimesGraphFactory() {}

  public String createGraph(String projectTitle, long pid, String auth, String type)
      throws IOException, InterruptedException {
    ArrayList<JSONObject> dataC = this.fetcher.getCategoryData(pid, auth);
    ArrayList<JSONObject> dataT = this.fetcher.getTransactionData(pid, auth);
    Map<String, Long> parsedData = this.parser.parse(dataC, dataT);
    switch (type) {
      case ("bar"):
        return this.barChartDrawer.draw(projectTitle, parsedData);
      case ("line"):
        return this.lineChartDrawer.draw(projectTitle, parsedData);
      default:
        return "Wrong graph type!";
    }
  }
}
