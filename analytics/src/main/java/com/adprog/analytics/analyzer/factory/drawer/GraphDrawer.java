package com.adprog.analytics.analyzer.factory.drawer;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.Map;
import javax.imageio.ImageIO;

public abstract class GraphDrawer {
  private Map<String, Long> dataset;
  /**
   * Constructs a new application frame.
   *
   * @param title the frame title.
   */
  public abstract String draw(String title, Map<String, Long> dataset);

  public static String encodeToString(BufferedImage image, String type) {
    String imageString = null;
    ByteArrayOutputStream bos = new ByteArrayOutputStream();

    try {
      ImageIO.write(image, type, bos);
      byte[] imageBytes = bos.toByteArray();

      imageString = Base64.getEncoder().encodeToString(imageBytes);

      bos.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return imageString;
  }
}
