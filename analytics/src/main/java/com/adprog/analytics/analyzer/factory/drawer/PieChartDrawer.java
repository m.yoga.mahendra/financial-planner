package com.adprog.analytics.analyzer.factory.drawer;

import java.awt.image.BufferedImage;
import java.util.Map;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

public class PieChartDrawer extends GraphDrawer {
  public PieChartDrawer() {}

  @Override
  public String draw(String title, Map<String, Long> dataset) {
    JFreeChart chart2 = ChartFactory.createPieChart(title, createDataSet(dataset));
    BufferedImage bi = chart2.createBufferedImage(450, 450);
    return encodeToString(bi, "png");
  }

  private PieDataset createDataSet(Map<String, Long> dataset) {
    DefaultPieDataset pieDataSet = new DefaultPieDataset();
    for (String key : dataset.keySet()) {
      pieDataSet.setValue(key, dataset.get(key));
    }
    return pieDataSet;
  }
}
