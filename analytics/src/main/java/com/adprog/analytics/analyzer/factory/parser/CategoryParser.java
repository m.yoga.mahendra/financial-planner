package com.adprog.analytics.analyzer.factory.parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public class CategoryParser implements DataParser {
  @Override
  public Map<String, Long> parse(ArrayList<JSONObject> dataC, ArrayList<JSONObject> dataT) {
    Map<Long, String> categoryName = new HashMap<>();
    Map<Long, Long> totalSumCategory = new HashMap<>();
    for (JSONObject obj : dataC) {
      categoryName.put(obj.getLong("id"), obj.getString("name"));
      totalSumCategory.put(obj.getLong("id"), 0L);
    }
    for (JSONObject obj : dataT) {
      String[] cids = obj.getJSONObject("category").getString("href").split("/");
      long cid = Long.parseLong(cids[cids.length - 1]);
      long temp = totalSumCategory.get(cid);
      long amount = obj.getLong("amount");
      totalSumCategory.replace(cid, temp + amount);
    }
    Map<String, Long> result = new HashMap<>();
    for (Long id : categoryName.keySet()) {
      result.put(categoryName.get(id), totalSumCategory.get(id));
    }
    return result;
  }
}
