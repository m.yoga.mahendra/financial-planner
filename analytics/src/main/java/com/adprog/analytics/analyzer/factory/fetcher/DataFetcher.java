package com.adprog.analytics.analyzer.factory.fetcher;

import java.io.IOException;
import java.util.ArrayList;
import org.json.JSONObject;

public interface DataFetcher {
  public ArrayList<JSONObject> getCategoryData(long pid, String auth)
      throws IOException, InterruptedException;

  public ArrayList<JSONObject> getTransactionData(long pid, String auth)
      throws IOException, InterruptedException;
}
