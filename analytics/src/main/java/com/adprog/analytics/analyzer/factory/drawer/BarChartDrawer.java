package com.adprog.analytics.analyzer.factory.drawer;

import java.awt.image.BufferedImage;
import java.util.Map;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.data.category.DefaultCategoryDataset;

public class BarChartDrawer extends GraphDrawer {
  public BarChartDrawer() {}

  @Override
  public String draw(String title, Map<String, Long> dataset) {
    JFreeChart chart2 = ChartFactory.createBarChart(title, null, "Rupiah", createDataSet(dataset));
    BufferedImage bi = chart2.createBufferedImage(450, 450);
    return encodeToString(bi, "png");
  }

  private DefaultCategoryDataset createDataSet(Map<String, Long> dataset) {
    DefaultCategoryDataset dataSet = new DefaultCategoryDataset();
    for (String key : dataset.keySet()) {
      dataSet.addValue(dataset.get(key), key, "Categories Sum");
    }
    return dataSet;
  }
}
