package com.adprog.analytics.analyzer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public class Analyzer {
  public Map<String, String> getBasicInfo(Map<String, Long> data, ArrayList<JSONObject> dataC) {
    Map<String, String> basicInfo = new HashMap<>();
    basicInfo.put("Total Project Expense", sumOne(data, dataC, "E"));
    basicInfo.put("Total Project Income", sumOne(data, dataC, "I"));
    basicInfo.put("Total Project E/I Difference", diff(data, dataC));
    basicInfo.put("Project Status", status(basicInfo));
    return basicInfo;
  }

  private String sumOne(Map<String, Long> data, ArrayList<JSONObject> dataC, String type) {
    long total = 0L;
    for (JSONObject obj : dataC) {
      if (obj.getString("type").equalsIgnoreCase(type)) {
        total += data.get(obj.getString("name"));
      }
    }
    return Long.toString(total);
  }

  private String diff(Map<String, Long> data, ArrayList<JSONObject> dataC) {
    long total = 0L;
    for (JSONObject obj : dataC) {
      if (obj.getString("type").equalsIgnoreCase("I")) {
        total += data.get(obj.getString("name"));
      }
      if (obj.getString("type").equalsIgnoreCase("E")) {
        total -= data.get(obj.getString("name"));
      }
    }
    return Long.toString(total);
  }

  private String status(Map<String, String> data) {
    String status = null;
    String position = "0";
    long expense = Long.parseLong(data.get("Total Project Expense"));
    long income = Long.parseLong(data.get("Total Project Income"));
    if (income > expense * 2) position = "3";
    else if (income > expense) position = "2";
    else if (income > expense / 2) position = "1";
    else if (income < expense) position = "-1";
    else if (income < expense * 2) position = "-2";
    switch (position) {
      case ("0"):
      case ("1"):
        status = "Normal";
        break;
      case ("2"):
        status = "Surplus";
        break;
      case ("3"):
        status = "Overly Surplus";
        break;
      case ("-1"):
        status = "Deficit";
        break;
      case ("-2"):
        status = "Overly Deficit";
        break;
    }
    return status;
  }
}
