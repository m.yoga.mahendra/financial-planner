package com.adprog.analytics.controller;

import static org.springframework.http.ResponseEntity.ok;

import com.adprog.analytics.service.AnalyticService;
import java.io.IOException;
import java.net.http.HttpClient;
import java.time.Duration;
import java.util.Map;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class AnalyticsController {
  @Autowired private AnalyticService analyticSvc;
  private static final HttpClient httpClient =
      HttpClient.newBuilder()
          .version(HttpClient.Version.HTTP_1_1)
          .connectTimeout(Duration.ofSeconds(12))
          .build();

  @GetMapping("/{projectId}/basic-info")
  public ResponseEntity<String> getBasicInfo(
      @RequestHeader("Authorization") String auth, @PathVariable("projectId") Long pid)
      throws InterruptedException, IOException {
    Map<String, String> result = analyticSvc.getBasicInfo(auth, pid);
    JSONObject res = new JSONObject(result);
    return ok(res.toString());
  }

  @GetMapping("/{projectId}/graph/categories")
  public ResponseEntity<String> getGraph(
      @RequestHeader("Authorization") String auth,
      @PathVariable("projectId") Long pid,
      @RequestBody Map<String, String> body)
      throws InterruptedException, IOException {
    String graphType = body.get("type");
    return ok(analyticSvc.createGraph("categories", graphType, pid, auth));
  }

  @GetMapping("/{projectId}/graph/times")
  public ResponseEntity<String> getTimedGraph(
      @RequestHeader("Authorization") String auth,
      @PathVariable("projectId") Long pid,
      @RequestParam Map<String, String> body)
      throws InterruptedException, IOException {
    String graphType = body.get("type");
    return ok(analyticSvc.createGraph("times", graphType, pid, auth));
  }
}
