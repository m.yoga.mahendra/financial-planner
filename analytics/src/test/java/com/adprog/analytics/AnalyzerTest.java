package com.adprog.analytics;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.adprog.analytics.analyzer.Analyzer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class AnalyzerTest {
  Analyzer analyzer = new Analyzer();
  Map<String, Long> testData;
  ArrayList<JSONObject> testCategoryData;

  @BeforeEach
  public void setUp() {
    testData = new HashMap<>();
    testCategoryData = new ArrayList<>();

    JSONObject temp = new JSONObject();
    testData.put("Income A", 10000L);
    temp.put("name", "Income A");
    temp.put("type", "I");
    testCategoryData.add(temp);

    temp = new JSONObject();
    testData.put("Income B", 10000L);
    temp.put("name", "Income B");
    temp.put("type", "I");
    testCategoryData.add(temp);

    temp = new JSONObject();
    temp.put("name", "Expense A");
    testData.put("Expense A", 10000L);
    temp.put("type", "E");
    testCategoryData.add(temp);

    temp = new JSONObject();
    testData.put("Expense B", 10000L);
    temp.put("name", "Expense B");
    temp.put("type", "E");
    testCategoryData.add(temp);
  }

  @Test
  public void testGetBasicInfoReturnsCorrectData() {
    Map<String, String> testResult = analyzer.getBasicInfo(testData, testCategoryData);
    Map<String, String> expectedResult = new HashMap<>();
    expectedResult.put("Total Project Expense", "20000");
    expectedResult.put("Total Project Income", "20000");
    expectedResult.put("Total Project E/I Difference", "0");
    expectedResult.put("Project Status", "Normal");
    for (String key : expectedResult.keySet()) {
      assertEquals(testResult.get(key), expectedResult.get(key));
    }
  }
}
