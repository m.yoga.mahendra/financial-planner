package com.adprog.analytics.factory.fetcher;

import static org.junit.jupiter.api.Assertions.assertTrue;

import com.adprog.analytics.analyzer.factory.fetcher.DataFetcher;
import com.adprog.analytics.analyzer.factory.fetcher.DataFetcherImpl;
import com.adprog.analytics.analyzer.factory.fetcher.TimeDataFetcherImpl;
import java.io.IOException;
import org.junit.jupiter.api.Test;

public class DataFetcherTest {
  DataFetcher fetcherD = new DataFetcherImpl();
  DataFetcher fetcherT = new TimeDataFetcherImpl();

  @Test
  public void testFetchersCallsCorrectURL_and_StatusCodeIsNot200()
      throws IOException, InterruptedException {
    assertTrue(fetcherD.getCategoryData(1, "").isEmpty());
    assertTrue(fetcherD.getTransactionData(1, "").isEmpty());
    assertTrue(fetcherT.getCategoryData(1, "").isEmpty());
    assertTrue(fetcherT.getTransactionData(1, "").isEmpty());
  }
}
