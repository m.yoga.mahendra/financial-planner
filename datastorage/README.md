# Finance Planner Data Storage Service

Link: (http://financial-planner-datastorage.herokuapp.com)

Data Storage service is a microservice that handles the flow of business data. It is being run using Spring Boot Framework.
It implements REST API architecture and support CRUD operations for the database.

Documentations of this API Service can be found [here](https://documenter.getpostman.com/view/8362963/SzmfaJcP?version=latest)

Main contributor:
1. Inigo Ramli - [@IgoRamli](https://gitlab.com/IgoRamli)
2. Muhamad Yoga Mahendra - [@m.yoga.mahendra](https://gitlab.com/m.yoga.mahendra)
