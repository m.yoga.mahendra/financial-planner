package com.adprog.datastorage.query.jpa;

import javax.persistence.criteria.*;

public abstract class QueryBuilder<T> {
  public abstract CriteriaBuilder getBuilder();

  public abstract CriteriaQuery<T> getQuery();

  public abstract Root<T> getRoot();

  protected Path<T> getPath(String attributeName) {
    Path<T> path = getRoot();
    for (String part : attributeName.split("\\.")) {
      path = path.get(part);
    }
    return path;
  }
}
