package com.adprog.datastorage.query.jpa;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;

public class OrderClause<T> extends QueryBuilder<T> {

  private QueryBuilder<T> baseQuery;

  private List<Order> ordering;

  public OrderClause(QueryBuilder<T> baseQuery) {
    this.baseQuery = baseQuery;
    this.ordering = new ArrayList<>();
  }

  @Override
  public CriteriaBuilder getBuilder() {
    return baseQuery.getBuilder();
  }

  @Override
  public CriteriaQuery<T> getQuery() {
    return baseQuery.getQuery().orderBy(ordering);
  }

  @Override
  public Root<T> getRoot() {
    return baseQuery.getRoot();
  }

  public OrderClause<T> addOrdering(String fieldName, boolean isAscending) {
    CriteriaBuilder cb = getBuilder();
    Order ord = (isAscending) ? cb.asc(getPath(fieldName)) : cb.desc(getPath(fieldName));
    ordering.add(ord);
    return this;
  }
}
