package com.adprog.datastorage.query.api;

import com.adprog.datastorage.models.Category;
import com.adprog.datastorage.query.jpa.BaseQuery;
import com.adprog.datastorage.query.jpa.QueryBuilder;
import com.adprog.datastorage.query.jpa.WhereClause;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;

public class CategoryGetAllQueryBuilder extends GetAllQueryBuilder<Category> {

  private QueryBuilder<Category> query;

  private Long projectId;

  private String type;

  private boolean isTypeSet;

  public CategoryGetAllQueryBuilder(Long projectId) {
    this.projectId = projectId;
    isTypeSet = false;
  }

  public void addTypeQuery(String type) {
    this.type = type;

    isTypeSet = true;
  }

  public void addTypeFilter(WhereClause<Category> where) {
    if (isTypeSet) {
      where.addEqualClause("type", type);
    }
  }

  @Override
  public List<Category> buildQuery(EntityManager em) {
    CriteriaBuilder cb = em.getCriteriaBuilder();

    QueryBuilder<Category> query = new BaseQuery<>(cb, Category.class);
    WhereClause<Category> where = new WhereClause<>(query);
    where.addEqualClause("project.id", projectId);

    addTypeFilter(where);

    query = where;

    return em.createQuery(query.getQuery()).getResultList();
  }
}
