package com.adprog.datastorage.query.api;

import com.adprog.datastorage.models.Transaction;
import com.adprog.datastorage.query.jpa.BaseQuery;
import com.adprog.datastorage.query.jpa.OrderClause;
import com.adprog.datastorage.query.jpa.QueryBuilder;
import com.adprog.datastorage.query.jpa.WhereClause;
import java.sql.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;

public class TransactionGetAllQueryBuilder extends GetAllQueryBuilder<Transaction> {

  private QueryBuilder<Transaction> query;

  private List<Long> categories;

  private Long projectId;

  private Date startDate, endDate;
  private boolean isCategoriesSet, isDateSet;

  public TransactionGetAllQueryBuilder(Long projectId) {
    this.projectId = projectId;

    isCategoriesSet = false;
    isDateSet = false;
  }

  public void addCategoriesQuery(List<Long> categories) {
    this.categories = categories;

    isCategoriesSet = true;
  }

  public void addTimeIntervalQuery(Date startDate, Date endDate) {
    this.startDate = startDate;
    this.endDate = endDate;

    isDateSet = true;
  }

  private void addProjectFilter(WhereClause<Transaction> where) {
    where.addEqualClause("project.id", projectId);
  }

  private void addCategoryFilter(WhereClause<Transaction> where) {
    if (isCategoriesSet) {
      where.addInClause("category.id", categories, Long.class);
    }
  }

  private void addDateFilter(WhereClause<Transaction> where) {
    if (isDateSet) {
      where.addBetweenClause("transactionDate", startDate, endDate);
    }
  }

  @Override
  public List<Transaction> buildQuery(EntityManager em) {
    CriteriaBuilder cb = em.getCriteriaBuilder();

    QueryBuilder<Transaction> query = new BaseQuery<>(cb, Transaction.class);
    WhereClause<Transaction> where = new WhereClause<>(query);

    addProjectFilter(where);
    addCategoryFilter(where);
    addDateFilter(where);

    OrderClause<Transaction> order = new OrderClause<>(where).addOrdering("transactionDate", false);

    TypedQuery<Transaction> finalQuery = addPagination(em.createQuery(order.getQuery()));

    return finalQuery.getResultList();
  }
}
