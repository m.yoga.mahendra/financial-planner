package com.adprog.datastorage.query.jpa;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.*;

public class WhereClause<T> extends QueryBuilder<T> {

  private QueryBuilder<T> baseQuery;

  private List<Predicate> clauses;

  public WhereClause(QueryBuilder<T> baseQuery) {
    this.baseQuery = baseQuery;
    this.clauses = new ArrayList<>();
  }

  @Override
  public CriteriaBuilder getBuilder() {
    return baseQuery.getBuilder();
  }

  @Override
  public CriteriaQuery<T> getQuery() {
    return baseQuery.getQuery().where(clauses.toArray(new Predicate[0]));
  }

  @Override
  public Root<T> getRoot() {
    return baseQuery.getRoot();
  }

  public WhereClause<T> addEqualClause(String fieldName, Object value) {
    CriteriaBuilder cb = getBuilder();
    clauses.add(cb.equal(getPath(fieldName), value));
    return this;
  }

  public WhereClause<T> addBetweenClause(String fieldName, Date start, Date end) {
    CriteriaBuilder cb = getBuilder();
    Expression<Date> field = getPath(fieldName).as(Date.class);
    clauses.add(cb.between(field, start, end));
    return this;
  }

  public WhereClause<T> addBetweenClause(String fieldName, Long low, Long high) {
    CriteriaBuilder cb = getBuilder();
    Expression<Long> field = getPath(fieldName).as(Long.class);
    clauses.add(cb.between(field, low, high));
    return this;
  }

  public <S> WhereClause<T> addInClause(String fieldName, List<S> values, Class<S> type) {
    CriteriaBuilder cb = getBuilder();
    Expression<S> field = getPath(fieldName).as(type);
    clauses.add(field.in(values));
    return this;
  }
}
