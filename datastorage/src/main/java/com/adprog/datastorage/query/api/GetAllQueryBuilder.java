package com.adprog.datastorage.query.api;

import com.adprog.datastorage.models.Transaction;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

public abstract class GetAllQueryBuilder<T> {
  public abstract List<T> buildQuery(EntityManager em);

  private int limit, offset;
  private boolean isLimitSet, isOffsetSet;

  public void addLimitQuery(int limit) {
    this.limit = limit;

    isLimitSet = true;
  }

  public void addOffsetQuery(int offset) {
    this.offset = offset;

    isOffsetSet = true;
  }

  protected TypedQuery<Transaction> addPagination(TypedQuery<Transaction> query) {
    if (isLimitSet) {
      query.setMaxResults(limit);
    }
    if (isOffsetSet) {
      query.setFirstResult(offset);
    }
    return query;
  }
}
