package com.adprog.datastorage.query.jpa;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class BaseQuery<T> extends QueryBuilder<T> {

  private CriteriaBuilder criteriaBuilder;

  private CriteriaQuery<T> baseQuery;

  private Root<T> baseRoot;

  public BaseQuery(CriteriaBuilder cb, Class<T> entityType) {
    criteriaBuilder = cb;
    baseQuery = cb.createQuery(entityType);
    baseRoot = baseQuery.from(entityType);
  }

  @Override
  public CriteriaBuilder getBuilder() {
    return criteriaBuilder;
  }

  @Override
  public CriteriaQuery<T> getQuery() {
    return baseQuery.select(baseRoot);
  }

  @Override
  public Root<T> getRoot() {
    return baseRoot;
  }
}
