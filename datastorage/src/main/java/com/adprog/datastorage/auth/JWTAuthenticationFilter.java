package com.adprog.datastorage.auth;

import static com.adprog.datastorage.auth.SecurityConstants.*;
import static com.auth0.jwt.algorithms.Algorithm.HMAC512;

import com.adprog.datastorage.view.user.UserLogin;
import com.auth0.jwt.JWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.sql.Date;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

  private PasswordEncoder encoder;

  private AuthenticationManager authenticationManager;

  public JWTAuthenticationFilter(
      AuthenticationManager authenticationManager, PasswordEncoder encoder) {
    this.authenticationManager = authenticationManager;
    this.encoder = encoder;
  }

  @Override
  public Authentication attemptAuthentication(
      HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
    try {
      UserLogin cred = new ObjectMapper().readValue(request.getInputStream(), UserLogin.class);
      return authenticationManager.authenticate(
          new UsernamePasswordAuthenticationToken(cred.getUsername(), cred.getPassword()));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  protected void successfulAuthentication(
      HttpServletRequest req, HttpServletResponse res, FilterChain chain, Authentication auth)
      throws IOException, ServletException {
    String token =
        JWT.create()
            .withSubject(
                ((org.springframework.security.core.userdetails.User) auth.getPrincipal())
                    .getUsername())
            .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
            .sign(HMAC512(SECRET.getBytes()));
    res.addHeader(HEADER_STRING, TOKEN_PREFIX + token);
    res.getWriter().write(TOKEN_PREFIX + token);
    res.getWriter().flush();
    res.getWriter().close();
  }
}
