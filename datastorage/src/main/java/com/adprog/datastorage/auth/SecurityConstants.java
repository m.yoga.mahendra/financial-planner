package com.adprog.datastorage.auth;

import org.springframework.stereotype.Component;

@Component
public class SecurityConstants {
  public static final String SECRET = System.getenv("JWT_SECRET");

  public static final long EXPIRATION_TIME =
      Long.parseLong(System.getenv("JWT_EXPIRATION_TIME_IN_MILLIS"));

  public static final String TOKEN_PREFIX = "Bearer ";

  public static final String HEADER_STRING = "Authorization";
}
