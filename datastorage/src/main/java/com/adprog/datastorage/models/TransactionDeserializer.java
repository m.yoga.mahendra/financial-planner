package com.adprog.datastorage.models;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;
import java.sql.Date;

public class TransactionDeserializer extends StdDeserializer<Transaction> {
  public TransactionDeserializer() {
    this(null);
  }

  public TransactionDeserializer(Class<?> vc) {
    super(vc);
  }

  @Override
  public Transaction deserialize(JsonParser p, DeserializationContext ctxt)
      throws IOException, JsonProcessingException {
    JsonNode node = p.getCodec().readTree(p);

    String title = node.get("title").asText();
    String description = node.get("description").asText();
    Long amount = node.get("amount").asLong();
    String transactionDateStr = node.get("transactionDate").asText();
    Date transactionDate = Date.valueOf(transactionDateStr);

    Long projectId = node.get("project").asLong();
    Project project = new Project(projectId);

    Long categoryId = node.get("category").asLong();
    Category category = new Category(categoryId);
    return new Transaction(project, category, title, description, amount, transactionDate);
  }
}
