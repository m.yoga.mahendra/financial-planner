package com.adprog.datastorage.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Map;
import javax.persistence.*;
import javax.validation.constraints.*;

@Entity(name = "transaction")
@Table(name = "transaction")
@JsonDeserialize(using = TransactionDeserializer.class)
public class Transaction {
  @Id
  @GeneratedValue
  @Column(name = "transaction_id", nullable = false, unique = true)
  private Long transactionId;

  @ManyToOne(optional = false, fetch = FetchType.EAGER)
  @JoinColumn(name = "project_id", referencedColumnName = "project_id")
  @NotNull(message = "Transaction must belong to a Project")
  private Project project;

  @ManyToOne(optional = false, fetch = FetchType.EAGER)
  @JoinColumn(name = "category_id", referencedColumnName = "id")
  @NotNull(message = "Transaction must belong to a Category")
  private Category category;

  @Column(name = "title", nullable = false, length = 120)
  @NotNull(message = "Transaction title must not be null")
  @Size(min = 1, max = 64, message = "Transaction title length must be between 1 to 64 characters")
  private String title;

  @Column(name = "description", nullable = false)
  @NotNull(message = "Transaction description must not be null")
  @Size(
      min = 0,
      max = 255,
      message = "Transaction description length must be between 0 to 255 characters")
  private String description;

  @Column(name = "transaction_date", nullable = false)
  private Date transactionDate;

  @AssertTrue(message = "Transaction category must belong to the same project")
  private boolean isCategoryMatchWithProject() {
    if (this.project == null) return false;
    Long projectId = this.project.getId();
    if (this.category == null) return false;
    Long categoryId = this.category.getProject().getId();
    return projectId.equals(categoryId);
  }

  @AssertTrue(message = "Transaction date must be set in the past")
  private boolean isTransactionDateInThePast() {
    return (transactionDate != null)
        && transactionDate.before(new Date(Calendar.getInstance().getTimeInMillis()));
  }

  @Column(name = "submit_date", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
  private Timestamp submitDate;

  @Column(name = "amount", nullable = false)
  @NotNull(message = "Transaction amount must not be null")
  @Min(value = 0, message = "Transaction amount must me non-negative")
  private Long amount;

  public Transaction() {}

  public Transaction(
      Project project,
      Category category,
      String title,
      String description,
      Long amount,
      Date transactionDate) {
    this.project = project;
    this.category = category;
    this.title = title;
    this.description = description;
    this.amount = amount;
    this.transactionDate = transactionDate;
  }

  public Long getId() {
    return transactionId;
  }

  public Project getProject() {
    return project;
  }

  public Category getCategory() {
    return category;
  }

  public String getTitle() {
    return title;
  }

  public String getDescription() {
    return description;
  }

  public Date getTransactionDate() {
    return transactionDate;
  }

  public Timestamp getSubmitDate() {
    return submitDate;
  }

  public Long getAmount() {
    return amount;
  }

  public void setProject(Project project) {
    this.project = project;
  }

  public void setCategory(Category category) {
    this.category = category;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setTransactionDate(Date transactionDate) {
    this.transactionDate = transactionDate;
  }

  public void setAmount(Long amount) {
    this.amount = amount;
  }

  private void modifyCategory(Map<String, Object> data) {
    if (data.containsKey("category")) {
      Long newCategory = (Long) data.get("category");
      setCategory(new Category(newCategory));
    }
  }

  private void modifyTitle(Map<String, Object> data) {
    if (data.containsKey("title")) {
      setTitle((String) data.get("title"));
    }
  }

  private void modifyDescription(Map<String, Object> data) {
    if (data.containsKey("description")) {
      setDescription((String) data.get("description"));
    }
  }

  private void modifyAmount(Map<String, Object> data) {
    if (data.containsKey("amount")) {
      Long newAmount = (Long) data.get("amount");
      setAmount(newAmount);
    }
  }

  private void modifyTransactionDate(Map<String, Object> data) {
    if (data.containsKey("transactionDate")) {
      Date newDate = Date.valueOf((String) data.get("transactionDate"));
      setTransactionDate(newDate);
    }
  }

  public void modify(Map<String, Object> data) {
    modifyCategory(data);
    modifyTitle(data);
    modifyDescription(data);
    modifyAmount(data);
    modifyTransactionDate(data);
  }

  public String getHref() {
    return String.format("/api/projects/%d/transactions/%d", getProject().getId(), getId());
  }
}
