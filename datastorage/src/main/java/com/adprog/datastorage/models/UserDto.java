package com.adprog.datastorage.models;

import com.adprog.datastorage.validation.PasswordMatches;
import com.adprog.datastorage.validation.ValidEmail;
import com.adprog.datastorage.validation.ValidUsername;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import org.springframework.security.crypto.password.PasswordEncoder;

@PasswordMatches
public class UserDto {

  @NotNull(message = "Full name should not be null")
  @NotEmpty(message = "Full name should not be empty")
  private String fullName;

  @ValidUsername
  @NotNull(message = "Username should not be null")
  @NotEmpty(message = "Username should not be empty")
  private String username;

  @NotNull(message = "Password should not be null")
  @NotEmpty(message = "Password should not be empty")
  private String password;

  private String matchingPassword;

  @ValidEmail
  @NotNull(message = "Email should not be null")
  @NotEmpty(message = "Email should not be empty")
  private String email;

  public String getFullName() {
    return fullName;
  }

  public String getUsername() {
    return username;
  }

  public String getPassword() {
    return password;
  }

  public String getMatchingPassword() {
    return matchingPassword;
  }

  public String getEmail() {
    return email;
  }

  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public void setMatchingPassword(String matchingPassword) {
    this.matchingPassword = matchingPassword;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public User getUser(PasswordEncoder encoder) {
    String hash = encoder.encode(getPassword());
    return new User(getUsername(), getEmail(), hash, getFullName());
  }
}
