package com.adprog.datastorage.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.persistence.*;
import javax.validation.constraints.*;

@Entity(name = "category")
@Table(name = "category")
@JsonDeserialize(using = CategoryDeserializer.class)
public class Category implements Serializable {
  @Id
  @GeneratedValue
  @Column(name = "id")
  private Long id;

  @ManyToOne(optional = false)
  @JoinColumn(name = "project_id", referencedColumnName = "project_id")
  @NotNull(message = "Category should be assigned to a Project")
  private Project project;

  @OneToMany(mappedBy = "category")
  private List<Transaction> transactions;

  @Column(name = "name", nullable = false)
  @NotNull(message = "Category name must not be null")
  @Size(min = 1, max = 50, message = "Category name length must be between 1 to 50 characters")
  private String name;

  @Column(name = "type", nullable = false)
  @NotNull(message = "Category type must not be null")
  @NotEmpty(message = "Category type must not be empty")
  @Pattern(regexp = "[IE]", message = "Category type must be either \"I\" or \"E\"")
  private String type;

  @Column(name = "description", nullable = false)
  @NotNull(message = "Category description must not be null")
  @Size(max = 255, message = "Category description length must not exceed 255 characters")
  private String description;

  public Category() {}

  public Category(Long id) {
    this.id = id;
  }

  public Category(Project project, String name, String description, String type) {
    this.project = project;
    setName(name);
    setDescription(description);
    setType(type);
  }

  private void setName(String name) {
    this.name = name;
  }

  private void setType(String type) {
    this.type = type;
  }

  private void setDescription(String description) {
    this.description = description;
  }

  private void modifyName(Map<String, Object> data) {
    if (data.containsKey("name")) {
      setName((String) data.get("name"));
    }
  }

  private void modifyType(Map<String, Object> data) {
    if (data.containsKey("type")) {
      setType((String) data.get("type"));
    }
  }

  private void modifyDescription(Map<String, Object> data) {
    if (data.containsKey("description")) {
      setType((String) data.get("type"));
    }
  }

  public void modify(Map<String, Object> data) {
    modifyName(data);
    modifyType(data);
    modifyDescription(data);
  }

  public Long getId() {
    return id;
  }

  public Project getProject() {
    return project;
  }

  public String getName() {
    return name;
  }

  public String getType() {
    return type;
  }

  public String getDescription() {
    return description;
  }

  @Override
  public String toString() {
    return String.format("Category[name=%s, type=%s]", name, type);
  }

  public String getHref() {
    return String.format("/api/projects/%d/categories/%d", getProject().getId(), getId());
  }
}
