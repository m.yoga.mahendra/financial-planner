package com.adprog.datastorage.models;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;
import java.util.HashSet;

public class RoleDeserializer extends StdDeserializer<Role> {

  public RoleDeserializer(Class<?> vc) {
    super(vc);
  }

  public RoleDeserializer() {
    this(null);
  }

  @Override
  public Role deserialize(JsonParser p, DeserializationContext ctxt)
      throws IOException, JsonProcessingException {
    JsonNode node = p.getCodec().readTree(p);
    String name = node.get("name").asText();
    String description = node.get("description").asText();
    Project project = new Project(node.get("project").asLong());

    JsonNode permission = node.get("authorities");
    HashSet<AuthorityType> authorities = new HashSet<>();
    if (permission.isArray()) {
      for (final JsonNode authority : permission) {
        try {
          AuthorityType tmp = AuthorityType.valueOf(authority.asText());
          authorities.add(tmp);
        } catch (Exception e) {
          String msg =
              String.format(
                  "Invalid AuthorityType '%s'. Permitted values are: 'GET', 'POST', 'PATCH', 'DELETE', 'ROLE'",
                  authority.asText());
          throw new JsonParseException(p, msg);
        }
      }
    } else {
      throw new JsonParseException(p, "Value of 'authorities' should be an array");
    }

    return new Role(project, authorities, name, description);
  }
}
