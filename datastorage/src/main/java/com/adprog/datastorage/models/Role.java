package com.adprog.datastorage.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.persistence.*;

@Table(name = "role")
@Entity(name = "role")
@JsonDeserialize(using = RoleDeserializer.class)
public class Role {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id")
  private Long id;

  @OneToMany(mappedBy = "role")
  private Set<UserRole> users;

  @ManyToOne(optional = false)
  private Project project;

  @Column(name = "authorities", nullable = false)
  private HashSet<AuthorityType> authorities;

  @Column(name = "name")
  private String name;

  @Column(name = "description")
  private String description;

  public Role() {}

  public Role(
      Project project, HashSet<AuthorityType> authorities, String name, String description) {
    this.project = project;
    this.users = new HashSet<>();
    this.authorities = authorities;
    this.name = name;
    this.description = description;
  }

  public Long getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getDescription() {
    return description;
  }

  public HashSet<AuthorityType> getAuthorities() {
    return authorities;
  }

  public Project getProject() {
    return project;
  }

  public Set<UserRole> getUserRoles() {
    return users;
  }

  public Set<User> getUsers() {
    return getUserRoles().parallelStream().map(UserRole::getUser).collect(Collectors.toSet());
  }

  private void modifyName(Map<String, Object> data) {
    if (data.containsKey("name")) {
      this.name = (String) data.get("name");
    }
  }

  private void modifyDescription(Map<String, Object> data) {
    if (data.containsKey("description")) {
      this.description = (String) data.get("description");
    }
  }

  public void modify(Map<String, Object> data) {
    modifyName(data);
    modifyDescription(data);
  }
}
