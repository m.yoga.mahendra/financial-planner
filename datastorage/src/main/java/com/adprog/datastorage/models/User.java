package com.adprog.datastorage.models;

import java.util.*;
import java.util.stream.Collectors;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity(name = "user")
@Table(name = "user")
public class User {
  @Id
  @NotEmpty
  @Column(name = "username", unique = true)
  private String username;

  @NotNull
  @NotEmpty
  @Column(name = "full_name")
  private String fullName;

  @NotNull
  @NotEmpty
  @Column(name = "email", unique = true)
  private String email;

  @NotNull
  @NotEmpty
  @Column(name = "password")
  private String password;

  @OneToMany(mappedBy = "user")
  private Set<UserRole> roles;

  public User() {}

  public User(String username, String email, String password, String fullName) {
    this.username = username;
    this.email = email;
    this.password = password;
    this.fullName = fullName;
    this.roles = Collections.emptySet();
  }

  public String getUsername() {
    return username;
  }

  public String getFullName() {
    return fullName;
  }

  public String getEmail() {
    return email;
  }

  public String getPassword() {
    return password;
  }

  public Set<UserRole> getUserRoles() {
    return roles;
  }

  public List<Role> getRoles() {
    return getUserRoles().parallelStream().map(UserRole::getRole).collect(Collectors.toList());
  }

  public String getHref() {
    return String.format("/api/users/%s", getUsername());
  }

  private void modifyFullName(Map<String, String> newData) {
    if (newData.containsKey("fullName")) {
      this.fullName = newData.get("fullName");
    }
  }

  public void modify(Map<String, String> newData) {
    modifyFullName(newData);
  }
}
