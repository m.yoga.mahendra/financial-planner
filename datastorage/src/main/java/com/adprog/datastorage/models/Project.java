package com.adprog.datastorage.models;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.json.JSONObject;

@Entity(name = "project")
@Table(name = "project")
public class Project {
  @Id
  @GeneratedValue
  @Column(name = "project_id")
  private Long id;

  @Column(name = "title", nullable = false)
  @NotNull(message = "Project title must not be null")
  @Size(min = 1, max = 64, message = "Project title length must be between 1 to 64 characters")
  private String title;

  @Column(name = "description", nullable = false)
  @NotNull(message = "Project description must not be null")
  @Size(max = 255, message = "Project description length must be less than 256 characters")
  private String description;

  @Column(name = "creation_date")
  @NotNull(message = "Creation date should not be null")
  private Timestamp creationDate;

  @Column(name = "last_updated")
  @NotNull(message = "Last updated time should not be null")
  private Timestamp lastUpdated;

  @OneToMany(mappedBy = "project")
  private List<Transaction> transactions;

  @OneToMany(mappedBy = "project")
  private List<Role> roles;

  public Project() {}

  public Project(Long id) {
    this.id = id;
  }

  public Project(String title, String description) {
    this.title = title;
    this.description = description;
  }

  public Long getId() {
    return id;
  }

  public String getTitle() {
    return title;
  }

  public String getDescription() {
    return description;
  }

  public Timestamp getCreationDate() {
    return creationDate;
  }

  public Timestamp getLastUpdated() {
    return lastUpdated;
  }

  public List<Transaction> getTransactions() {
    return transactions;
  }

  public List<Role> getRoles() {
    return roles;
  }

  @PrePersist
  private void setTimestamps() {
    if (this.getCreationDate() == null) {
      this.creationDate = new Timestamp(Calendar.getInstance().getTimeInMillis());
      this.lastUpdated = new Timestamp(Calendar.getInstance().getTimeInMillis());
    }
  }

  @PreUpdate
  private void setLastUpdated() {
    this.lastUpdated = new Timestamp(Calendar.getInstance().getTimeInMillis());
  }

  public void setId(Long id) {
    this.id = id;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  private void modifyTitle(Map<String, String> data) {
    if (data.containsKey("title")) {
      setTitle(data.get("title"));
    }
  }

  private void modifyDescription(Map<String, String> data) {
    if (data.containsKey("description")) {
      setDescription(data.get("description"));
    }
  }

  public void modify(Map<String, String> newProject) {
    modifyTitle(newProject);
    modifyDescription(newProject);
  }

  public String getHref() {
    return String.format("/api/projects/%d", getId());
  }

  public JSONObject getProjectHref() {
    JSONObject res = new JSONObject();
    res.put("id", this.getId());
    res.put("href", this.getHref());
    return res;
  }
}
