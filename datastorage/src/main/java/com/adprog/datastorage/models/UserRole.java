package com.adprog.datastorage.models;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

@Table(name = "user_role")
@Entity
public class UserRole {
  @EmbeddedId UserRoleKey id;

  @ManyToOne
  @MapsId("username")
  @JoinColumn(name = "username")
  User user;

  @ManyToOne
  @MapsId("role_id")
  @JoinColumn(name = "role_id")
  Role role;

  public UserRole() {}

  public UserRole(User user, Role role) {
    this.id = new UserRoleKey(user.getUsername(), role.getId());
    this.user = user;
    this.role = role;
  }

  public User getUser() {
    return user;
  }

  public Role getRole() {
    return role;
  }

  public List<String> getAuthorities() {
    List<String> auth = new ArrayList<>();
    String projectId = role.getProject().getId().toString();
    for (AuthorityType type : role.getAuthorities()) {
      auth.add(projectId + "_" + type.toString());
    }
    return auth;
  }
}
