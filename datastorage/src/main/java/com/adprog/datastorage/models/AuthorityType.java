package com.adprog.datastorage.models;

public enum AuthorityType {
  GET,
  POST,
  PATCH,
  DELETE,
  ROLE
};
