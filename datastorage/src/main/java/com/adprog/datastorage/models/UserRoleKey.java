package com.adprog.datastorage.models;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class UserRoleKey implements Serializable {
  @Column(name = "username")
  String username;

  @Column(name = "role_id")
  Long roleId;

  public UserRoleKey() {}

  public UserRoleKey(String username, Long roleId) {
    this.username = username;
    this.roleId = roleId;
  }

  public String getUsername() {
    return username;
  }

  public Long getRoleId() {
    return roleId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    UserRoleKey that = (UserRoleKey) o;
    return getUsername().equals(that.username) && getRoleId().equals(that.roleId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(username, roleId);
  }
}
