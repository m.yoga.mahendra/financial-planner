package com.adprog.datastorage.models;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;

public class CategoryDeserializer extends StdDeserializer<Category> {
  public CategoryDeserializer(Class<?> vc) {
    super(vc);
  }

  public CategoryDeserializer() {
    this(null);
  }

  @Override
  public Category deserialize(JsonParser p, DeserializationContext ctxt)
      throws IOException, JsonProcessingException {
    JsonNode node = p.getCodec().readTree(p);
    String name = node.get("name").asText();
    String description = node.get("description").asText();
    String type = node.get("type").asText();
    Project project = new Project(node.get("project").asLong());
    return new Category(project, name, description, type);
  }
}
