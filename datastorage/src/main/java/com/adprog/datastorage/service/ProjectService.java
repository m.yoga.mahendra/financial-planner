package com.adprog.datastorage.service;

import com.adprog.datastorage.models.*;
import com.adprog.datastorage.view.*;
import java.util.List;
import java.util.Map;

public interface ProjectService {
  List<Project> getEntities();

  List<Project> getEntities(String username);

  Project getEntity(Long projectId);

  Project postEntity(Project newProject, User user);

  Project patchEntity(Long id, Map<String, String> newProject);

  void deleteEntity(Long id);
}
