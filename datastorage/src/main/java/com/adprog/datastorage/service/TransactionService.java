package com.adprog.datastorage.service;

import com.adprog.datastorage.exception.ProjectNotFoundException;
import com.adprog.datastorage.exception.TransactionNotFoundException;
import com.adprog.datastorage.models.Transaction;
import com.adprog.datastorage.query.api.GetAllQueryBuilder;
import com.adprog.datastorage.repository.CategoryRepository;
import com.adprog.datastorage.repository.ProjectRepository;
import com.adprog.datastorage.repository.TransactionRepository;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TransactionService implements ProjectDataService<Transaction> {

  @Autowired private EntityManager em;

  private ProjectRepository projectRepository;

  private CategoryRepository categoryRepository;

  private TransactionRepository transactionRepository;

  public TransactionService(
      ProjectRepository projectRepository,
      CategoryRepository categoryRepository,
      TransactionRepository transactionRepository) {
    this.projectRepository = projectRepository;
    this.categoryRepository = categoryRepository;
    this.transactionRepository = transactionRepository;
  }

  private void verifyProjectExists(Long projectId) throws ProjectNotFoundException {
    if (!projectRepository.existsById(projectId)) {
      throw new ProjectNotFoundException(projectId);
    }
  }

  @Override
  public List<Transaction> getEntities(Long projectId, GetAllQueryBuilder<Transaction> q) {
    verifyProjectExists(projectId);
    return q.buildQuery(em);
  }

  @Override
  public Transaction getEntity(Long projectId, Long id) {
    verifyProjectExists(projectId);
    Optional<Transaction> transaction = transactionRepository.findOneByProject(projectId, id);
    if (transaction.isPresent()) {
      return transaction.get();
    } else {
      throw new TransactionNotFoundException(projectId, id);
    }
  }

  @Override
  public Transaction postEntity(Long projectId, Transaction newEntity) {
    verifyProjectExists(projectId);
    if (newEntity.getProject().getId().equals(projectId)) {
      Long categoryId = newEntity.getCategory().getId();
      newEntity.setCategory(categoryRepository.getOne(categoryId));
      return transactionRepository.saveAndFlush(newEntity);
    } else {
      throw new IllegalArgumentException(
          "Project ID in the new Transaction does not match current Project");
    }
  }

  @Override
  public Transaction patchEntity(Long projectId, Long id, Map<String, Object> newData) {
    verifyProjectExists(projectId);
    Optional<Transaction> oldData = transactionRepository.findOneByProject(projectId, id);
    if (oldData.isPresent()) {
      oldData.get().modify(newData);
      return transactionRepository.saveAndFlush(oldData.get());
    } else {
      throw new TransactionNotFoundException(projectId, id);
    }
  }

  @Override
  public void deleteEntity(Long projectId, Long id) {
    verifyProjectExists(projectId);
    Optional<Transaction> transaction = transactionRepository.findOneByProject(projectId, id);
    if (transaction.isPresent()) {
      transactionRepository.delete(transaction.get());
    } else {
      throw new TransactionNotFoundException(projectId, id);
    }
  }
}
