package com.adprog.datastorage.service;

import com.adprog.datastorage.models.User;
import com.adprog.datastorage.models.UserRole;
import com.adprog.datastorage.repository.UserRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class AuthService implements UserDetailsService {

  private UserRepository repository;

  public AuthService(UserRepository repository) {
    this.repository = repository;
  }

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

    Optional<User> fetch = repository.findByUsername(username);
    if (!fetch.isPresent()) {
      throw new UsernameNotFoundException("No user found with username: " + username);
    }
    User user = fetch.get();
    boolean enabled = true;
    boolean accountNonExpired = true;
    boolean credentialsNonExpired = true;
    boolean accountNonLocked = true;
    return new org.springframework.security.core.userdetails.User(
        user.getUsername(),
        user.getPassword(),
        enabled,
        accountNonExpired,
        credentialsNonExpired,
        accountNonLocked,
        getAuthorities(user));
  }

  private List<GrantedAuthority> getAuthorities(User user) {
    List<GrantedAuthority> authorities = new ArrayList<>();

    List<UserRole> roles = repository.findAllUserRole(user.getUsername());
    for (UserRole role : roles) {
      List<String> auth = role.getAuthorities();
      for (String a : auth) {
        authorities.add(new SimpleGrantedAuthority(a));
      }
    }

    return authorities;
  }
}
