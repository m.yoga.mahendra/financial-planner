package com.adprog.datastorage.service;

import com.adprog.datastorage.exception.ProjectNotFoundException;
import com.adprog.datastorage.models.*;
import com.adprog.datastorage.repository.*;
import java.util.*;
import org.springframework.stereotype.Service;

@Service
public class ProjectServiceImpl implements ProjectService {

  private TransactionRepository transactionRepository;

  private CategoryRepository categoryRepository;

  private ProjectRepository projectRepository;

  private RoleRepository roleRepository;

  private UserRoleRepository userRoleRepository;

  public ProjectServiceImpl(
      ProjectRepository projectRepository,
      CategoryRepository categoryRepository,
      TransactionRepository transactionRepository,
      RoleRepository roleRepository,
      UserRoleRepository userRoleRepository) {
    this.projectRepository = projectRepository;
    this.categoryRepository = categoryRepository;
    this.transactionRepository = transactionRepository;
    this.roleRepository = roleRepository;
    this.userRoleRepository = userRoleRepository;
  }

  @Override
  public List<Project> getEntities() {
    return projectRepository.findAll();
  }

  @Override
  public List<Project> getEntities(String username) {
    return projectRepository.findAllByUser(username);
  }

  @Override
  public Project getEntity(Long projectId) {
    Optional<Project> project = projectRepository.findById(projectId);
    if (project.isPresent()) {
      return project.get();
    } else {
      throw new ProjectNotFoundException(projectId);
    }
  }

  private void generateAdminRole(Project newProject, User user) {
    HashSet<AuthorityType> adminAuthorities;
    adminAuthorities = new HashSet<>();
    adminAuthorities.add(AuthorityType.GET);
    adminAuthorities.add(AuthorityType.POST);
    adminAuthorities.add(AuthorityType.PATCH);
    adminAuthorities.add(AuthorityType.DELETE);
    adminAuthorities.add(AuthorityType.ROLE);

    Role adminRole;
    adminRole =
        new Role(
            newProject,
            adminAuthorities,
            "Admin Permission",
            "Default Admin Permission - Assigned by default to the project creator");
    roleRepository.saveAndFlush(adminRole);

    userRoleRepository.saveAndFlush(new UserRole(user, adminRole));
  }

  private void generateUserRole(Project newProject) {
    HashSet<AuthorityType> userAuthorities;
    userAuthorities = new HashSet<>();
    userAuthorities.add(AuthorityType.GET);
    userAuthorities.add(AuthorityType.POST);

    Role userRole;
    userRole =
        new Role(
            newProject,
            userAuthorities,
            "User Permission",
            "Default User Permission - Can view and add transaction data on the project");

    roleRepository.saveAndFlush(userRole);
  }

  private void generateRole(Project newProject, User user) {
    generateAdminRole(newProject, user);
    generateUserRole(newProject);
  }

  private void generateDefaultCategories(Project newProject) {
    final String DEFAULT_CATEGORY_DESC = "System generated category";
    List<Category> defaultCategories =
        Arrays.asList(
            new Category(newProject, "Salary", DEFAULT_CATEGORY_DESC, "I"),
            new Category(newProject, "Part-Time", DEFAULT_CATEGORY_DESC, "I"),
            new Category(newProject, "Food", DEFAULT_CATEGORY_DESC, "E"),
            new Category(newProject, "Entertainment", DEFAULT_CATEGORY_DESC, "E"),
            new Category(newProject, "Education", DEFAULT_CATEGORY_DESC, "E"),
            new Category(newProject, "Living", DEFAULT_CATEGORY_DESC, "E"),
            new Category(newProject, "Transport", DEFAULT_CATEGORY_DESC, "E"),
            new Category(newProject, "Shopping", DEFAULT_CATEGORY_DESC, "E"));
    defaultCategories.forEach(
        category -> {
          categoryRepository.saveAndFlush(category);
        });
  }

  private void generateDefaultProjectData(Project newProject, User creator) {
    generateRole(newProject, creator);
    generateDefaultCategories(newProject);
  }

  @Override
  public Project postEntity(Project newProject, User user) {
    Project result = projectRepository.saveAndFlush(newProject);
    generateDefaultProjectData(newProject, user);
    return result;
  }

  @Override
  public Project patchEntity(Long id, Map<String, String> newProject) {
    Optional<Project> oldProject = projectRepository.findById(id);
    if (oldProject.isPresent()) {
      oldProject.get().modify(newProject);
      return projectRepository.saveAndFlush(oldProject.get());
    } else {
      throw new ProjectNotFoundException(id);
    }
  }

  private void deleteAssociations(Long projectId) {
    List<Long> roleIds = roleRepository.findAllRoleIdByProject(projectId);
    transactionRepository.deleteAssociatedTransactionsByProject(projectId);
    categoryRepository.deleteAssociatedCategoriesByProject(projectId);
    roleRepository.deleteUserRoleAssociationsByRoles(roleIds);
    roleRepository.deleteAssociatedRolesByProject(projectId);
  }

  @Override
  public void deleteEntity(Long id) {
    if (projectRepository.existsById(id)) {
      deleteAssociations(id);
      projectRepository.deleteById(id);
    } else {
      throw new ProjectNotFoundException(id);
    }
  }
}
