package com.adprog.datastorage.service;

import com.adprog.datastorage.exception.CategoryNotFoundException;
import com.adprog.datastorage.exception.ProjectNotFoundException;
import com.adprog.datastorage.models.Category;
import com.adprog.datastorage.query.api.GetAllQueryBuilder;
import com.adprog.datastorage.repository.CategoryRepository;
import com.adprog.datastorage.repository.ProjectRepository;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoryService implements ProjectDataService<Category> {

  @Autowired private EntityManager em;

  private ProjectRepository projectRepository;

  private CategoryRepository categoryRepository;

  public CategoryService(
      ProjectRepository projectRepository, CategoryRepository categoryRepository) {
    this.projectRepository = projectRepository;
    this.categoryRepository = categoryRepository;
  }

  private void verifyProjectExists(Long projectId) throws ProjectNotFoundException {
    if (!projectRepository.existsById(projectId)) {
      throw new ProjectNotFoundException(projectId);
    }
  }

  @Override
  public List<Category> getEntities(Long projectId, GetAllQueryBuilder<Category> q) {
    verifyProjectExists(projectId);
    return q.buildQuery(em);
  }

  @Override
  public Category getEntity(Long projectId, Long id) {
    verifyProjectExists(projectId);
    Optional<Category> category = categoryRepository.findOneByProject(projectId, id);
    if (category.isPresent()) {
      return category.get();
    } else {
      throw new CategoryNotFoundException(projectId, id);
    }
  }

  @Override
  public Category postEntity(Long projectId, Category newEntity) {
    verifyProjectExists(projectId);
    if (newEntity.getProject().getId().equals(projectId))
      return categoryRepository.saveAndFlush(newEntity);
    else throw new IllegalArgumentException("Category Project does not match current project");
  }

  @Override
  public Category patchEntity(Long projectId, Long id, Map<String, Object> newData) {
    verifyProjectExists(projectId);
    Optional<Category> oldCategory = categoryRepository.findOneByProject(projectId, id);
    if (oldCategory.isPresent()) {
      oldCategory.get().modify(newData);
      return categoryRepository.saveAndFlush(oldCategory.get());
    } else {
      throw new CategoryNotFoundException(projectId, id);
    }
  }

  private void validateCategoryIsEligibleForDeletion(Long projectId, Long id) {
    if (categoryRepository.getTransactionCount(projectId, id) > 0) {
      String msg = String.format("Cannot delete Category ID (%d): ", id);
      msg += "There are still some transactions associated with this category";
    }
  }

  @Override
  public void deleteEntity(Long projectId, Long id) {
    verifyProjectExists(projectId);
    Optional<Category> category = categoryRepository.findOneByProject(projectId, id);
    if (category.isPresent()) {
      // validateCategoryIsEligibleForDeletion(projectId, id);
      categoryRepository.delete(category.get());
    } else {
      throw new CategoryNotFoundException(projectId, id);
    }
  }
}
