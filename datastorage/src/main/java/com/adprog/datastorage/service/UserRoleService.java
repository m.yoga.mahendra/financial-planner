package com.adprog.datastorage.service;

import com.adprog.datastorage.exception.ProjectNotFoundException;
import com.adprog.datastorage.exception.RoleNotFoundException;
import com.adprog.datastorage.exception.UserNotFoundException;
import com.adprog.datastorage.models.Role;
import com.adprog.datastorage.models.User;
import com.adprog.datastorage.models.UserRole;
import com.adprog.datastorage.models.UserRoleKey;
import com.adprog.datastorage.repository.ProjectRepository;
import com.adprog.datastorage.repository.RoleRepository;
import com.adprog.datastorage.repository.UserRepository;
import com.adprog.datastorage.repository.UserRoleRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;

@Service
public class UserRoleService {

  private ProjectRepository projectRepository;

  private RoleRepository roleRepository;

  private UserRepository repository;

  private UserRoleRepository userRoleRepository;

  public UserRoleService(
      ProjectRepository projectRepository,
      RoleRepository roleRepository,
      UserRepository repository,
      UserRoleRepository userRoleRepository) {
    this.projectRepository = projectRepository;
    this.roleRepository = roleRepository;
    this.repository = repository;
    this.userRoleRepository = userRoleRepository;
  }

  private void verifyRoleExists(Long projectId, Long roleId) {
    if (projectRepository.existsById(projectId)) {
      if (!roleRepository.existsInProject(projectId, roleId)) {
        throw new RoleNotFoundException(projectId, roleId);
      }
    } else {
      throw new ProjectNotFoundException(projectId);
    }
  }

  public List<User> getEntities(Long projectId, Long roleId) {
    verifyRoleExists(projectId, roleId);
    return repository.findAllWithRole(projectId, roleId);
  }

  public User getEntity(Long projectId, Long roleId, String id) {
    verifyRoleExists(projectId, roleId);

    Optional<User> result = repository.findOneWithRole(projectId, roleId, id);
    if (result.isPresent()) {
      return result.get();
    } else {
      throw new UserNotFoundException(roleId, id);
    }
  }

  public Role postEntity(Long projectId, Long roleId, String newEntityRef) {
    verifyRoleExists(projectId, roleId);

    // Add role here
    Optional<Role> roleToBeAdded = roleRepository.findOneByProject(projectId, roleId);
    Optional<User> userToAdd = repository.findById(newEntityRef);
    if (userToAdd.isPresent()) {
      if (roleToBeAdded.isPresent()) {
        Role role = roleToBeAdded.get();
        User user = userToAdd.get();

        UserRole newMapping = new UserRole(user, role);
        return userRoleRepository.saveAndFlush(newMapping).getRole();
      } else {
        throw new RoleNotFoundException(projectId, roleId);
      }
    } else {
      throw new UserNotFoundException(newEntityRef);
    }
  }

  public void deleteEntity(Long projectId, Long roleId, String id) {
    verifyRoleExists(projectId, roleId);

    Optional<Role> roleToBeDeleted = roleRepository.findOneByProject(projectId, roleId);
    Optional<User> userToDelete = repository.findOneWithRole(projectId, roleId, id);
    if (userToDelete.isPresent()) {
      if (roleToBeDeleted.isPresent()) {
        userRoleRepository.deleteById(new UserRoleKey(id, roleId));
      } else {
        throw new RoleNotFoundException(projectId, roleId);
      }
    } else {
      throw new UserNotFoundException(roleId, id);
    }
  }
}
