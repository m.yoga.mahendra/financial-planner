package com.adprog.datastorage.service;

import java.util.List;

public interface RoleDataService<T, PK> {
  List<T> getEntities(Long projectId, Long roleId);

  T getEntity(Long projectId, Long roleId, PK id);

  T postEntity(Long projectId, Long roleId, PK newEntityRef);

  void deleteEntity(Long projectId, Long roleId, PK id);
}
