package com.adprog.datastorage.service;

import com.adprog.datastorage.query.api.GetAllQueryBuilder;
import java.util.List;
import java.util.Map;

public interface ProjectDataService<T> {
  List<T> getEntities(Long projectId, GetAllQueryBuilder<T> q);

  T getEntity(Long projectId, Long id);

  T postEntity(Long projectId, T newEntity);

  T patchEntity(Long projectId, Long id, Map<String, Object> newData);

  void deleteEntity(Long projectId, Long id);
}
