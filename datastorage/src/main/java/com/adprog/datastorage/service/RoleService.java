package com.adprog.datastorage.service;

import com.adprog.datastorage.exception.ProjectNotFoundException;
import com.adprog.datastorage.exception.RoleNotFoundException;
import com.adprog.datastorage.models.Role;
import com.adprog.datastorage.query.api.GetAllQueryBuilder;
import com.adprog.datastorage.repository.ProjectRepository;
import com.adprog.datastorage.repository.RoleRepository;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleService implements ProjectDataService<Role> {
  @Autowired public ProjectRepository projectRepository;

  @Autowired public RoleRepository roleRepository;

  public RoleService(ProjectRepository projectRepository, RoleRepository roleRepository) {
    this.projectRepository = projectRepository;
    this.roleRepository = roleRepository;
  }

  private void verifyProjectExists(Long projectId) {
    if (!projectRepository.existsById(projectId)) {
      throw new ProjectNotFoundException(projectId);
    }
  }

  public List<Role> getEntities(Long projectId, GetAllQueryBuilder<Role> q) {
    verifyProjectExists(projectId);
    return roleRepository.findAllByProject(projectId);
  }

  public Role getEntity(Long projectId, Long roleId) {
    verifyProjectExists(projectId);

    Optional<Role> result = roleRepository.findOneByProject(projectId, roleId);
    if (result.isPresent()) {
      return result.get();
    } else {
      throw new RoleNotFoundException(projectId, roleId);
    }
  }

  @Override
  public Role postEntity(Long projectId, Role newEntity) {
    verifyProjectExists(projectId);

    System.err.println(newEntity);
    System.err.println(newEntity.getProject());

    if (newEntity.getProject().getId().equals(projectId)) {
      return roleRepository.saveAndFlush(newEntity);
    } else {
      throw new IllegalArgumentException("Project assigned to new Role does not match Project ID");
    }
  }

  @Override
  public Role patchEntity(Long projectId, Long id, Map<String, Object> newData) {
    verifyProjectExists(projectId);

    Optional<Role> result = roleRepository.findOneByProject(projectId, id);
    if (result.isPresent()) {
      Role oldData = result.get();
      oldData.modify(newData);
      return roleRepository.saveAndFlush(oldData);
    } else {
      throw new RoleNotFoundException(projectId, id);
    }
  }

  private void deleteAssociations(Long roleId) {
    roleRepository.deleteUserRoleAssociationsByRole(roleId);
  }

  @Override
  public void deleteEntity(Long projectId, Long id) {
    verifyProjectExists(projectId);

    if (roleRepository.existsInProject(projectId, id)) {
      deleteAssociations(id);
      roleRepository.deleteById(id);
    } else {
      throw new RoleNotFoundException(projectId, id);
    }
  }
}
