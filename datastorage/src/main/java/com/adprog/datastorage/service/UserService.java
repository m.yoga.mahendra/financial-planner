package com.adprog.datastorage.service;

import com.adprog.datastorage.exception.UserAlreadyExistException;
import com.adprog.datastorage.models.User;
import com.adprog.datastorage.models.UserDto;
import com.adprog.datastorage.repository.UserRepository;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {

  @Autowired private PasswordEncoder encoder;

  @Autowired private UserRepository repository;

  private void checkEmailAlreadyExist(String email) {
    Optional<User> check = repository.findByEmail(email);
    if (check.isPresent()) {
      throw new UserAlreadyExistException("An account for that email already exists");
    }
  }

  private void checkUsernameAlreadyExist(String username) {
    Optional<User> check = repository.findByUsername(username);
    if (check.isPresent()) {
      throw new UserAlreadyExistException("That username is already taken.");
    }
  }

  private void validateUserDto(UserDto user) {
    checkEmailAlreadyExist(user.getEmail());
    checkUsernameAlreadyExist(user.getUsername());
  }

  public List<User> getEntities() {
    return repository.findAll();
  }

  public User getEntity(String username) {
    Optional<User> result = repository.findByUsername(username);
    if (!result.isPresent())
      throw new UsernameNotFoundException(
          String.format("User with username '%s' does not exist!", username));
    return result.get();
  }

  public User postEntity(UserDto user) {
    validateUserDto(user);

    User newUser = user.getUser(encoder);
    return repository.saveAndFlush(newUser);
  }

  public User patchEntity(String username, Map<String, String> newData) {
    Optional<User> oldData = repository.findByUsername(username);
    if (!oldData.isPresent())
      throw new UsernameNotFoundException(
          String.format("User with username '%s' does not exist!", username));
    oldData.get().modify(newData);
    return repository.saveAndFlush(oldData.get());
  }

  private void deleteAssociations(String username) {
    repository.deleteUserRoleAssociationsByUser(username);
  }

  public void deleteEntity(String username) {
    Optional<User> user = repository.findByUsername(username);
    if (!user.isPresent()) {
      throw new UsernameNotFoundException(
          String.format("User with username '%s' does not exist!", username));
    }
    deleteAssociations(username);
    repository.delete(user.get());
  }
}
