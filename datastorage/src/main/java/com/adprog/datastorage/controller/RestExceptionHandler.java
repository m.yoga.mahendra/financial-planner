package com.adprog.datastorage.controller;

import com.adprog.datastorage.exception.*;
import com.fasterxml.jackson.core.JsonParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.json.JSONObject;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

  @ExceptionHandler(value = {ConstraintViolationException.class})
  protected ResponseEntity<Object> handleValidationError(
      ConstraintViolationException ex, WebRequest request) {
    Set<ConstraintViolation<?>> violations = ex.getConstraintViolations();
    JSONObject body = new JSONObject();
    JSONObject error = new JSONObject();
    error.put("code", 400);
    violations.forEach(
        item -> {
          Map<String, String> errorItem = new HashMap<>();
          errorItem.put("message", item.getMessage());
          errorItem.put("type", "ValidationError");
          error.append("errors", errorItem);
        });
    body.put("error", error);
    return ResponseEntity.badRequest().body(body.toString());
  }

  @ExceptionHandler(value = {JsonParseException.class})
  protected ResponseEntity<Object> handleJsonParseError(JsonParseException ex, WebRequest request) {
    JSONObject body = new JSONObject();
    JSONObject error = new JSONObject();
    error.put("code", 400);

    Map<String, String> errorItem = new HashMap<>();
    errorItem.put("message", ex.getMessage());
    errorItem.put("type", "JsonParseException");
    error.append("errors", errorItem);
    body.put("error", error);
    return ResponseEntity.badRequest().body(body.toString());
  }

  @ExceptionHandler(
      value = {
        ProjectNotFoundException.class,
        CategoryNotFoundException.class,
        TransactionNotFoundException.class,
        UsernameNotFoundException.class,
        RoleNotFoundException.class,
        UserNotFoundException.class
      })
  protected ResponseEntity<Object> handleDataNotFoundError(
      RuntimeException ex, WebRequest request) {
    return ResponseEntity.notFound().build();
  }

  @ExceptionHandler(value = {DataIntegrityViolationException.class})
  protected ResponseEntity<Object> handlePSQLException(
      DataIntegrityViolationException ex, WebRequest request) {
    JSONObject body = new JSONObject();
    JSONObject error = new JSONObject();
    error.put("code", 409);

    Map<String, String> errorItem = new HashMap<>();
    errorItem.put("message", ex.getMessage());
    errorItem.put("type", "DataIntegrityViolationException");
    error.append("errors", errorItem);
    body.put("error", error);
    return new ResponseEntity<>(body.toString(), HttpStatus.CONFLICT);
  }

  @Override
  protected ResponseEntity<Object> handleHttpMessageNotReadable(
      HttpMessageNotReadableException ex,
      HttpHeaders headers,
      HttpStatus status,
      WebRequest request) {
    Map<String, Object> body = new HashMap<>();
    body.put("code", "400");
    Map<String, String> error = new HashMap<>();
    error.put("message", "Required request body is missing");
    error.put("type", "HttpMessageNotReadableException");
    body.put("errors", error);
    return handleExceptionInternal(ex, body, headers, status, request);
  }

  @ExceptionHandler(value = {ForbiddenRequestException.class})
  protected ResponseEntity<String> handleForbiddenRequest(
      ForbiddenRequestException ex, WebRequest request) {
    Map<String, Object> body = new HashMap<>();
    body.put("code", "403");
    Map<String, String> error = new HashMap<>();
    error.put("message", ex.getMessage());
    error.put("type", "ForbiddenRequestException");
    body.put("errors", error);
    return new ResponseEntity<String>(body.toString(), HttpStatus.FORBIDDEN);
  }
}
