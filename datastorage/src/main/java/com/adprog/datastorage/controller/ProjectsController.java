package com.adprog.datastorage.controller;

import static org.springframework.http.ResponseEntity.ok;

import com.adprog.datastorage.models.Project;
import com.adprog.datastorage.models.User;
import com.adprog.datastorage.service.ProjectService;
import com.adprog.datastorage.service.UserService;
import com.adprog.datastorage.view.EntityProxy;
import com.adprog.datastorage.view.project.ProjectDetailProxy;
import com.adprog.datastorage.view.project.ProjectListProxy;
import java.net.URI;
import java.util.List;
import java.util.Map;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/api/projects")
public class ProjectsController {

  private ProjectService dataService;

  private UserService userService;

  public ProjectsController(ProjectService dataService, UserService userService) {
    this.dataService = dataService;
    this.userService = userService;
  }

  private User getAuthentication(Authentication auth) {
    String username = (String) auth.getPrincipal();
    return userService.getEntity(username);
  }

  @GetMapping("")
  public ResponseEntity<String> getProjects() {
    List<Project> projects = dataService.getEntities();
    EntityProxy<List<Project>> serializer = new ProjectListProxy(projects);
    return ok(serializer.serialize());
  }

  @GetMapping("/{id}")
  @PreAuthorize("hasAuthority(#id + '_GET')")
  public ResponseEntity<String> getProjectById(@PathVariable("id") Long id) {
    Project project = dataService.getEntity(id);
    EntityProxy<Project> serializer = new ProjectDetailProxy(project);
    return ok(serializer.serialize());
  }

  @PostMapping("")
  @PreAuthorize("isAuthenticated()")
  public ResponseEntity<String> postProject(Authentication auth, @RequestBody Project newProject) {
    Project result = dataService.postEntity(newProject, getAuthentication(auth));
    EntityProxy<Project> serializer = new ProjectDetailProxy(result);
    URI location = URI.create(result.getHref());
    return ResponseEntity.created(location).body(serializer.serialize());
  }

  @PatchMapping("/{id}")
  @PreAuthorize("hasAuthority(#id + '_PATCH')")
  public ResponseEntity<String> patchProject(
      @PathVariable("id") Long id, @RequestBody Map<String, String> newProject) {
    Project result = dataService.patchEntity(id, newProject);
    EntityProxy<Project> serializer = new ProjectDetailProxy(result);
    return ok(serializer.serialize());
  }

  @DeleteMapping("/{id}")
  @PreAuthorize("hasAuthority(#id + '_DELETE')")
  public ResponseEntity<String> deleteProjectById(@PathVariable("id") Long id) {
    dataService.deleteEntity(id);
    return ok("Project ID (" + id + ") has been deleted");
  }
}
