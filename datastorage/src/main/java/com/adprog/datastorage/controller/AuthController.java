package com.adprog.datastorage.controller;

import com.adprog.datastorage.exception.ForbiddenRequestException;
import com.adprog.datastorage.models.User;
import com.adprog.datastorage.models.UserDto;
import com.adprog.datastorage.service.UserService;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/auth")
public class AuthController {
  @Autowired private UserService service;

  private void verifyUserByUsername(Authentication authentication, String expectedUsername) {
    if (!authentication.getPrincipal().equals(expectedUsername)) {
      throw new ForbiddenRequestException();
    }
  }

  @GetMapping("")
  public List<User> getUserList() {
    return service.getEntities();
  }

  @GetMapping("/{username}")
  public User getUser(Authentication authentication, @PathVariable("username") String username) {
    verifyUserByUsername(authentication, username);
    return service.getEntity(username);
  }

  @PostMapping("")
  public User registerUser(@RequestBody @Valid UserDto user) {
    System.err.println(user);
    return service.postEntity(user);
  }

  @PatchMapping("/{username}")
  public User modifyUser(
      @PathVariable("username") String username, @RequestBody Map<String, String> newData) {
    return service.patchEntity(username, newData);
  }

  @DeleteMapping("/{username}")
  public ResponseEntity<Object> deleteUser(@PathVariable("username") String username) {
    service.deleteEntity(username);
    return ResponseEntity.ok().body(String.format("User %s successfully deleted!", username));
  }
}
