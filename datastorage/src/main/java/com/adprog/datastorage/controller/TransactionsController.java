package com.adprog.datastorage.controller;

import static org.springframework.http.ResponseEntity.created;
import static org.springframework.http.ResponseEntity.ok;

import com.adprog.datastorage.models.Transaction;
import com.adprog.datastorage.query.api.TransactionGetAllQueryBuilder;
import com.adprog.datastorage.service.TransactionService;
import com.adprog.datastorage.view.EntityProxy;
import com.adprog.datastorage.view.transaction.TransactionDetailProxy;
import com.adprog.datastorage.view.transaction.TransactionListProxy;
import java.net.URI;
import java.sql.Date;
import java.util.List;
import java.util.Map;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/api/projects/{projectId}/transactions")
public class TransactionsController {

  private TransactionService service;

  public TransactionsController(TransactionService service) {
    this.service = service;
  }

  @GetMapping("")
  @PreAuthorize("hasAuthority(#projectId + '_GET')")
  public ResponseEntity<String> getTransactions(
      @PathVariable("projectId") Long projectId,
      @RequestParam(required = false) List<Long> categories,
      @RequestParam(required = false) Date startDate,
      @RequestParam(required = false) Date endDate,
      @RequestParam(required = false) Integer limit,
      @RequestParam(required = false) Integer offset) {
    TransactionGetAllQueryBuilder q = new TransactionGetAllQueryBuilder(projectId);
    if (categories != null) q.addCategoriesQuery(categories);
    if (startDate != null && endDate != null) q.addTimeIntervalQuery(startDate, endDate);
    if (limit != null) {
      q.addLimitQuery(limit);
      q.addOffsetQuery((offset != null) ? offset : 0);
    }

    List<Transaction> result = service.getEntities(projectId, q);
    EntityProxy<List<Transaction>> serializer = new TransactionListProxy(result);
    return ok(serializer.serialize());
  }

  @GetMapping("/{id}")
  @PreAuthorize("hasAuthority(#projectId + '_GET')")
  public ResponseEntity<String> getTransactionById(
      @PathVariable("projectId") Long projectId, @PathVariable("id") Long id) {
    Transaction result = service.getEntity(projectId, id);
    EntityProxy<Transaction> serializer = new TransactionDetailProxy(result);
    return ok(serializer.serialize());
  }

  @PostMapping("")
  @PreAuthorize("hasAuthority(#projectId + '_POST')")
  public ResponseEntity<String> postTransaction(
      @PathVariable("projectId") Long projectId, @RequestBody Transaction body) {
    Transaction result = service.postEntity(projectId, body);
    EntityProxy<Transaction> serializer = new TransactionDetailProxy(result);
    URI location = URI.create(result.getHref());
    return created(location).body(serializer.serialize());
  }

  @PatchMapping("/{id}")
  @PreAuthorize("hasAuthority(#projectId + '_PATCH')")
  public ResponseEntity<String> patchTransaction(
      @PathVariable("projectId") Long projectId,
      @PathVariable("id") Long id,
      @RequestBody Map<String, Object> body) {
    Transaction result = service.patchEntity(projectId, id, body);
    EntityProxy<Transaction> serializer = new TransactionDetailProxy(result);
    return ok(serializer.serialize());
  }

  @DeleteMapping("/{id}")
  @PreAuthorize("hasAuthority(#projectId + '_DELETE')")
  public ResponseEntity<String> deleteTransaction(
      @PathVariable("projectId") Long projectId, @PathVariable("id") Long id) {
    service.deleteEntity(projectId, id);
    return ok(
        String.format(
            "Transaction ID (%d) has been deleted from project with ID (%d)", id, projectId));
  }
}
