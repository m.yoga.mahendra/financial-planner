package com.adprog.datastorage.controller;

import com.adprog.datastorage.models.Role;
import com.adprog.datastorage.models.User;
import com.adprog.datastorage.service.UserRoleService;
import com.adprog.datastorage.view.EntityProxy;
import com.adprog.datastorage.view.role.RoleDetailProxy;
import com.adprog.datastorage.view.user.UserCollectionProxy;
import com.adprog.datastorage.view.user.UserViewProxy;
import java.net.URI;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/projects/{projectId}/roles/{roleId}/users")
public class UserRoleController {

  private UserRoleService service;

  public UserRoleController(UserRoleService service) {
    this.service = service;
  }

  @GetMapping("")
  @PreAuthorize("hasAuthority(#projectId + '_ROLE')")
  public String getUsersWithRole(
      @PathVariable("projectId") Long projectId, @PathVariable("roleId") Long roleId) {
    List<User> rawResult = service.getEntities(projectId, roleId);
    EntityProxy<Collection<User>> serializer = new UserCollectionProxy(rawResult);
    return serializer.serialize();
  }

  @GetMapping("/{username}")
  @PreAuthorize("hasAuthority(#projectId + '_ROLE')")
  public String getUserWithRole(
      @PathVariable("projectId") Long projectId,
      @PathVariable("roleId") Long roleId,
      @PathVariable("username") String username) {
    User rawResult = service.getEntity(projectId, roleId, username);
    EntityProxy<User> serializer = new UserViewProxy(rawResult);
    return serializer.serialize();
  }

  @PostMapping("")
  @PreAuthorize("hasAuthority(#projectId + '_ROLE')")
  public ResponseEntity<String> addUserToRole(
      @PathVariable("projectId") Long projectId,
      @PathVariable("roleId") Long roleId,
      @RequestBody Map<String, String> body) {
    String username = body.get("username");
    Role rawResult = service.postEntity(projectId, roleId, username);
    EntityProxy<Role> serializer = new RoleDetailProxy(rawResult);
    URI location =
        URI.create(
            String.format("/api/projects/%d/roles/%d/users/%s", projectId, roleId, username));
    return ResponseEntity.created(location).body(serializer.serialize());
  }

  @DeleteMapping("/{username}")
  @PreAuthorize("hasAuthority(#projectId + '_ROLE')")
  public String deleteUserFromRole(
      @PathVariable("projectId") Long projectId,
      @PathVariable("roleId") Long roleId,
      @PathVariable("username") String username) {
    service.deleteEntity(projectId, roleId, username);
    return String.format("User '%s' successfully deleted from Role ID (%d)", username, roleId);
  }
}
