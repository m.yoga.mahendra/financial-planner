package com.adprog.datastorage.controller;

import static org.springframework.http.ResponseEntity.created;
import static org.springframework.http.ResponseEntity.ok;

import com.adprog.datastorage.models.Category;
import com.adprog.datastorage.query.api.CategoryGetAllQueryBuilder;
import com.adprog.datastorage.service.CategoryService;
import com.adprog.datastorage.view.EntityProxy;
import com.adprog.datastorage.view.category.CategoryDetailProxy;
import com.adprog.datastorage.view.category.CategoryListProxy;
import java.net.URI;
import java.util.List;
import java.util.Map;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/api/projects/{projectId}/categories")
public class CategoriesController {

  private CategoryService service;

  public CategoriesController(CategoryService service) {
    this.service = service;
  }

  @GetMapping("")
  @PreAuthorize("hasAuthority(#projectId + '_GET')")
  public ResponseEntity<String> getCategories(
      @PathVariable("projectId") Long projectId, @RequestParam(required = false) String type) {
    CategoryGetAllQueryBuilder q = new CategoryGetAllQueryBuilder(projectId);
    if (type != null) {
      System.err.println("Adding type");
      q.addTypeQuery(type);
    }

    List<Category> result = service.getEntities(projectId, q);
    EntityProxy<List<Category>> serializer = new CategoryListProxy(result);
    return ok(serializer.serialize());
  }

  @GetMapping("/{id}")
  @PreAuthorize("hasAuthority(#projectId + '_GET')")
  public ResponseEntity<String> getCategoryById(
      @PathVariable("projectId") Long projectId, @PathVariable("id") Long id) {
    Category result = service.getEntity(projectId, id);
    EntityProxy<Category> serializer = new CategoryDetailProxy(result);
    return ok(serializer.serialize());
  }

  @PostMapping("")
  @PreAuthorize("hasAuthority(#projectId + '_POST')")
  public ResponseEntity<String> postCategory(
      @PathVariable("projectId") Long projectId, @RequestBody Category body) {
    Category result = service.postEntity(projectId, body);

    EntityProxy<Category> serializer = new CategoryDetailProxy(result);
    return created(URI.create(result.getHref())).body(serializer.serialize());
  }

  @PatchMapping("/{id}")
  @PreAuthorize("hasAuthority(#projectId + '_PATCH')")
  public ResponseEntity<String> patchCategory(
      @PathVariable("projectId") Long projectId,
      @PathVariable("id") Long id,
      @RequestBody Map<String, Object> body) {
    Category result = service.patchEntity(projectId, id, body);
    EntityProxy<Category> serializer = new CategoryDetailProxy(result);
    return ok(serializer.serialize());
  }

  @DeleteMapping("/{id}")
  @PreAuthorize("hasAuthority(#projectId + '_DELETE')")
  public ResponseEntity<Object> deleteCategory(
      @PathVariable("projectId") Long projectId, @PathVariable("id") Long id) {
    service.deleteEntity(projectId, id);
    return ok(String.format("Category ID (%d) deleted from Project (%d)", id, projectId));
  }
}
