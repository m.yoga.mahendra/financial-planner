package com.adprog.datastorage.controller;

import com.adprog.datastorage.exception.ForbiddenRequestException;
import com.adprog.datastorage.models.User;
import com.adprog.datastorage.models.UserDto;
import com.adprog.datastorage.service.UserService;
import com.adprog.datastorage.view.EntityProxy;
import com.adprog.datastorage.view.user.UserCollectionProxy;
import com.adprog.datastorage.view.user.UserDetailsProxy;
import java.util.Collection;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/users")
public class UserController {
  private UserService service;

  public UserController(UserService service) {
    this.service = service;
  }

  private void verifyUserByUsername(Authentication authentication, String expectedUsername) {
    if (!authentication.getPrincipal().equals(expectedUsername)) {
      throw new ForbiddenRequestException();
    }
  }

  @GetMapping("")
  public String getUserList() {
    EntityProxy<Collection<User>> serializer = new UserCollectionProxy(service.getEntities());
    return serializer.serialize();
  }

  @GetMapping("/{username}")
  public String getUser(Authentication authentication, @PathVariable("username") String username) {
    verifyUserByUsername(authentication, username);

    User rawResult = service.getEntity(username);
    EntityProxy<User> serializer = new UserDetailsProxy(rawResult);
    return serializer.serialize();
  }

  @PostMapping("")
  public String registerUser(@RequestBody @Valid UserDto user) {
    User rawResult = service.postEntity(user);
    EntityProxy<User> serializer = new UserDetailsProxy(rawResult);
    return serializer.serialize();
  }

  @PatchMapping("/{username}")
  public String modifyUser(
      Authentication authentication,
      @PathVariable("username") String username,
      @RequestBody Map<String, String> newData) {
    verifyUserByUsername(authentication, username);

    User rawResult = service.patchEntity(username, newData);
    EntityProxy<User> serializer = new UserDetailsProxy(rawResult);
    return serializer.serialize();
  }

  @DeleteMapping("/{username}")
  public String deleteUser(
      Authentication authentication, @PathVariable("username") String username) {
    verifyUserByUsername(authentication, username);

    service.deleteEntity(username);
    return String.format("User %s successfully deleted!", username);
  }
}
