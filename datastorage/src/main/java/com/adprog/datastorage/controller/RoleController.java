package com.adprog.datastorage.controller;

import com.adprog.datastorage.models.Role;
import com.adprog.datastorage.service.RoleService;
import com.adprog.datastorage.view.EntityProxy;
import com.adprog.datastorage.view.role.RoleCollectionProxy;
import com.adprog.datastorage.view.role.RoleDetailProxy;
import java.net.URI;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/projects/{projectId}/roles")
public class RoleController {

  private RoleService service;

  public RoleController(RoleService service) {
    this.service = service;
  }

  @GetMapping("")
  @PreAuthorize("hasAuthority(#projectId + '_GET')")
  public String getRoles(@PathVariable("projectId") Long projectId) {
    List<Role> rawResult = service.getEntities(projectId, null);
    EntityProxy<Collection<Role>> serializer = new RoleCollectionProxy(rawResult);
    return serializer.serialize();
  }

  @GetMapping("/{roleId}")
  @PreAuthorize("hasAuthority(#projectId + '_GET')")
  public String getRole(
      @PathVariable("projectId") Long projectId, @PathVariable("roleId") Long roleId) {
    Role rawResult = service.getEntity(projectId, roleId);
    EntityProxy<Role> serializer = new RoleDetailProxy(rawResult);
    return serializer.serialize();
  }

  @PostMapping("")
  @PreAuthorize("hasAuthority(#projectId + '_ROLE')")
  public ResponseEntity<String> postRole(
      @PathVariable("projectId") Long projectId, @RequestBody Role newRole) {
    Role rawResult = service.postEntity(projectId, newRole);
    EntityProxy<Role> serializer = new RoleDetailProxy(rawResult);
    URI location =
        URI.create(String.format("/api/projects/%d/roles/%d", projectId, rawResult.getId()));
    return ResponseEntity.created(location).body(serializer.serialize());
  }

  @PatchMapping("/{roleId}")
  @PreAuthorize("hasAuthority(#projectId + '_ROLE')")
  public String patchRole(
      @PathVariable("projectId") Long projectId,
      @PathVariable("roleId") Long roleId,
      @RequestBody Map<String, Object> newData) {
    Role rawResult = service.patchEntity(projectId, roleId, newData);
    EntityProxy<Role> serializer = new RoleDetailProxy(rawResult);
    return serializer.serialize();
  }

  @DeleteMapping("/{roleId}")
  @PreAuthorize("hasAuthority(#projectId + '_ROLE')")
  public String deleteRole(
      @PathVariable("projectId") Long projectId, @PathVariable("roleId") Long roleId) {
    service.deleteEntity(projectId, roleId);
    return String.format(
        "Role ID (%d) in Project (%d) has successfully been deleted", roleId, projectId);
  }
}
