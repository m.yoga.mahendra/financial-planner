package com.adprog.datastorage.exception;

public class CategoryNotFoundException extends RuntimeException {
  public CategoryNotFoundException(String msg) {
    super(msg);
  }

  public CategoryNotFoundException(Long projectId, Long categoryId) {
    this(
        String.format("Category ID (%d) not found in Project with ID (%d)", categoryId, projectId));
  }
}
