package com.adprog.datastorage.exception;

public class TransactionNotFoundException extends RuntimeException {
  public TransactionNotFoundException(String msg) {
    super(msg);
  }

  public TransactionNotFoundException(Long projectId, Long transactionId) {
    this(
        String.format(
            "Transaction ID (%d) not found in Project with ID (%d)", transactionId, projectId));
  }
}
