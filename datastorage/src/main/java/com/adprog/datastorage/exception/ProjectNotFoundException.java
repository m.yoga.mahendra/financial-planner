package com.adprog.datastorage.exception;

public class ProjectNotFoundException extends RuntimeException {
  public ProjectNotFoundException(String msg) {
    super(msg);
  }

  public ProjectNotFoundException(Long projectId) {
    this("Project ID (" + projectId + ") does not exist");
  }
}
