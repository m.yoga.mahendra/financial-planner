package com.adprog.datastorage.exception;

public class ForbiddenRequestException extends RuntimeException {
  public ForbiddenRequestException() {
    super("You do not have the authorization to do this request!");
  }
}
