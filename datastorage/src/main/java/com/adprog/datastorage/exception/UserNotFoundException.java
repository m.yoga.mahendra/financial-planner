package com.adprog.datastorage.exception;

public class UserNotFoundException extends RuntimeException {

  public UserNotFoundException(String username) {
    super(String.format("No user with username '%s'", username));
  }

  public UserNotFoundException(Long roleId, String username) {
    super(String.format("User '%s' is not part of role ID (%d)", username, roleId));
  }
}
