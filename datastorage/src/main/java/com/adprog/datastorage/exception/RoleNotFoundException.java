package com.adprog.datastorage.exception;

public class RoleNotFoundException extends RuntimeException {
  public RoleNotFoundException(String msg) {
    super(msg);
  }

  public RoleNotFoundException(Long projectId, Long transactionId) {
    this(String.format("Role ID (%d) not found in Project with ID (%d)", transactionId, projectId));
  }
}
