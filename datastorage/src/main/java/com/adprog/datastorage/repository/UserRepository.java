package com.adprog.datastorage.repository;

import com.adprog.datastorage.models.User;
import com.adprog.datastorage.models.UserRole;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

  @Query("select u from user as u where u.email = :email")
  Optional<User> findByEmail(@Param("email") String email);

  @Query("select u from user as u where u.username = :username")
  Optional<User> findByUsername(@Param("username") String username);

  @Query(
      "select ur.user from UserRole as ur where ur.role.project.id = :projectId and ur.role.id = :roleId")
  List<User> findAllWithRole(@Param("projectId") Long projectId, @Param("roleId") Long roleId);

  @Query("select ur from UserRole as ur where ur.id.username = :username")
  List<UserRole> findAllUserRole(@Param("username") String username);

  @Query(
      "select ur.user from UserRole as ur where ur.role.project.id = :projectId and ur.role.id = :roleId and ur.id.username = :username")
  Optional<User> findOneWithRole(
      @Param("projectId") Long projectId,
      @Param("roleId") Long roleId,
      @Param("username") String username);

  @Query("select ur.user from UserRole as ur where ur.role.project.id = :projectId")
  List<User> findAllInProject(@Param("projectId") Long projectId);

  @Query("delete from UserRole as ur where ur.id.username = :username")
  void deleteUserRoleAssociationsByUser(@Param("username") String username);
}
