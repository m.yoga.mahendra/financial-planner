package com.adprog.datastorage.repository;

import com.adprog.datastorage.models.UserRole;
import com.adprog.datastorage.models.UserRoleKey;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRoleRepository extends JpaRepository<UserRole, UserRoleKey> {}
