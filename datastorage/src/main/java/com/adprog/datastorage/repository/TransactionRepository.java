package com.adprog.datastorage.repository;

import com.adprog.datastorage.models.Transaction;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {
  @Query("select t from transaction as t where t.project.id = :projectId")
  List<Transaction> findAllByProject(Long projectId);

  @Query("select t from transaction as t where t.project.id = :projectId and t.id = :id")
  Optional<Transaction> findOneByProject(Long projectId, Long id);

  @Transactional
  @Modifying
  @Query("delete from transaction as t where t.project.id = :projectId")
  void deleteAssociatedTransactionsByProject(@Param("projectId") Long projectId);
}
