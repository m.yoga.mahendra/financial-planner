package com.adprog.datastorage.repository;

import com.adprog.datastorage.models.Category;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
  @Query("select c from category as c where c.project.id = :projectId")
  List<Category> findAllByProject(@Param("projectId") Long projectId);

  @Query("select c from category as c where c.project.id = :projectId and c.id = :id")
  Optional<Category> findOneByProject(@Param("projectId") Long projectId, @Param("id") Long id);

  @Query(
      "select count(c.transactions) from category as c where c.project.id = :projectId and c.id = :id")
  Long getTransactionCount(@Param("projectId") Long projectId, @Param("id") Long id);

  @Transactional
  @Modifying
  @Query("DELETE FROM category AS c WHERE c.project.id = :projectId")
  void deleteAssociatedCategoriesByProject(@Param("projectId") Long projectId);
}
