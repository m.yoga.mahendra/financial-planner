package com.adprog.datastorage.repository;

import com.adprog.datastorage.models.User;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthRepository extends JpaRepository<User, String> {

  @Query("select u from user as u where u.email = ?1")
  <T> Optional<T> findByEmail(String email);

  @Query("select u from user as u where u.username = :username")
  <T> Optional<T> findByUsername(@Param("username") String username, Class<T> type);
}
