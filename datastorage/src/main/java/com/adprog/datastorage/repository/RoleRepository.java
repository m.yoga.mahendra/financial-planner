package com.adprog.datastorage.repository;

import com.adprog.datastorage.models.Role;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
  @Override
  Optional<Role> findById(Long id);

  @Query("select r from role as r where r.project.id=:projectId")
  List<Role> findAllByProject(@Param("projectId") Long projectId);

  @Query("select r from role as r where r.project.id=:projectId and r.id=:roleId")
  Optional<Role> findOneByProject(@Param("projectId") Long projectId, @Param("roleId") Long roleId);

  @Query(
      "select ("
          + "case when count(r) > 0 "
          + "then true "
          + "else false "
          + "end"
          + ") from role as r where r.project.id=:projectId and r.id=:roleId")
  boolean existsInProject(@Param("projectId") Long projectId, @Param("roleId") Long roleId);

  @Query("select ur.role from UserRole as ur where ur.id.username = :username")
  List<Role> findAllByUser(@Param("username") String username);

  @Query("select r.id from role as r where r.project.id = :projectId")
  List<Long> findAllRoleIdByProject(@Param("projectId") Long projectId);

  @Transactional
  @Modifying
  @Query("DELETE FROM role AS r WHERE r.project.id = :projectId")
  void deleteAssociatedRolesByProject(@Param("projectId") Long projectId);

  @Transactional
  @Modifying
  @Query("delete from UserRole as ur where ur.role.id = :roleId")
  void deleteUserRoleAssociationsByRole(@Param("roleId") Long roleId);

  @Transactional
  @Modifying
  @Query("delete from UserRole as ur where ur.role.id in (:roleIds)")
  void deleteUserRoleAssociationsByRoles(@Param("roleIds") List<Long> roleIds);
}
