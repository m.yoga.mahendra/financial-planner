package com.adprog.datastorage.repository;

import com.adprog.datastorage.models.Project;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {
  @Query("select ur.role.project from UserRole as ur where ur.id.username = :username")
  List<Project> findAllByUser(@Param("username") String username);
}
