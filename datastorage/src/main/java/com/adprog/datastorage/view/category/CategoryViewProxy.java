package com.adprog.datastorage.view.category;

import com.adprog.datastorage.models.Category;
import com.adprog.datastorage.view.EntityObjectProxy;
import org.json.JSONObject;

public class CategoryViewProxy extends EntityObjectProxy<Category> {
  public CategoryViewProxy(Category fullEntity) {
    super(fullEntity);
  }

  @Override
  public JSONObject toSerializableObject() {
    JSONObject result = new JSONObject();
    result.put("id", fullEntity.getId());
    result.put("name", fullEntity.getName());
    result.put("type", fullEntity.getType());
    return result;
  }
}
