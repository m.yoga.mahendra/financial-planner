package com.adprog.datastorage.view.transaction;

import com.adprog.datastorage.models.Transaction;
import com.adprog.datastorage.view.EntityArrayProxy;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class TransactionListProxy extends EntityArrayProxy<List<Transaction>> {

  public TransactionListProxy(List<Transaction> fullEntity) {
    super(fullEntity);
  }

  @Override
  public JSONArray toSerializableArray() {
    JSONArray result = new JSONArray();
    fullEntity.forEach(
        transaction -> {
          JSONObject transactionObject =
              new JSONObject()
                  .put("id", transaction.getId())
                  .put("title", transaction.getTitle())
                  .put("amount", transaction.getAmount())
                  .put("href", transaction.getHref())
                  .put("transactionDate", transaction.getTransactionDate())
                  .put("project", new JSONObject().put("href", transaction.getProject().getHref()))
                  .put(
                      "category",
                      new JSONObject()
                          .put("href", transaction.getCategory().getHref())
                          .put("id", transaction.getCategory().getId()));
          result.put(transactionObject);
        });
    return result;
  }
}
