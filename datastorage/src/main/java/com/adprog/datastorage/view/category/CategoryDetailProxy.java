package com.adprog.datastorage.view.category;

import com.adprog.datastorage.models.Category;
import com.adprog.datastorage.view.EntityObjectProxy;
import org.json.JSONObject;

public class CategoryDetailProxy extends EntityObjectProxy<Category> {
  public CategoryDetailProxy(Category fullEntity) {
    super(fullEntity);
  }

  @Override
  public JSONObject toSerializableObject() {
    JSONObject result = new JSONObject();
    result.put("id", fullEntity.getId());
    result.put("name", fullEntity.getName());
    result.put("type", fullEntity.getType());
    result.put("description", fullEntity.getDescription());

    JSONObject projectRef = new JSONObject();
    Long projectId = fullEntity.getProject().getId();
    projectRef.put("id", projectId);
    projectRef.put("href", String.format("/api/projects/%d", projectId));
    result.put("project", projectRef);

    return result;
  }
}
