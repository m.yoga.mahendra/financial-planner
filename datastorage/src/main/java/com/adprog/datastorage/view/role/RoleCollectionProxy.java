package com.adprog.datastorage.view.role;

import com.adprog.datastorage.models.Role;
import com.adprog.datastorage.view.EntityArrayProxy;
import java.util.Collection;
import org.json.JSONArray;

public class RoleCollectionProxy extends EntityArrayProxy<Collection<Role>> {

  public RoleCollectionProxy(Collection<Role> fullEntity) {
    super(fullEntity);
  }

  @Override
  public JSONArray toSerializableArray() {
    JSONArray res = new JSONArray();
    if (fullEntity != null) {
      fullEntity.forEach(
          role -> {
            String id = Long.toString(role.getId());
            res.put(new RoleViewProxy(role).toSerializableObject());
          });
    }
    return res;
  }
}
