package com.adprog.datastorage.view.category;

import com.adprog.datastorage.models.Category;
import com.adprog.datastorage.view.EntityArrayProxy;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class CategoryListProxy extends EntityArrayProxy<List<Category>> {
  public CategoryListProxy(List<Category> fullEntity) {
    super(fullEntity);
  }

  @Override
  public JSONArray toSerializableArray() {
    JSONArray result = new JSONArray();
    fullEntity.forEach(
        category -> {
          JSONObject categoryObject = new CategoryViewProxy(category).toSerializableObject();
          String projectId = category.getProject().getId().toString();
          String id = category.getId().toString();
          categoryObject.put(
              "href", String.format("/api/projects/%s/categories/%s", projectId, id));
          result.put(categoryObject);
        });
    return result;
  }
}
