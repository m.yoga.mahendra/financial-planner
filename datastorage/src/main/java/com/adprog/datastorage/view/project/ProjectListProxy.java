package com.adprog.datastorage.view.project;

import com.adprog.datastorage.models.Project;
import com.adprog.datastorage.view.EntityArrayProxy;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class ProjectListProxy extends EntityArrayProxy<List<Project>> {
  public ProjectListProxy(List<Project> fullEntity) {
    super(fullEntity);
  }

  @Override
  public JSONArray toSerializableArray() {
    JSONArray result = new JSONArray();
    fullEntity.forEach(
        project -> {
          JSONObject serializedProject = new ProjectViewProxy(project).toSerializableObject();
          String id = project.getId().toString();
          serializedProject.put("href", "/api/projects/" + id);
          result.put(serializedProject);
        });

    return result;
  }
}
