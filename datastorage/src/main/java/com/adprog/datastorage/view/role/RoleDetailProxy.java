package com.adprog.datastorage.view.role;

import com.adprog.datastorage.models.Role;
import com.adprog.datastorage.view.EntityObjectProxy;
import com.adprog.datastorage.view.user.UserCollectionProxy;
import org.json.JSONArray;
import org.json.JSONObject;

public class RoleDetailProxy extends EntityObjectProxy<Role> {

  public RoleDetailProxy(Role fullEntity) {
    super(fullEntity);
  }

  @Override
  public JSONObject toSerializableObject() {
    JSONObject result = new JSONObject();
    result.put("id", fullEntity.getId());
    result.put("name", fullEntity.getName());
    result.put("project", fullEntity.getProject().getProjectHref());
    result.put("authorities", fullEntity.getAuthorities());

    JSONArray serializedUsers =
        new UserCollectionProxy(fullEntity.getUsers()).toSerializableArray();
    result.put("users", serializedUsers);
    return result;
  }
}
