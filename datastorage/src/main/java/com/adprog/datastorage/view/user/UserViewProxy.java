package com.adprog.datastorage.view.user;

import com.adprog.datastorage.models.User;
import com.adprog.datastorage.view.EntityObjectProxy;
import org.json.JSONObject;

public class UserViewProxy extends EntityObjectProxy<User> {
  public UserViewProxy(User fullEntity) {
    super(fullEntity);
  }

  @Override
  public JSONObject toSerializableObject() {
    JSONObject result = new JSONObject();
    result.put("username", fullEntity.getUsername());
    result.put("fullName", fullEntity.getFullName());
    result.put("href", fullEntity.getHref());
    return result;
  }
}
