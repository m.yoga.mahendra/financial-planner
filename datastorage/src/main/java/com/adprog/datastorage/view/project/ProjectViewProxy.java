package com.adprog.datastorage.view.project;

import com.adprog.datastorage.models.Project;
import com.adprog.datastorage.view.EntityObjectProxy;
import org.json.JSONObject;

public class ProjectViewProxy extends EntityObjectProxy<Project> {
  public ProjectViewProxy(Project fullEntity) {
    super(fullEntity);
  }

  @Override
  public JSONObject toSerializableObject() {
    JSONObject result = new JSONObject();
    result.put("id", fullEntity.getId());
    result.put("title", fullEntity.getTitle());
    result.put("description", fullEntity.getDescription());

    return result;
  }
}
