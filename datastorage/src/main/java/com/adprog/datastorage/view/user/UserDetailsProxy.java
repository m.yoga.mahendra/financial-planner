package com.adprog.datastorage.view.user;

import com.adprog.datastorage.models.User;
import com.adprog.datastorage.view.EntityObjectProxy;
import com.adprog.datastorage.view.role.RoleCollectionProxy;
import org.json.JSONArray;
import org.json.JSONObject;

public class UserDetailsProxy extends EntityObjectProxy<User> {
  public UserDetailsProxy(User fullEntity) {
    super(fullEntity);
  }

  @Override
  public JSONObject toSerializableObject() {
    JSONObject result = new JSONObject();
    result.put("username", fullEntity.getUsername());
    result.put("fullName", fullEntity.getFullName());
    result.put("email", fullEntity.getEmail());

    JSONArray roles = new RoleCollectionProxy(fullEntity.getRoles()).toSerializableArray();
    result.put("roles", roles);
    return result;
  }
}
