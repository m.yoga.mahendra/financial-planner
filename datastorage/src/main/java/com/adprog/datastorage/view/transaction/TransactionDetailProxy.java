package com.adprog.datastorage.view.transaction;

import com.adprog.datastorage.models.Category;
import com.adprog.datastorage.models.Project;
import com.adprog.datastorage.models.Transaction;
import com.adprog.datastorage.view.EntityObjectProxy;
import org.json.JSONObject;

public class TransactionDetailProxy extends EntityObjectProxy<Transaction> {

  public TransactionDetailProxy(Transaction fullEntity) {
    super(fullEntity);
  }

  @Override
  public JSONObject toSerializableObject() {
    JSONObject result = new JSONObject();
    Project projectRef = fullEntity.getProject();
    Category categoryRef = fullEntity.getCategory();

    result.put("id", fullEntity.getId());
    result.put("title", fullEntity.getTitle());
    result.put("description", fullEntity.getDescription());
    result.put("amount", fullEntity.getAmount());
    result.put("transactionDate", fullEntity.getTransactionDate());
    result.put("submitDate", fullEntity.getSubmitDate());
    result.put(
        "project",
        new JSONObject()
            .put("id", projectRef.getId())
            .put("title", projectRef.getTitle())
            .put("href", projectRef.getHref()));
    result.put(
        "category",
        new JSONObject()
            .put("id", categoryRef.getId())
            .put("name", categoryRef.getName())
            .put("href", categoryRef.getHref()));
    return result;
  }
}
