package com.adprog.datastorage.view;

import org.json.JSONArray;

public abstract class EntityArrayProxy<T> extends EntityProxy<T> {
  public EntityArrayProxy(T fullEntity) {
    super(fullEntity);
  }

  public abstract JSONArray toSerializableArray();

  @Override
  public String serialize() {
    return toSerializableArray().toString();
  }
}
