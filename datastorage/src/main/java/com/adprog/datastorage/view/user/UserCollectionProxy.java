package com.adprog.datastorage.view.user;

import com.adprog.datastorage.models.User;
import com.adprog.datastorage.view.EntityArrayProxy;
import java.util.Collection;
import org.json.JSONArray;

public class UserCollectionProxy extends EntityArrayProxy<Collection<User>> {

  public UserCollectionProxy(Collection<User> fullEntity) {
    super(fullEntity);
  }

  @Override
  public JSONArray toSerializableArray() {
    JSONArray res = new JSONArray();
    if (fullEntity != null) {
      fullEntity.forEach(
          user -> {
            res.put(new UserViewProxy(user).toSerializableObject());
          });
    }
    return res;
  }
}
