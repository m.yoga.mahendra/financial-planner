package com.adprog.datastorage.view.role;

import com.adprog.datastorage.models.Role;
import com.adprog.datastorage.view.EntityObjectProxy;
import org.json.JSONObject;

public class RoleViewProxy extends EntityObjectProxy<Role> {

  public RoleViewProxy(Role fullEntity) {
    super(fullEntity);
  }

  @Override
  public JSONObject toSerializableObject() {
    JSONObject result = new JSONObject();
    result.put("id", fullEntity.getId());
    result.put("name", fullEntity.getName());
    result.put("project", fullEntity.getProject().getProjectHref());
    return result;
  }
}
