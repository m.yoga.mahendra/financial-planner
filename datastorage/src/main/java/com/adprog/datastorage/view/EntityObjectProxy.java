package com.adprog.datastorage.view;

import org.json.JSONObject;

public abstract class EntityObjectProxy<T> extends EntityProxy<T> {
  public EntityObjectProxy(T fullEntity) {
    super(fullEntity);
  }

  public abstract JSONObject toSerializableObject();

  @Override
  public String serialize() {
    return toSerializableObject().toString();
  }
}
