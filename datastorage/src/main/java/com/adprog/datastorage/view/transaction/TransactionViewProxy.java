package com.adprog.datastorage.view.transaction;

import com.adprog.datastorage.models.Category;
import com.adprog.datastorage.models.Project;
import com.adprog.datastorage.models.Transaction;
import com.adprog.datastorage.view.EntityObjectProxy;
import org.json.JSONObject;

public class TransactionViewProxy extends EntityObjectProxy<Transaction> {

  public TransactionViewProxy(Transaction fullEntity) {
    super(fullEntity);
  }

  @Override
  public JSONObject toSerializableObject() {
    JSONObject result = new JSONObject();
    Project projectRef = fullEntity.getProject();
    Category categoryRef = fullEntity.getCategory();

    result.put("id", fullEntity.getId());
    result.put("title", fullEntity.getTitle());
    result.put("description", fullEntity.getDescription());
    result.put("transactionDate", fullEntity.getTransactionDate());
    result.put("amount", fullEntity.getAmount());
    result.put(
        "project",
        new JSONObject().put("href", String.format("/api/projects/%d", projectRef.getId())));
    result.put(
        "category",
        new JSONObject()
            .put(
                "href",
                String.format(
                    "/api/projects/%d/categories/%d", projectRef.getId(), categoryRef.getId())));
    return result;
  }
}
