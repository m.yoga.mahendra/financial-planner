package com.adprog.datastorage.view;

public abstract class EntityProxy<T> {
  protected T fullEntity;

  public EntityProxy(T fullEntity) {
    this.fullEntity = fullEntity;
  }

  public abstract String serialize();
}
