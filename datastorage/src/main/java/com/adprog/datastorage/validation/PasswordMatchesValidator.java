package com.adprog.datastorage.validation;

import com.adprog.datastorage.models.UserDto;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, Object> {

  @Override
  public void initialize(PasswordMatches constraintAnnotation) {}

  public boolean isNotNull(UserDto user) {
    return (user.getPassword() != null) && (user.getMatchingPassword() != null);
  }

  @Override
  public boolean isValid(Object obj, ConstraintValidatorContext context) {
    UserDto user = (UserDto) obj;
    return isNotNull(user) && user.getPassword().equals(user.getMatchingPassword());
  }
}
