package com.adprog.datastorage.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UsernameValidator implements ConstraintValidator<ValidUsername, String> {
  private static final String USERNAME_PATTERN = "^[a-zA-Z0-9]*$";

  public UsernameValidator() {}

  @Override
  public void initialize(ValidUsername constraintAnnotation) {}

  @Override
  public boolean isValid(String value, ConstraintValidatorContext context) {
    return (isNotNull(value) && validateUsername(value));
  }

  private boolean isNotNull(String username) {
    return (username != null);
  }

  private boolean validateUsername(String username) {
    return username.matches(USERNAME_PATTERN);
  }
}
