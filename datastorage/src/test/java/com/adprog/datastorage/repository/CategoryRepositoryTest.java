package com.adprog.datastorage.repository;

import static org.assertj.core.api.Assertions.assertThat;

import com.adprog.datastorage.models.Category;
import com.adprog.datastorage.models.Project;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class CategoryRepositoryTest {

  @Autowired private ProjectRepository projectRepo;

  @Autowired private CategoryRepository categoryRepo;

  private Category income, expense;

  @BeforeEach
  public void setUp() {
    Project mockProject = new Project("Mock", "Mock");
    projectRepo.saveAndFlush(mockProject);
    income = new Category(mockProject, "Monthly Income", "Everything in", "I");
    categoryRepo.saveAndFlush(income);
    expense = new Category(mockProject, "Food", "All food", "E");
    categoryRepo.saveAndFlush(expense);
  }

  @Test
  public void testCategoryRepositoryFindAll_returnAllCategories() {
    List<Category> categoryList = categoryRepo.findAll();
    assertThat(categoryList).hasSize(2);
    assertThat(categoryList.get(0).toString()).isEqualTo(income.toString());
    assertThat(categoryList.get(1).toString()).isEqualTo(expense.toString());
  }
}
