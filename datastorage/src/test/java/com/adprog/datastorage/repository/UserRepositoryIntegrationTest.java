package com.adprog.datastorage.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

import com.adprog.datastorage.models.*;
import java.util.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
public class UserRepositoryIntegrationTest {
  @Autowired private ProjectRepository projectRepository;

  @Autowired private UserRepository userRepository;

  @Autowired private UserRoleRepository userRoleRepository;

  @Autowired private RoleRepository roleRepository;

  private Project mockProject, mockProject2;
  private User admin, user;

  private UserRole admin1;

  private Role role1, role2;

  @BeforeEach
  public void setUp() {
    userRepository.deleteAll();

    admin = new User("Admin", "admin@gmail.com", "Admin", "Admin");
    user = new User("User", "user@gmail.com", "User", "User");

    userRepository.saveAndFlush(admin);
    userRepository.saveAndFlush(user);

    mockProject = new Project("Mock Project", "Mocks");
    mockProject2 = new Project("Mock another", "Des");

    projectRepository.saveAndFlush(mockProject);
    projectRepository.saveAndFlush(mockProject2);

    role1 = new Role(mockProject, new HashSet<>(), "Mock", "Mock");
    role2 = new Role(mockProject2, new HashSet<>(), "Mock", "Mock");

    roleRepository.saveAndFlush(role1);
    roleRepository.saveAndFlush(role2);

    admin1 = new UserRole(admin, role1);

    userRoleRepository.saveAndFlush(admin1);
  }

  @Test
  public void testUserRepositoryFindByEmail_adminEmail_shouldReturnAdmin() {
    Optional<User> res = userRepository.findByEmail("admin@gmail.com");

    assertThat(res.isPresent());
    assertThat(res.get()).satisfies(user -> user.getUsername().equals("admin"));
  }

  @Test
  public void testUserRepositoryFindByEmail_invalidEmail_shouldReturnEmpty() {
    Optional<User> res = userRepository.findByEmail("notadmin@gmail.com");

    assertThat(res.isPresent()).isFalse();
  }

  @Test
  public void testUserRepositoryFindByUsername_userUsername_shouldReturnUser() {
    Optional<User> res = userRepository.findByUsername("User");

    assertThat(res.isPresent());
    assertThat(res.get()).satisfies(user -> user.getUsername().equals("user"));
  }

  @Test
  public void testUserRepositoryFindByUsername_invalidUsername_shouldReturnEmpty() {
    Optional<User> res = userRepository.findByUsername("not user");

    assertThat(res.isPresent()).isFalse();
  }

  @Test
  public void testUserRepositoryFindAllByRole_validRole_shouldReturnListOfRoles() {
    List<User> res = userRepository.findAllWithRole(mockProject.getId(), role1.getId());

    assertThat(res).anyMatch(user -> user.getUsername().equals("Admin"));
    assertThat(res).hasSize(1);
  }

  @Test
  public void testUserRepositoryFindAllByRole_invalidRole_shouldReturnEmptyList() {
    List<User> res;

    res = userRepository.findAllWithRole(mockProject.getId(), -1L);
    assertThat(res).isEmpty();

    res = userRepository.findAllWithRole(-1L, role1.getId());
    assertThat(res).isEmpty();
  }

  @Test
  public void testUserRepositoryFindAllUserRole_adminUsername_shouldReturnAdminRole() {
    List<UserRole> res = userRepository.findAllUserRole("Admin");

    assertThat(res).hasSize(1);
  }

  @Test
  public void testUserRepositoryFindAllUserRole_invalidUsername_shouldReturnEmptyList() {
    List<UserRole> res = userRepository.findAllUserRole("Not Admin");

    assertThat(res).isEmpty();
  }

  @Test
  public void testUserRepositoryFindOneWithRole_validUsername_shouldReturnUser() {
    Optional<User> res =
        userRepository.findOneWithRole(mockProject.getId(), role1.getId(), "Admin");

    assertThat(res.isPresent());
    assertThat(res.get()).matches(user -> user.getUsername().equals("Admin"));
  }

  @Test
  public void testUserRepositoryFindOneWithRole_invalidInput_shouldReturnEmpty() {
    Optional<User> res;

    res = userRepository.findOneWithRole(mockProject.getId(), role1.getId(), "User");
    assertThat(res.isPresent()).isFalse();

    res = userRepository.findOneWithRole(mockProject.getId(), -1L, "Admin");
    assertThat(res.isPresent()).isFalse();

    res = userRepository.findOneWithRole(-1L, role1.getId(), "User");
    assertThat(res.isPresent()).isFalse();
  }

  @Test
  public void testUserRepositoryFindAllInProject_validProject_shouldReturnListOfRolesInProject() {
    List<User> res = userRepository.findAllInProject(mockProject.getId());

    assertThat(res).anyMatch(user -> user.getUsername().equals("Admin"));
  }

  @Test
  public void testUserRepositoryFindAllInProject_invalidProject_shouldReturnEmptyList() {
    List<User> res = userRepository.findAllInProject(-1L);

    assertThat(res).isEmpty();
  }
}
