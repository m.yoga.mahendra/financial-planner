package com.adprog.datastorage.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

import com.adprog.datastorage.models.Category;
import com.adprog.datastorage.models.Project;
import com.adprog.datastorage.models.Transaction;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
public class ProjectRepositoryTest {
  @Autowired private TestEntityManager entityManager;

  @Autowired private ProjectRepository project;

  private Project sampleProject1, sampleProject2;
  private Category income, expense;
  private Transaction sampleTransaction1, sampleTransaction2, sampleTransaction3;

  @BeforeEach
  public void setUp() {
    // Clean current database
    project.deleteAll();

    // Add one project
    sampleProject1 = new Project("Sample Project", "This is a description");
    project.save(sampleProject1);
    sampleProject2 = new Project("Another Project", "Another project");
    project.save(sampleProject2);
  }

  @AfterEach
  public void tearDown() {}

  @Test
  public void testProjectRepositoryFindAll_returnAllProjectAsOverview() {
    List<Project> projectList = project.findAll();
    assertThat(projectList.size()).isEqualTo(2);

    assertThat(
        projectList.stream()
            .anyMatch(
                p ->
                    p.getTitle().equals(sampleProject1.getTitle())
                        && p.getDescription().equals(sampleProject1.getDescription())));

    assertThat(
        projectList.stream()
            .anyMatch(
                p ->
                    p.getTitle().equals(sampleProject2.getTitle())
                        && p.getDescription().equals(sampleProject2.getDescription())));
  }

  @Test
  public void testProjectRepositoryFindById_validId_returnOneProjectWithDetails() {
    Optional<Project> res = project.findById(sampleProject1.getId());
    assertThat(res).isPresent();
    assertThat(res.get()).isEqualTo(sampleProject1);
  }

  @Test
  public void testProjectRepositorySaveAndFlush_updateObjectOnDb() {
    sampleProject1.setTitle("New Project 1");
    sampleProject1.setDescription("Brand new project");
    project.saveAndFlush(sampleProject1);

    Project res = project.getOne(sampleProject1.getId());
    assertThat(res.getTitle()).isEqualTo("New Project 1");
    assertThat(res.getDescription()).isEqualTo("Brand new project");
  }
}
