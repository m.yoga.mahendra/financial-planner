package com.adprog.datastorage.repository;

import static org.assertj.core.api.Assertions.assertThat;

import com.adprog.datastorage.models.Category;
import com.adprog.datastorage.models.Project;
import com.adprog.datastorage.models.Transaction;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class TransactionRepositoryTest {
  @Autowired private TestEntityManager entityManager;

  @Autowired private TransactionRepository repository;

  private Project sampleProject1, sampleProject2;
  private Category income, income2, expense;
  private Transaction sampleTransaction1, sampleTransaction2, sampleTransaction3;

  @BeforeEach
  public void setUp() {
    // Delete current repository
    repository.deleteAll();

    // Add one project
    sampleProject1 = new Project("Sample Project", "This is a description");
    entityManager.persistAndFlush(sampleProject1);
    sampleProject2 = new Project("Another Project", "Another project");
    entityManager.persistAndFlush(sampleProject2);

    // Add two categories
    income = new Category(sampleProject1, "Monthly Income", "Everything in", "I");
    entityManager.persistAndFlush(income);
    income2 = new Category(sampleProject2, "Monthly Income", "Everything in", "I");
    entityManager.persistAndFlush(income2);
    expense = new Category(sampleProject1, "Food", "Every food", "E");
    entityManager.persistAndFlush(expense);

    // Add two transactions
    sampleTransaction1 =
        new Transaction(
            sampleProject1,
            income,
            "Monthly Income",
            "Income for Feb 2020",
            Integer.toUnsignedLong(20000),
            new Date(Calendar.getInstance().getTimeInMillis()));
    entityManager.persistAndFlush(sampleTransaction1);

    sampleTransaction2 =
        new Transaction(
            sampleProject1,
            expense,
            "Monthly Expense",
            "Expense for Mar 2020",
            Integer.toUnsignedLong(10000),
            new Date(Calendar.getInstance().getTimeInMillis()));
    entityManager.persistAndFlush(sampleTransaction2);

    sampleTransaction3 =
        new Transaction(
            sampleProject2,
            income2,
            "Monthly Income",
            "Income for Feb 2020",
            Integer.toUnsignedLong(20000),
            new Date(Calendar.getInstance().getTimeInMillis()));
    entityManager.persistAndFlush(sampleTransaction3);
  }

  @Test
  public void testTransactionRepositoryFindByProject_validProject_returnAllTransaction() {
    List<Transaction> transactionList = repository.findAllByProject(sampleProject1.getId());
    assertThat(transactionList).hasSize(2);
    assertThat(
        transactionList.stream()
            .anyMatch(
                t ->
                    t.getTitle().equals(sampleTransaction1.getTitle())
                        && t.getAmount().equals(sampleTransaction1.getAmount())));
    assertThat(
        transactionList.stream()
            .anyMatch(
                t ->
                    t.getTitle().equals(sampleTransaction2.getTitle())
                        && t.getAmount().equals(sampleTransaction2.getAmount())));
  }

  @Test
  public void testTransactionRepositoryFindByProject_invalidProject_returnEmptyList() {
    List<Transaction> transactionList = repository.findAllByProject(Long.parseLong("-1"));
    assertThat(transactionList).isEmpty();
  }
}
