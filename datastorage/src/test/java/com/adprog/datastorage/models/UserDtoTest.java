package com.adprog.datastorage.models;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class UserDtoTest {
  public static Validator validator;

  @BeforeAll
  public static void setUp() {
    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    validator = factory.getValidator();
  }

  @Test
  public void testUserDtoModel_validData_noConstraintViolation() {
    UserDto mock = new UserDto();
    mock.setUsername("cykes123");
    mock.setEmail("cykes@gmail.com");
    mock.setPassword("cykes");
    mock.setMatchingPassword("cykes");
    mock.setFullName("Cykes");
    Set<ConstraintViolation<UserDto>> violations = validator.validate(mock);
    assertThat(violations.isEmpty());
  }

  @Test
  public void testUSerDtoModel_nullValues_shouldCreateNotNullConstraintViolations() {
    UserDto mock = new UserDto();
    Set<ConstraintViolation<UserDto>> violations = validator.validate(mock);
    assertThat(violations)
        .anyMatch(v -> (v.getMessage().contains("Username") && v.getMessage().contains("null")));
    assertThat(violations)
        .anyMatch(v -> (v.getMessage().contains("Password") && v.getMessage().contains("null")));
    assertThat(violations)
        .anyMatch(v -> (v.getMessage().contains("Full name") && v.getMessage().contains("null")));
    assertThat(violations)
        .anyMatch(v -> (v.getMessage().contains("Email") && v.getMessage().contains("null")));
  }

  @Test
  public void testUserDtoModel_invalidEmail_shouldCreateInvalidEmailConstraintViolation() {
    UserDto mock = new UserDto();
    mock.setEmail("cykes");
    Set<ConstraintViolation<UserDto>> violations = validator.validate(mock);
    assertThat(violations).anyMatch(v -> v.getMessage().toLowerCase().contains("email"));
  }

  @Test
  public void testUserDtoModel_invalidUsername_shouldCreateInvalidUsernameConstraintViolation() {
    UserDto mock;
    Set<ConstraintViolation<UserDto>> violations;

    mock = new UserDto();
    mock.setUsername("cykes 123"); // Should have no whitespace characters
    violations = validator.validate(mock);
    assertThat(violations)
        .anyMatch(v -> (v.getMessage().contains("Username") && v.getMessage().contains("Invalid")));

    mock = new UserDto();
    mock.setUsername("cykes-123"); // Should have no symbols
    violations = validator.validate(mock);
    assertThat(violations)
        .anyMatch(v -> (v.getMessage().contains("Username") && v.getMessage().contains("Invalid")));

    mock = new UserDto();
    mock.setUsername("1Cykes1"); // Uppercase alphabet is allowed
    violations = validator.validate(mock);
    assertThat(violations)
        .noneMatch(
            v -> (v.getMessage().contains("Username") && v.getMessage().contains("Invalid")));
  }

  @Test
  public void testUserDtoModel_mismatchPassword_shouldCreatePasswordMismatchConstraintViolation() {
    UserDto mock = new UserDto();
    mock.setPassword("abc");
    mock.setMatchingPassword("Abc");
    Set<ConstraintViolation<UserDto>> violations = validator.validate(mock);
    assertThat(violations).anyMatch(v -> v.getMessage().contains("Password"));
  }
}
