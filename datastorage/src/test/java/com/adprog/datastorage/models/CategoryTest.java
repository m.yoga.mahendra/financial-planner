package com.adprog.datastorage.models;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CategoryTest {

  private Validator validator;

  @BeforeEach
  public void setUp() {
    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    validator = factory.getValidator();
    System.out.println(validator);
  }

  @Test
  public void testCategoryModel_validObject_noConstraintViolations() {
    Category category = new Category(new Project(1L), "Test", "Test", "I");

    Set<ConstraintViolation<Category>> violations = validator.validate(category);
    assertThat(violations).isEmpty();
  }

  @Test
  public void testCategoryModel_nullValues_throwViolations() {
    Category category = new Category();
    Set<ConstraintViolation<Category>> violations = validator.validate(category);
    assertThat(violations).anyMatch(violation -> (violation.getMessage().contains("Project")));
    assertThat(violations).anyMatch(violation -> (violation.getMessage().contains("name")));
    assertThat(violations).anyMatch(violation -> (violation.getMessage().contains("description")));
    assertThat(violations).anyMatch(violation -> (violation.getMessage().contains("type")));
  }

  @Test
  public void testCategoryModel_invalidStringLength_throwViolations() {
    StringBuilder description = new StringBuilder();
    for (int i = 0; i < 256; ++i) {
      description.append('A');
    }

    Category category = new Category(new Project(), "", description.toString(), "I");
    Set<ConstraintViolation<Category>> violations = validator.validate(category);
    System.out.println(violations);
    assertThat(violations)
        .anyMatch(violation -> (violation.getPropertyPath().toString().equals("name")));
    assertThat(violations)
        .anyMatch(violation -> (violation.getPropertyPath().toString().equals("description")));
  }

  @Test
  public void testCategoryModel_invalidType_throwViolations() {
    Category category = new Category(new Project(), "", "", "U");
    Set<ConstraintViolation<Category>> violations = validator.validate(category);
    System.out.println(violations);
    assertThat(violations).anyMatch(violation -> (violation.getMessage().contains("type")));
  }
}
