package com.adprog.datastorage.models;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

public class ProjectTest {

  @Autowired private TestEntityManager em;

  private Validator validator;

  @BeforeEach
  public void setUp() {
    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    validator = factory.getValidator();
  }

  @Test
  public void testProjectModel_validProject_noConstraintException() {
    Project project = new Project();
    project.setTitle("Test");
    project.setDescription("test");

    Set<ConstraintViolation<Project>> violations = validator.validate(project);
    assertThat(violations.isEmpty());
  }

  @Test
  public void testProjectModel_nullValues_throwViolations() {
    Project project = new Project();

    Set<ConstraintViolation<Project>> violations = validator.validate(project);
    assertThat(violations)
        .anyMatch(v -> v.getMessage().contains("title") && v.getMessage().contains("null"));
    assertThat(violations)
        .anyMatch(v -> v.getMessage().contains("description") && v.getMessage().contains("null"));
  }

  @Test
  public void testProjectModel_invalidPropertyLength_throwViolations() {
    Project project = new Project();
    project.setTitle("");

    StringBuilder str = new StringBuilder();
    for (int i = 0; i < 256; ++i) {
      str.append('A');
    }
    project.setDescription(str.toString());

    Set<ConstraintViolation<Project>> violations = validator.validate(project);
    assertThat(violations)
        .anyMatch(v -> v.getMessage().contains("title") && v.getMessage().contains("length"));
    assertThat(violations)
        .anyMatch(v -> v.getMessage().contains("description") && v.getMessage().contains("length"));
  }

  @Test
  public void testProjectModelModifyMethod_changeProperties() {
    Project old = new Project();

    Map<String, String> modification = new HashMap<>();
    modification.put("title", "New Title");
    modification.put("description", "New desc");

    old.modify(modification);

    assertThat(old.getTitle()).isEqualTo("New Title");
    assertThat(old.getDescription()).isEqualTo("New desc");
  }
}
