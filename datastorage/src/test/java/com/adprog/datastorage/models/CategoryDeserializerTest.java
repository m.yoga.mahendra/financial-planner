package com.adprog.datastorage.models;

import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.jupiter.api.Test;

public class CategoryDeserializerTest {

  private CategoryDeserializer deserializer;

  @Before
  public void setUp() {
    deserializer = new CategoryDeserializer();
  }

  private Category deserialize(String json) throws IOException {
    return new ObjectMapper().readValue(json, Category.class);
  }

  @Test
  public void testCategoryDeserializer_validInput_giveValidObject() throws Exception {
    JSONObject requestBody = new JSONObject();
    requestBody.put("name", "Test");
    requestBody.put("type", "I");
    requestBody.put("description", "Desc");
    requestBody.put("project", "1");

    Category result = deserialize(requestBody.toString());
    assertThat(result.getName()).isEqualTo("Test");
    assertThat(result.getDescription()).isEqualTo("Desc");
    assertThat(result.getType()).isEqualTo("I");
    assertThat(result.getProject().getId()).isEqualTo(1L);
  }

  @Test
  public void testCategoryDeserializer_missingInput_throwException() throws Exception {
    // Category result = deserialize("{ }");
  }
}
