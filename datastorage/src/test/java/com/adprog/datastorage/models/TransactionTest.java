package com.adprog.datastorage.models;

import static org.assertj.core.api.Assertions.assertThat;

import java.sql.Date;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TransactionTest {

  private Validator validator;

  @BeforeEach
  public void setUp() {
    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    validator = factory.getValidator();
    System.out.println(validator);
  }

  @Test
  public void testTransactionModel_validTransaction_noConstraintViolations() {
    Transaction transaction = new Transaction();
    transaction.setProject(new Project(1L));
    transaction.setCategory(new Category(new Project(1L), "Mock", "Mock", "I"));
    transaction.setTitle("Test");
    transaction.setDescription("Test");
    transaction.setAmount(3000L);
    transaction.setTransactionDate(Date.valueOf("3000-09-11"));

    Set<ConstraintViolation<Transaction>> violations = validator.validate(transaction);
    assertThat(violations.isEmpty());
  }

  @Test
  public void testTransactionModel_nullValues_throwViolations() {
    Transaction transaction = new Transaction();

    Set<ConstraintViolation<Transaction>> violations = validator.validate(transaction);
    assertThat(violations)
        .anyMatch(v -> v.getMessage().contains("title") && v.getMessage().contains("null"));
    assertThat(violations)
        .anyMatch(v -> v.getMessage().contains("description") && v.getMessage().contains("null"));
    assertThat(violations)
        .anyMatch(v -> v.getMessage().contains("amount") && v.getMessage().contains("null"));
    assertThat(violations).anyMatch(v -> v.getMessage().contains("Project"));
    assertThat(violations).anyMatch(v -> v.getMessage().contains("Category"));
  }

  @Test
  public void testTransactionModel_invalidLengthProperty_throwViolations() {
    Transaction transaction = new Transaction();
    transaction.setTitle("");
    transaction.setDescription("");

    Set<ConstraintViolation<Transaction>> violations = validator.validate(transaction);
    assertThat(violations)
        .anyMatch(v -> v.getMessage().contains("title") && v.getMessage().contains("length"));
  }

  @Test
  public void testTransactionModel_invalidAmount_throwViolations() {
    Transaction transaction = new Transaction();
    transaction.setAmount(-1L);

    Set<ConstraintViolation<Transaction>> violations = validator.validate(transaction);
    assertThat(violations)
        .anyMatch(v -> v.getMessage().contains("amount") && v.getMessage().contains("negative"));
  }

  private Long getCurrentTime() {
    return Calendar.getInstance().getTimeInMillis();
  }

  private Long getFutureTime() {
    int nextYear = Calendar.getInstance().get(Calendar.YEAR) + 1;
    Calendar time = Calendar.getInstance();
    time.set(Calendar.YEAR, nextYear);
    return time.getTimeInMillis();
  }

  @Test
  public void testTransactionModel_futureTransactionDate_throwViolations() {
    Transaction transaction = new Transaction();
    transaction.setTransactionDate(new Date(getFutureTime()));

    Set<ConstraintViolation<Transaction>> violations = validator.validate(transaction);
    assertThat(violations).anyMatch(v -> v.getMessage().contains("Transaction date"));
  }

  @Test
  public void testTransactionModelModifyMethod_changeProperties() {
    Transaction old = new Transaction();

    Map<String, Object> data = new HashMap<>();
    data.put("title", "New Title");
    data.put("description", "New desc");
    data.put("amount", 200L);
    data.put("transactionDate", "2020-01-01");
    data.put("category", 420L);

    old.modify(data);

    assertThat(old.getTitle()).isEqualTo("New Title");
    assertThat(old.getDescription()).isEqualTo("New desc");
    assertThat(old.getAmount()).isEqualTo(200L);
    assertThat(old.getTransactionDate()).isEqualTo(Date.valueOf("2020-01-01"));
    assertThat(old.getCategory().getId()).isEqualTo(420L);
  }
}
