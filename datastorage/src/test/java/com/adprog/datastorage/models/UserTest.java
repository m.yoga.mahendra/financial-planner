package com.adprog.datastorage.models;

import static org.junit.Assert.assertEquals;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class UserTest {

  private Validator validator;

  @BeforeEach
  public void setUp() {
    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    validator = factory.getValidator();
    System.out.println(validator);
  }

  @Test
  public void testUserModel_constructors_gettersetter() {
    User user = new User("username", "email@test.com", "password123", "username full");
    assertEquals(user.getUsername(), "username");
    assertEquals(user.getEmail(), "email@test.com");
    assertEquals(user.getPassword(), "password123");
    assertEquals(user.getFullName(), "username full");
  }
}
