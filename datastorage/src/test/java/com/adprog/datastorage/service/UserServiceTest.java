package com.adprog.datastorage.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.adprog.datastorage.controller.stub.UserDtoStub;
import com.adprog.datastorage.exception.UserAlreadyExistException;
import com.adprog.datastorage.models.User;
import com.adprog.datastorage.models.UserDto;
import com.adprog.datastorage.repository.UserRepository;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {
  @Mock private PasswordEncoder encoder;

  @Mock private UserRepository repository;

  @InjectMocks private UserService service;

  private User user1, user2;

  private void mockEntities() {
    user1 = new User("admin", "admin@gmail.com", "admin", "Admin");
    user2 = new User("cykes", "cykes@gmail.com", "admin", "Cykes Bin Admin");
  }

  @BeforeEach
  public void setUp() {
    mockEntities();
  }

  @Test
  public void testAuthServiceGetEntities_shouldReturnUserLists() {
    when(repository.findAll()).thenReturn(Arrays.asList(user1, user2));

    List<User> result = service.getEntities();
    assertThat(result).containsExactlyInAnyOrder(user1, user2);
  }

  @Test
  public void testAuthServiceGetEntity_validUsername_shouldReturnUserEntity() {
    when(repository.findByUsername("admin")).thenReturn(Optional.of(user1));

    User result = service.getEntity("admin");
    assertThat(result).isEqualTo(user1);
  }

  @Test
  public void testAuthServiceGetEntity_invalidUsername_shouldThrowUsernameNotFoundException() {
    when(repository.findByUsername("admin2")).thenThrow(UsernameNotFoundException.class);

    assertThatExceptionOfType(UsernameNotFoundException.class)
        .isThrownBy(() -> service.getEntity("admin2"));
  }

  @Test
  public void testAuthServicePostEntity_newUser_shouldReturnNewUser() {
    when(repository.saveAndFlush(any(User.class)))
        .thenAnswer(invocation -> invocation.getArgument(0));
    UserDto newUser =
        new UserDtoStub()
            .email("john@gmail.com")
            .username("John")
            .password("john")
            .matchingPassword("john")
            .fullName("Johnny");

    User result = service.postEntity(newUser);
    assertThat(result.getFullName()).isNotNull();
  }

  @Test
  public void testAuthServicePostEntity_existingUsername_shouldThrowUserAlreadyExistException() {
    when(repository.findByUsername("admin")).thenReturn(Optional.of(user1));
    UserDto newUser =
        new UserDtoStub()
            .email("john@gmail.com")
            .username("admin")
            .password("john")
            .matchingPassword("john")
            .fullName("New Admin");

    assertThatExceptionOfType(UserAlreadyExistException.class)
        .isThrownBy(() -> service.postEntity(newUser));
  }

  @Test
  public void testAuthServicePostEntity_existingEmail_shouldThrowUserAlreadyExistException() {
    when(repository.findByEmail("cykes@gmail.com")).thenReturn(Optional.of(user2));
    UserDto newUser =
        new UserDtoStub()
            .email("cykes@gmail.com")
            .username("Cykes2")
            .password("john")
            .matchingPassword("john")
            .fullName("New Admin");

    assertThatExceptionOfType(UserAlreadyExistException.class)
        .isThrownBy(() -> service.postEntity(newUser));
  }

  @Test
  public void testAuthServicePatchEntity_validUsername_shouldCallModifyMethod() {
    when(repository.findByUsername("admin")).thenReturn(Optional.of(user1));
    when(repository.saveAndFlush(any(User.class)))
        .thenAnswer(invocation -> invocation.getArgument(0));

    User result = service.patchEntity("admin", new HashMap<>());
    assertThat(result).isEqualTo(user1);
  }

  @Test
  public void testAuthServicePatchEntity_invalidUsername_shouldThrowUsernameNotFoundException() {
    when(repository.findByUsername("admin2")).thenReturn(Optional.empty());

    assertThatExceptionOfType(UsernameNotFoundException.class)
        .isThrownBy(() -> service.patchEntity("admin2", new HashMap<>()));
  }

  @Test
  public void testAuthServiceDeleteEntity_validUsername_shouldCallDeleteMethod() {
    when(repository.findByUsername("admin")).thenReturn(Optional.of(user1));
    service.deleteEntity("admin");

    verify(repository, Mockito.times(1)).delete(user1);
  }

  @Test
  public void testAuthServiceDeleteEntity_invalidUsername_shouldThrowUsernameNotFoundException() {
    when(repository.findByUsername("admin2")).thenReturn(Optional.empty());
    assertThatExceptionOfType(UsernameNotFoundException.class)
        .isThrownBy(() -> service.deleteEntity("admin2"));
  }
}
