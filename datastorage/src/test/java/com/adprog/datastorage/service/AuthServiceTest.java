package com.adprog.datastorage.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.lenient;

import com.adprog.datastorage.controller.stub.ProjectStub;
import com.adprog.datastorage.controller.stub.UserStub;
import com.adprog.datastorage.models.*;
import com.adprog.datastorage.repository.UserRepository;
import java.util.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@ExtendWith(MockitoExtension.class)
public class AuthServiceTest {
  @Mock private UserRepository repository;

  @InjectMocks private AuthService service;

  @BeforeEach
  public void setUp() {
    Project mockProject1 = new ProjectStub().id(1L);
    Project mockProject2 = new ProjectStub().id(2L);

    Role mockRole1 =
        new Role(
            mockProject1,
            new HashSet<>(Arrays.asList(AuthorityType.GET, AuthorityType.POST)),
            "Mock 1",
            "First Mock");
    Role mockRole2 =
        new Role(
            mockProject2,
            new HashSet<>(Arrays.asList(AuthorityType.GET, AuthorityType.POST)),
            "Mock 2",
            "Second Mock");
    Role mockRole3 =
        new Role(
            mockProject1,
            new HashSet<>(
                Arrays.asList(
                    AuthorityType.GET,
                    AuthorityType.POST,
                    AuthorityType.PATCH,
                    AuthorityType.DELETE)),
            "Mock 3",
            "Third Mock");

    User mockAdmin =
        new UserStub()
            .username("admin")
            .email("admin@gmail.com")
            .password("admin")
            .fullName("Admin bin Admin")
            .roles(new ArrayList<>(Arrays.asList(mockRole1, mockRole2, mockRole3)));

    UserRole mockUserRole1 = new UserRole(mockAdmin, mockRole1);
    UserRole mockUserRole2 = new UserRole(mockAdmin, mockRole2);
    UserRole mockUserRole3 = new UserRole(mockAdmin, mockRole3);

    lenient().when(repository.findByUsername("admin")).thenReturn(Optional.of(mockAdmin));
    lenient().when(repository.findByUsername("admin2")).thenReturn(Optional.empty());
    lenient()
        .when(repository.findAllUserRole("admin"))
        .thenReturn(Arrays.asList(mockUserRole1, mockUserRole2, mockUserRole3));
  }

  @Test
  public void testUserServiceLoadUserByUsername_validUser_shouldReturnRepresentationOfUser() {
    UserDetails result = service.loadUserByUsername("admin");
    assertThat(result.getUsername()).isEqualTo("admin");
    assertThat(result.getPassword()).isEqualTo("admin");
    assertThat(result.isEnabled()).isEqualTo(true);
    assertThat(result.isAccountNonLocked()).isEqualTo(true);
    assertThat(result.isAccountNonExpired()).isEqualTo(true);
    assertThat(result.isCredentialsNonExpired()).isEqualTo(true);

    Collection<? extends GrantedAuthority> authList = result.getAuthorities();
    assertThat(authList).anyMatch(auth -> auth.getAuthority().equals("1_GET"));
    assertThat(authList).anyMatch(auth -> auth.getAuthority().equals("1_POST"));
    assertThat(authList).anyMatch(auth -> auth.getAuthority().equals("1_PATCH"));
    assertThat(authList).anyMatch(auth -> auth.getAuthority().equals("1_DELETE"));
    assertThat(authList).anyMatch(auth -> auth.getAuthority().equals("2_GET"));
    assertThat(authList).anyMatch(auth -> auth.getAuthority().equals("2_POST"));
  }

  @Test
  public void testUserServiceLoadUserByUsername_invalidUser_shouldReturnRepresentationOfUser() {
    assertThatExceptionOfType(UsernameNotFoundException.class)
        .isThrownBy(() -> service.loadUserByUsername("admin2"));
  }
}
