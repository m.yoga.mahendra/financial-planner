package com.adprog.datastorage.service;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import com.adprog.datastorage.exception.ProjectNotFoundException;
import com.adprog.datastorage.models.Project;
import com.adprog.datastorage.models.Role;
import com.adprog.datastorage.models.User;
import com.adprog.datastorage.models.UserRole;
import com.adprog.datastorage.repository.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ProjectServiceTest {

  @Mock private TransactionRepository transactionRepo;

  @Mock private CategoryRepository categoryRepo;

  @Mock private ProjectRepository projectRepo;

  @Mock private RoleRepository roleRepo;

  @Mock private UserRoleRepository userRoleRepository;

  @InjectMocks private ProjectServiceImpl service;

  private Project project1, project2, project3;

  @BeforeEach
  public void setUp() {
    project1 = new Project(1L);
    project2 = new Project(2L);
    project3 = new Project(3L);

    lenient().when(projectRepo.existsById(eq(1L))).thenReturn(true);
    lenient().when(projectRepo.existsById(eq(2L))).thenReturn(true);
    lenient().when(projectRepo.existsById(eq(3L))).thenReturn(true);
    lenient().when(projectRepo.existsById(eq(4L))).thenReturn(false);

    lenient().when(projectRepo.findAll()).thenReturn(Arrays.asList(project1, project2, project3));
    lenient()
        .when(projectRepo.findAllByUser(eq("user1")))
        .thenReturn(Arrays.asList(project1, project3));
    lenient()
        .when(projectRepo.findAllByUser(eq("user2")))
        .thenReturn(Arrays.asList(project2, project3));
    lenient().when(projectRepo.findById(eq(1L))).thenReturn(Optional.of(project1));
    lenient().when(projectRepo.findById(eq(2L))).thenReturn(Optional.of(project2));
    lenient().when(projectRepo.findById(eq(3L))).thenReturn(Optional.of(project3));
    lenient()
        .when(projectRepo.saveAndFlush(any(Project.class)))
        .thenAnswer(invocation -> invocation.getArgument(0));
  }

  @Test
  public void testServiceProjectGetEntities_noParameter_shouldReturnListOfAllProjects() {
    List<Project> result = service.getEntities();

    assertThat(result).containsExactlyInAnyOrder(project1, project2, project3);
  }

  @Test
  public void
      testServiceProjectGetEntities_usernameParameter_shouldReturnListOfProjectsAccessibleByThatUser() {
    List<Project> result;

    result = service.getEntities("user1");
    assertThat(result).containsExactlyInAnyOrder(project1, project3);

    result = service.getEntities("user2");
    assertThat(result).containsExactlyInAnyOrder(project2, project3);
  }

  @Test
  public void testServiceProjectGetEntity_validProject_shouldReturnProject() {
    Project result = service.getEntity(1L);
    assertThat(result).isEqualTo(project1);
  }

  @Test
  public void testServiceProjectGetEntity_invalidProject_shouldThrowProjectNotFoundException() {
    assertThatExceptionOfType(ProjectNotFoundException.class)
        .isThrownBy(() -> service.getEntity(4L));
  }

  @Test
  public void testServicePostProject_validProject_shouldReturnNewProject() {
    Project newProject = new Project("Sample Project", "A new project");
    Project result = service.postEntity(newProject, new User("a", "a", "a", "a"));

    assertThat(result).isEqualTo(newProject);
  }

  @Test
  public void testServicePostProject_validProject_shouldCallSaveProjectAndSaveRole() {
    when(userRoleRepository.saveAndFlush(any(UserRole.class)))
        .thenAnswer(invocation -> invocation.getArgument(0));

    Project newProject = new Project("Sample Project", "A new project");
    service.postEntity(newProject, new User("a", "a", "a", "a"));

    verify(projectRepo, times(1)).saveAndFlush(newProject);
    verify(roleRepo, times(2)).saveAndFlush(any(Role.class));
  }

  @Test
  public void testServicePatchProject_validProject_shouldCallModifyMethod() {
    service.patchEntity(1L, new HashMap<>()); // TODO: How to verify this?
  }

  @Test
  public void testServicePatchProject_invalidProject_shouldThrowProjectNotFoundException() {
    assertThatExceptionOfType(ProjectNotFoundException.class)
        .isThrownBy(() -> service.patchEntity(4L, new HashMap<>()));
  }

  @Test
  public void testServiceDeleteProject_validProject_shouldCallDeleteMethod() {
    service.deleteEntity(1L);

    verify(projectRepo, times(1)).deleteById(1L);
  }

  @Test
  public void testServiceDeleteProject_invalidProject_shouldThrowProjectNotFoundException() {
    assertThatExceptionOfType(ProjectNotFoundException.class)
        .isThrownBy(() -> service.deleteEntity(4L));
  }
}
