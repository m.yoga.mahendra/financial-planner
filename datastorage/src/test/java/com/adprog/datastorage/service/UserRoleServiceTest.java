package com.adprog.datastorage.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import com.adprog.datastorage.controller.stub.ProjectStub;
import com.adprog.datastorage.controller.stub.RoleStub;
import com.adprog.datastorage.controller.stub.UserStub;
import com.adprog.datastorage.exception.ProjectNotFoundException;
import com.adprog.datastorage.exception.RoleNotFoundException;
import com.adprog.datastorage.exception.UserNotFoundException;
import com.adprog.datastorage.models.*;
import com.adprog.datastorage.repository.ProjectRepository;
import com.adprog.datastorage.repository.RoleRepository;
import com.adprog.datastorage.repository.UserRepository;
import com.adprog.datastorage.repository.UserRoleRepository;
import java.util.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class UserRoleServiceTest {

  @Mock private UserRepository userRepository;

  @Mock private ProjectRepository projectRepository;

  @Mock private RoleRepository roleRepository;

  @Mock private UserRoleRepository userRoleRepository;

  @InjectMocks private UserRoleService service;

  private Role mockRole;

  private User mockUser1, mockUser2, mockUser3;

  private void mockEntities() {
    Project mockProject = new ProjectStub().id(1L);

    mockUser1 = new UserStub().username("admin").roles(Collections.singletonList(mockRole));

    mockUser2 = new UserStub().username("admin2").roles(Collections.singletonList(mockRole));

    mockUser3 = new UserStub().username("admin3").roles(Collections.emptyList());

    mockRole =
        new RoleStub()
            .id(1L)
            .name("Role")
            .project(mockProject)
            .users(new HashSet<>(Arrays.asList(mockUser1, mockUser2)));
  }

  @BeforeEach
  public void setUp() {
    mockEntities();
  }

  @Test
  public void testUserRoleServiceGetEntities_validRole_shouldReturnListOfEntities() {
    when(projectRepository.existsById(1L)).thenReturn(true);
    when(roleRepository.existsInProject(1L, 1L)).thenReturn(true);
    when(userRepository.findAllWithRole(1L, 1L)).thenReturn(Arrays.asList(mockUser1, mockUser2));

    List<User> result = service.getEntities(1L, 1L);
    assertThat(result).containsExactlyInAnyOrder(mockUser1, mockUser2);
  }

  @Test
  public void testUserRoleServiceGetEntities_invalidRole_shouldThrowRoleNotFoundException() {
    when(projectRepository.existsById(1L)).thenReturn(true);
    when(roleRepository.existsInProject(1L, 3L)).thenReturn(false);

    assertThatExceptionOfType(RoleNotFoundException.class)
        .isThrownBy(() -> service.getEntities(1L, 3L));
  }

  @Test
  public void testUserRoleServiceGetEntities_invalidProject_shouldThrowProjectNotFoundException() {
    when(projectRepository.existsById(2L)).thenReturn(false);

    assertThatExceptionOfType(ProjectNotFoundException.class)
        .isThrownBy(() -> service.getEntities(2L, 3L));
  }

  @Test
  public void testUserRoleServiceGetEntity_validUsername_shouldReturnUser() {
    when(projectRepository.existsById(1L)).thenReturn(true);
    when(roleRepository.existsInProject(1L, 1L)).thenReturn(true);
    when(userRepository.findOneWithRole(1L, 1L, "admin")).thenReturn(Optional.of(mockUser1));

    assertThat(service.getEntity(1L, 1L, "admin")).isEqualTo(mockUser1);
  }

  @Test
  public void testUserRoleServiceGetEntity_invalidUsername_shouldThrowUserNotFoundException() {
    when(projectRepository.existsById(1L)).thenReturn(true);
    when(roleRepository.existsInProject(1L, 1L)).thenReturn(true);
    when(userRepository.findOneWithRole(1L, 1L, "admin4")).thenReturn(Optional.empty());

    assertThatExceptionOfType(UserNotFoundException.class)
        .isThrownBy(() -> service.getEntity(1L, 1L, "admin4"));
  }

  @Test
  public void testUserRoleServiceGetEntity_invalidRole_shouldThrowRoleNotFoundException() {
    when(projectRepository.existsById(1L)).thenReturn(true);
    when(roleRepository.existsInProject(1L, 3L)).thenReturn(false);

    assertThatExceptionOfType(RoleNotFoundException.class)
        .isThrownBy(() -> service.getEntity(1L, 3L, "admin"));
  }

  @Test
  public void testUserRoleServiceGetEntity_invalidProject_shouldThrowProjectNotFoundException() {
    when(projectRepository.existsById(2L)).thenReturn(false);

    assertThatExceptionOfType(ProjectNotFoundException.class)
        .isThrownBy(() -> service.getEntity(2L, 1L, "admin"));
  }

  @Test
  public void testUserRoleServicePostEntity_validUsername_shouldReturnNewListOfUsers() {
    when(projectRepository.existsById(1L)).thenReturn(true);
    when(roleRepository.existsInProject(1L, 1L)).thenReturn(true);
    when(roleRepository.findOneByProject(1L, 1L)).thenReturn(Optional.of(mockRole));
    when(userRepository.findById("admin3")).thenReturn(Optional.of(mockUser3));
    when(userRoleRepository.saveAndFlush(any(UserRole.class)))
        .thenAnswer(invocation -> invocation.getArgument(0));

    Role result = service.postEntity(1L, 1L, "admin3");
    assertThat(result).isEqualTo(mockRole);
    assertThat(mockRole.getUsers()).containsExactlyInAnyOrder(mockUser1, mockUser2);
  }

  @Test
  public void testUserRoleServicePostEntity_invalidUsername_shouldThrowUserNotFoundException() {
    when(projectRepository.existsById(1L)).thenReturn(true);
    when(roleRepository.existsInProject(1L, 1L)).thenReturn(true);
    when(userRepository.findById("admin4")).thenReturn(Optional.empty());

    assertThatExceptionOfType(UserNotFoundException.class)
        .isThrownBy(() -> service.postEntity(1L, 1L, "admin4"));
  }

  @Test
  public void testUserRoleServicePostEntity_invalidRole_shouldThrowRoleNotFoundException() {
    when(projectRepository.existsById(1L)).thenReturn(true);
    when(roleRepository.existsInProject(1L, 3L)).thenReturn(false);

    assertThatExceptionOfType(RoleNotFoundException.class)
        .isThrownBy(() -> service.postEntity(1L, 3L, "admin3"));
  }

  @Test
  public void testUserRoleServicePostEntity_invalidProject_shouldThrowProjectNotFoundException() {
    when(projectRepository.existsById(2L)).thenReturn(false);

    assertThatExceptionOfType(ProjectNotFoundException.class)
        .isThrownBy(() -> service.postEntity(2L, 1L, "admin3"));
  }

  @Test
  public void testUserRoleServiceDeleteEntity_validUsername_shouldDeleteUser() {
    when(projectRepository.existsById(1L)).thenReturn(true);
    when(roleRepository.existsInProject(1L, 1L)).thenReturn(true);
    when(roleRepository.findOneByProject(1L, 1L)).thenReturn(Optional.of(mockRole));
    when(userRepository.findOneWithRole(1L, 1L, "admin2")).thenReturn(Optional.of(mockUser2));

    service.deleteEntity(1L, 1L, "admin2");
    verify(userRoleRepository, times(1)).deleteById(any(UserRoleKey.class));
  }

  @Test
  public void testUserRoleServiceDeleteEntity_invalidUsername_shouldThrowUserNotFoundException() {
    when(projectRepository.existsById(1L)).thenReturn(true);
    when(roleRepository.existsInProject(1L, 1L)).thenReturn(true);
    when(userRepository.findOneWithRole(1L, 1L, "admin4")).thenReturn(Optional.empty());

    assertThatExceptionOfType(UserNotFoundException.class)
        .isThrownBy(() -> service.deleteEntity(1L, 1L, "admin4"));
  }

  @Test
  public void testUserRoleServiceDeleteEntity_invalidRole_shouldThrowRoleNotFoundException() {
    when(projectRepository.existsById(1L)).thenReturn(true);
    when(roleRepository.existsInProject(1L, 3L)).thenReturn(false);

    assertThatExceptionOfType(RoleNotFoundException.class)
        .isThrownBy(() -> service.deleteEntity(1L, 3L, "admin2"));
  }

  @Test
  public void testUserRoleServiceDeleteEntity_invalidProject_shouldThrowProjectNotFoundException() {
    when(projectRepository.existsById(2L)).thenReturn(false);

    assertThatExceptionOfType(ProjectNotFoundException.class)
        .isThrownBy(() -> service.deleteEntity(2L, 1L, "admin2"));
  }
}
