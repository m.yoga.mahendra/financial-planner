package com.adprog.datastorage.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.*;

import com.adprog.datastorage.exception.ProjectNotFoundException;
import com.adprog.datastorage.exception.TransactionNotFoundException;
import com.adprog.datastorage.models.Category;
import com.adprog.datastorage.models.Project;
import com.adprog.datastorage.models.Transaction;
import com.adprog.datastorage.query.api.GetAllQueryBuilder;
import com.adprog.datastorage.query.api.TransactionGetAllQueryBuilder;
import com.adprog.datastorage.repository.CategoryRepository;
import com.adprog.datastorage.repository.ProjectRepository;
import com.adprog.datastorage.repository.TransactionRepository;
import java.sql.Date;
import java.util.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ExtendWith(MockitoExtension.class)
public class TransactionServiceTest {
  @Autowired private TestEntityManager em;

  @Mock private CategoryRepository categoryRepository;

  @Mock private TransactionRepository transactionRepository;

  @Mock private ProjectRepository projectRepository;

  @InjectMocks private TransactionService service;

  private Project mockProject;

  private Category income, expense;
  private Transaction tra1, tra2, tra3;

  @BeforeEach
  public void setUp() {
    lenient().when(projectRepository.existsById(1L)).thenReturn(true);
    lenient().when(projectRepository.existsById(2L)).thenReturn(true);
    lenient().when(projectRepository.existsById(3L)).thenReturn(false);

    mockProject = new Project(1L);

    income = new Category(mockProject, "Income", "Income", "I");
    expense = new Category(new Project(2L), "Expense", "Expense", "E");

    tra1 = new Transaction(mockProject, income, "Transaction 1", "Desc 1", 10L, new Date(0L));
    tra2 = new Transaction(new Project(2L), expense, "Transaction 2", "Desc 2", 20L, new Date(0L));
    tra3 = new Transaction(mockProject, income, "Transaction 3", "Desc 3", 30L, new Date(0L));

    lenient().when(categoryRepository.getOne(1L)).thenReturn(income);

    lenient()
        .when(transactionRepository.findAllByProject(1L))
        .thenReturn(Arrays.asList(tra1, tra3));
    lenient()
        .when(transactionRepository.findAllByProject(2L))
        .thenReturn(Collections.singletonList(tra2));
    lenient().when(transactionRepository.findOneByProject(1L, 1L)).thenReturn(Optional.of(tra1));
    lenient().when(transactionRepository.findOneByProject(1L, 2L)).thenReturn(Optional.empty());
    lenient().when(transactionRepository.findOneByProject(3L, 3L)).thenReturn(Optional.empty());
    lenient().when(transactionRepository.findOneByProject(1L, 4L)).thenReturn(Optional.empty());
    lenient().when(transactionRepository.findOneByProject(3L, 2L)).thenReturn(Optional.empty());
    lenient()
        .when(transactionRepository.saveAndFlush(any(Transaction.class)))
        .thenAnswer(invocation -> invocation.getArgument(0));
  }

  @Test
  public void testServiceTransactionGetEntities_validProject_shouldReturnListOfTransactions() {
    GetAllQueryBuilder<Transaction> mockBuilder = Mockito.mock(TransactionGetAllQueryBuilder.class);
    when(mockBuilder.buildQuery(null)).thenReturn(Arrays.asList(tra1, tra3));
    List<Transaction> result = service.getEntities(1L, mockBuilder);
    assertThat(result).containsExactlyInAnyOrder(tra1, tra3);
  }

  @Test
  public void
      testServiceTransactionGetEntities_invalidProject_shouldThrowProjectNotFoundException() {
    assertThatExceptionOfType(ProjectNotFoundException.class)
        .isThrownBy(() -> service.getEntities(3L, new TransactionGetAllQueryBuilder(3L)));
  }

  @Test
  public void testServiceTransactionGetEntity_validTransaction_shouldReturnTransaction() {
    Transaction result = service.getEntity(1L, 1L);
    assertThat(result).isEqualTo(tra1);
  }

  @Test
  public void
      testServiceTransactionGetEntity_invalidTransaction_shouldThrowTransactionNotFoundException() {
    assertThatExceptionOfType(TransactionNotFoundException.class)
        .isThrownBy(
            () -> service.getEntity(1L, 2L)); // Valid project and transaction, but don't match
    assertThatExceptionOfType(TransactionNotFoundException.class)
        .isThrownBy(() -> service.getEntity(1L, 2L)); // Valid project, but invalid transaction
  }

  @Test
  public void testServiceTransactionGetEntity_invalidProject_shouldThrowProjectNotFoundException() {
    assertThatExceptionOfType(ProjectNotFoundException.class)
        .isThrownBy(() -> service.getEntity(3L, 3L)); // Invalid project and transaction
    assertThatExceptionOfType(ProjectNotFoundException.class)
        .isThrownBy(() -> service.getEntity(3L, 2L)); // Valid transaction, but invalid project
  }

  @Test
  public void testServiceTransactionPostEntity_validInput_shouldReturnNewTransaction() {
    Transaction newTransaction =
        new Transaction(mockProject, income, "New", "New", 42L, new Date(0L));
    Transaction result = service.postEntity(1L, newTransaction);
    assertThat(result).isEqualTo(newTransaction);
  }

  @Test
  public void
      testServiceTransactionPostEntity_projectIdDoNotMatch_shouldThrowIllegalArgumentException() {
    Transaction newTransaction =
        new Transaction(mockProject, income, "New", "New", 42L, new Date(0L));
    assertThatExceptionOfType(IllegalArgumentException.class)
        .isThrownBy(() -> service.postEntity(2L, newTransaction));
  }

  @Test
  public void
      testServiceTransactionPostEntity_invalidProject_shouldThrowProjectNotFoundException() {
    assertThatExceptionOfType(ProjectNotFoundException.class)
        .isThrownBy(() -> service.postEntity(3L, new Transaction()));
  }

  @Test
  public void testServiceTransactionPatchEntity_validTransaction_shouldCallModifyMethod() {
    service.patchEntity(1L, 1L, new HashMap<>());
  }

  @Test
  public void
      testServiceTransactionPatchEntity_invalidTransaction_shouldThrowTransactionNotFoundException() {
    assertThatExceptionOfType(TransactionNotFoundException.class)
        .isThrownBy(() -> service.patchEntity(1L, 4L, new HashMap<>()));
  }

  @Test
  public void
      testServiceTransactionPatchEntity_invalidProject_shouldThrowProjectNotFoundException() {
    assertThatExceptionOfType(ProjectNotFoundException.class)
        .isThrownBy(() -> service.patchEntity(3L, 1L, new HashMap<>()));
  }

  @Test
  public void testServiceTransactionDeleteEntity_validTransaction_shouldCallDeleteMethod() {
    service.deleteEntity(1L, 1L);

    verify(transactionRepository).delete(tra1);
  }

  @Test
  public void
      testServiceTransactionDeleteEntity_invalidTransaction_shouldThrowTransactionNotFoundException() {
    assertThatExceptionOfType(TransactionNotFoundException.class)
        .isThrownBy(() -> service.deleteEntity(1L, 4L));
  }

  @Test
  public void
      testServiceTransactionDeleteEntity_invalidProject_shouldThrowProjectNotFoundException() {
    assertThatExceptionOfType(ProjectNotFoundException.class)
        .isThrownBy(() -> service.deleteEntity(3L, 1L));
  }
}
