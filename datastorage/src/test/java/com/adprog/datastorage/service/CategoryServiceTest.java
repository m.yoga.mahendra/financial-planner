package com.adprog.datastorage.service;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import com.adprog.datastorage.exception.CategoryNotFoundException;
import com.adprog.datastorage.exception.ProjectNotFoundException;
import com.adprog.datastorage.models.Category;
import com.adprog.datastorage.models.Project;
import com.adprog.datastorage.query.api.CategoryGetAllQueryBuilder;
import com.adprog.datastorage.query.api.GetAllQueryBuilder;
import com.adprog.datastorage.repository.CategoryRepository;
import com.adprog.datastorage.repository.ProjectRepository;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ExtendWith(MockitoExtension.class)
public class CategoryServiceTest {
  @Autowired private TestEntityManager em;

  @Mock private ProjectRepository projectRepository;

  @Mock private CategoryRepository categoryRepository;

  @InjectMocks private CategoryService service;

  private Category cat1, cat2;

  @BeforeEach
  public void setUp() {
    lenient().when(projectRepository.existsById(1L)).thenReturn(true);
    lenient().when(projectRepository.existsById(2L)).thenReturn(false);

    cat1 = new Category(new Project(1L), "Cat 1", "Desc 1", "I");
    cat2 = new Category(new Project(1L), "Cat 2", "Desc 2", "E");
    lenient().when(categoryRepository.findAllByProject(1L)).thenReturn(Arrays.asList(cat1, cat2));
    lenient().when(categoryRepository.findOneByProject(1L, 1L)).thenReturn(Optional.of(cat1));
    lenient().when(categoryRepository.findOneByProject(1L, 3L)).thenReturn(Optional.empty());
    lenient().when(categoryRepository.findOneByProject(2L, 1L)).thenReturn(Optional.empty());
    lenient().when(categoryRepository.findOneByProject(2L, 3L)).thenReturn(Optional.empty());
    lenient()
        .when(categoryRepository.saveAndFlush(any(Category.class)))
        .thenAnswer(invocation -> invocation.getArgument(0));
  }

  @Test
  public void testServiceCategoryGetEntities_validId_returnCategory() {
    GetAllQueryBuilder<Category> mockBuilder = Mockito.mock(CategoryGetAllQueryBuilder.class);
    when(mockBuilder.buildQuery(null)).thenReturn(Arrays.asList(cat1, cat2));
    List<Category> result = service.getEntities(1L, mockBuilder);
    assertThat(result).containsExactlyInAnyOrder(cat1, cat2);
  }

  @Test
  public void testServiceCategoryGetEntities_invalidId_throwProjectNotFoundException() {
    assertThatExceptionOfType(ProjectNotFoundException.class)
        .isThrownBy(() -> service.getEntities(2L, new CategoryGetAllQueryBuilder(2L)));
  }

  @Test
  public void testServiceCategoryGetEntity_validId_returnCategory() {
    Category result = service.getEntity(1L, 1L);
    assertThat(result).isNotNull();
    assertThat(result).isEqualTo(cat1);
  }

  @Test
  public void testServiceCategoryGetEntity_invalidId_throwCategoryNotFoundException() {
    assertThatExceptionOfType(CategoryNotFoundException.class)
        .isThrownBy(() -> service.getEntity(1L, 3L));
    assertThatExceptionOfType(ProjectNotFoundException.class)
        .isThrownBy(() -> service.getEntity(2L, 1L));
    assertThatExceptionOfType(ProjectNotFoundException.class)
        .isThrownBy(() -> service.getEntity(2L, 3L));
  }

  @Test
  public void testServiceCategoryPostEntity_returnNewCategory() {
    Category newCat = new Category(new Project(1L), "Cat3", "Desc3", "I");
    Category result = service.postEntity(1L, newCat);
    assertThat(result).isEqualTo(newCat);
    verify(categoryRepository).saveAndFlush(newCat);
  }

  @Test
  public void testServiceCategoryPostEntity_invalidProject_throwProjectNotFound() {
    Category newCat = new Category(new Project(1L), "Cat3", "Desc3", "I");
    assertThatExceptionOfType(ProjectNotFoundException.class)
        .isThrownBy(() -> service.postEntity(2L, newCat));
  }

  @Test
  public void testServiceCategoryPostEntity_invalidCategory_throwProjectNotFound() {
    Category newCat = new Category(new Project(2L), "Cat3", "Desc3", "I");
    assertThatExceptionOfType(IllegalArgumentException.class)
        .isThrownBy(() -> service.postEntity(1L, newCat));
  }

  @Test
  public void testServiceCategoryPatchEntity_shouldCallModifyMethod() {
    Category result = service.patchEntity(1L, 1L, new HashMap<>());
  }

  @Test
  public void
      testServiceCategoryPatchEntity_invalidCategory_shouldThrowCategoryNotFoundException() {
    assertThatExceptionOfType(CategoryNotFoundException.class)
        .isThrownBy(() -> service.patchEntity(1L, 3L, new HashMap<>()));
  }

  @Test
  public void testServiceCategoryDeleteEntity_shouldCallDeleteMethod() {
    service.deleteEntity(1L, 1L);

    verify(categoryRepository).delete(cat1);
  }

  @Test
  public void
      testServiceCategoryDeleteEntity_invalidCategory_shouldThrowCategoryNotFoundException() {
    assertThatExceptionOfType(CategoryNotFoundException.class)
        .isThrownBy(() -> service.deleteEntity(1L, 3L));
  }
}
