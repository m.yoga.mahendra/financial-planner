package com.adprog.datastorage.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.*;

import com.adprog.datastorage.controller.stub.ProjectStub;
import com.adprog.datastorage.controller.stub.RoleStub;
import com.adprog.datastorage.exception.ProjectNotFoundException;
import com.adprog.datastorage.exception.RoleNotFoundException;
import com.adprog.datastorage.models.Project;
import com.adprog.datastorage.models.Role;
import com.adprog.datastorage.repository.ProjectRepository;
import com.adprog.datastorage.repository.RoleRepository;
import java.util.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class RoleServiceTest {
  @Mock private ProjectRepository projectRepository;

  @Mock private RoleRepository repository;

  @InjectMocks private RoleService service;

  private Project mockProject;

  private RoleStub mockRole1, mockRole2;

  private void mockEntities() {
    mockProject = new ProjectStub().id(1L);
    mockRole1 = new RoleStub().id(10L).name("Mock Role 1").project(mockProject);

    mockRole2 = new RoleStub().id(11L).name("Mock Role 2").project(mockProject);
  }

  @BeforeEach
  public void setUp() {
    mockEntities();
  }

  @Test
  public void testRoleServiceGetEntities_validId_shouldReturnListOfProjects() {
    when(projectRepository.existsById(1L)).thenReturn(true);
    when(repository.findAllByProject(1L)).thenReturn(Arrays.asList(mockRole1, mockRole2));

    List<Role> result = service.getEntities(1L, null);
    assertThat(result).containsExactlyInAnyOrder(mockRole1, mockRole2);
  }

  @Test
  public void testRoleServiceGetEntities_invalidProjectId_shouldThrowProjectNotFoundException() {
    when(projectRepository.existsById(2L)).thenReturn(false);

    assertThatExceptionOfType(ProjectNotFoundException.class)
        .isThrownBy(() -> service.getEntities(2L, null));
  }

  @Test
  public void testRoleServiceGetEntity_validRoleId_shouldReturnRole() {
    when(projectRepository.existsById(1L)).thenReturn(true);
    when(repository.findOneByProject(1L, 10L)).thenReturn(Optional.of(mockRole1));

    Role result = service.getEntity(1L, 10L);
    assertThat(result).isEqualTo(mockRole1);
  }

  @Test
  public void testRoleServiceGetEntity_invalidId_shouldThrowRoleNotFoundException() {
    when(projectRepository.existsById(1L)).thenReturn(true);
    when(repository.findOneByProject(1L, 12L)).thenReturn(Optional.empty());

    assertThatExceptionOfType(RoleNotFoundException.class)
        .isThrownBy(() -> service.getEntity(1L, 12L));
  }

  @Test
  public void testRoleServiceGetEntity_invalidProject_shouldThrowProjectNotFoundException() {
    when(projectRepository.existsById(2L)).thenReturn(false);

    assertThatExceptionOfType(ProjectNotFoundException.class)
        .isThrownBy(() -> service.getEntity(2L, 10L));
  }

  @Test
  public void testRoleServicePostEntity_validRole_shouldReturnNewRole() {
    when(projectRepository.existsById(1L)).thenReturn(true);
    when(repository.saveAndFlush(any(Role.class)))
        .thenAnswer(invocation -> invocation.getArgument(0));

    Role newRole = new RoleStub().id(1L).name("New Role").project(mockProject);
    Role result = service.postEntity(1L, newRole);

    verify(repository, times(1)).saveAndFlush(newRole);
    assertThat(result).isEqualTo(newRole);
  }

  @Test
  public void
      testRoleServicePostEntity_projectIdDoesNotMatch_shouldThrowIllegalArgumentException() {
    when(projectRepository.existsById(2L)).thenReturn(true);

    Role newRole = new RoleStub().id(1L).name("Mock Role 2").project(mockProject);
    assertThatExceptionOfType(IllegalArgumentException.class)
        .isThrownBy(() -> service.postEntity(2L, newRole));
  }

  @Test
  public void testRoleServicePatchEntity_validRole_shouldCallPatchMethod() {
    when(projectRepository.existsById(1L)).thenReturn(true);
    when(repository.findOneByProject(1L, 10L)).thenReturn(Optional.of(mockRole1));

    assertThat(service.patchEntity(1L, 10L, Collections.emptyMap()));
  }

  @Test
  public void testRoleServicePatchEntity_invalidRoleId_shouldThrowRoleNotFoundException() {
    when(projectRepository.existsById(1L)).thenReturn(true);
    when(repository.findOneByProject(1L, 12L)).thenReturn(Optional.empty());

    assertThatExceptionOfType(RoleNotFoundException.class)
        .isThrownBy(() -> service.patchEntity(1L, 12L, Collections.emptyMap()));
  }

  @Test
  public void testRoleServicePatchEntity_invalidProjectId_shouldThrowProjectNotFoundException() {
    when(projectRepository.existsById(12L)).thenReturn(false);

    assertThatExceptionOfType(ProjectNotFoundException.class)
        .isThrownBy(() -> service.patchEntity(12L, 12L, Collections.emptyMap()));
  }

  @Test
  public void testRoleServiceDeleteEntity_validRole_shouldCallDeleteMethod() {
    when(projectRepository.existsById(1L)).thenReturn(true);
    when(repository.existsInProject(1L, 10L)).thenReturn(true);

    service.deleteEntity(1L, 10L);

    verify(repository, times(1)).deleteById(10L);
  }

  @Test
  public void testRoleServiceDeleteEntity_invalidRole_shouldThrowRoleNotFoundException() {
    when(projectRepository.existsById(1L)).thenReturn(true);
    when(repository.existsInProject(1L, 12L)).thenReturn(false);

    assertThatExceptionOfType(RoleNotFoundException.class)
        .isThrownBy(() -> service.deleteEntity(1L, 12L));
  }

  @Test
  public void testRoleServiceDeleteEntity_invalidProject_shouldThrowProjectNotFoundException() {
    when(projectRepository.existsById(2L)).thenReturn(false);

    assertThatExceptionOfType(ProjectNotFoundException.class)
        .isThrownBy(() -> service.deleteEntity(2L, 10L));
  }
}
