package com.adprog.datastorage.controller.stub;

import com.adprog.datastorage.models.Project;
import java.sql.Timestamp;

public class ProjectStub extends Project {

  private Long id;

  private String title, description;
  private Timestamp lastUpdated, creationDate;

  public ProjectStub id(Long id) {
    this.id = id;
    return this;
  }

  public ProjectStub title(String title) {
    this.title = title;
    return this;
  }

  public ProjectStub description(String description) {
    this.description = description;
    return this;
  }

  public ProjectStub lastUpdated(Timestamp lastUpdated) {
    this.lastUpdated = lastUpdated;
    return this;
  }

  public ProjectStub creationDate(Timestamp creationDate) {
    this.creationDate = creationDate;
    return this;
  }

  @Override
  public Long getId() {
    return id;
  }

  @Override
  public String getTitle() {
    return title;
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public Timestamp getCreationDate() {
    return creationDate;
  }

  @Override
  public Timestamp getLastUpdated() {
    return lastUpdated;
  }
}
