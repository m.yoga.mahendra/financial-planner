package com.adprog.datastorage.controller.stub;

import com.adprog.datastorage.models.Category;
import com.adprog.datastorage.models.Project;

public class CategoryStub extends Category {

  private Long id;

  private String name, type, description;

  private Project project;

  public CategoryStub id(Long id) {
    this.id = id;
    return this;
  }

  public CategoryStub name(String name) {
    this.name = name;
    return this;
  }

  public CategoryStub description(String description) {
    this.description = description;
    return this;
  }

  public CategoryStub type(String type) {
    this.type = type;
    return this;
  }

  public CategoryStub project(Project project) {
    this.project = project;
    return this;
  }

  @Override
  public Long getId() {
    return id;
  }

  @Override
  public Project getProject() {
    return project;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public String getType() {
    return type;
  }

  @Override
  public String getDescription() {
    return description;
  }
}
