package com.adprog.datastorage.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.adprog.datastorage.controller.stub.ProjectStub;
import com.adprog.datastorage.controller.stub.RoleStub;
import com.adprog.datastorage.exception.RoleNotFoundException;
import com.adprog.datastorage.models.AuthorityType;
import com.adprog.datastorage.models.Project;
import com.adprog.datastorage.models.Role;
import com.adprog.datastorage.models.User;
import com.adprog.datastorage.service.AuthService;
import com.adprog.datastorage.service.RoleService;
import com.adprog.datastorage.service.UserService;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = RoleController.class)
public class RoleControllerTest {
  @Autowired private MockMvc mvc;

  @MockBean private UserService userService;

  @MockBean private AuthService authService;

  @MockBean private RoleService service;

  private final String BASE_URL = "/api/projects/1/roles/";

  private String jwtToken;

  private void loginAsAdmin() throws Exception {
    List<GrantedAuthority> adminAuthority =
        Arrays.asList(
            new SimpleGrantedAuthority("1_GET"),
            new SimpleGrantedAuthority("1_POST"),
            new SimpleGrantedAuthority("1_PATCH"),
            new SimpleGrantedAuthority("1_DELETE"),
            new SimpleGrantedAuthority("1_ROLE"));
    when(authService.loadUserByUsername("admin"))
        .thenReturn(
            new org.springframework.security.core.userdetails.User(
                "admin", "admin", true, true, true, true, adminAuthority));
    when(userService.getEntity("admin")).thenReturn(new User("admin", "admin", "admin", "admin"));

    String LOGIN_CREDENTIAL = "{ \"username\": \"admin\", \"password\": \"admin\" }";
    this.mvc
        .perform(post("/login").contentType(MediaType.APPLICATION_JSON).content(LOGIN_CREDENTIAL))
        .andDo(print())
        .andDo(
            (i) -> {
              jwtToken = i.getResponse().getHeader("Authorization");
            });
  }

  private MockHttpServletRequestBuilder buildGetRequest(String url) {
    return get(url).contentType(MediaType.APPLICATION_JSON).header("Authorization", jwtToken);
  }

  private MockHttpServletRequestBuilder buildPostRequest(String url, String body) {
    return post(url)
        .contentType(MediaType.APPLICATION_JSON)
        .header("Authorization", jwtToken)
        .content(body);
  }

  private MockHttpServletRequestBuilder buildPatchRequest(String url, String body) {
    return patch(url)
        .contentType(MediaType.APPLICATION_JSON)
        .header("Authorization", jwtToken)
        .content(body);
  }

  private MockHttpServletRequestBuilder buildDeleteRequest(String url) {
    return delete(url).contentType(MediaType.APPLICATION_JSON).header("Authorization", jwtToken);
  }

  private Project mockProject;

  private Role mockRole1, mockRole2;

  private void mockEntities() {
    mockProject = new ProjectStub().id(1L);

    mockRole1 =
        new RoleStub()
            .id(1L)
            .project(mockProject)
            .authorities(new HashSet<>(Arrays.asList(AuthorityType.GET, AuthorityType.POST)))
            .name("User Role");

    mockRole2 =
        new RoleStub()
            .id(2L)
            .project(mockProject)
            .authorities(
                new HashSet<>(
                    Arrays.asList(
                        AuthorityType.GET,
                        AuthorityType.POST,
                        AuthorityType.PATCH,
                        AuthorityType.DELETE)))
            .name("Admin Role");
  }

  @BeforeEach
  public void setUp() throws Exception {
    mockEntities();
    loginAsAdmin();
  }

  @Test
  public void testRoleControllerGetRequest_authorized_shouldReturn200() throws Exception {
    when(service.getEntities(1L, null)).thenReturn(Arrays.asList(mockRole1, mockRole2));

    this.mvc.perform(buildGetRequest(BASE_URL)).andExpect(status().isOk());
  }

  @Test
  public void testRoleControllerGetRequest_unauthorized_shouldReturn403() throws Exception {
    jwtToken = "";

    this.mvc.perform(buildGetRequest(BASE_URL)).andExpect(status().isForbidden());
  }

  @Test
  public void testRoleControllerGetRequestWithId_authorized_shouldReturn200() throws Exception {
    when(service.getEntity(1L, 1L)).thenReturn(mockRole1);

    String url = BASE_URL + "/1";

    this.mvc.perform(buildGetRequest(url)).andExpect(status().isOk());
  }

  @Test
  public void testRoleControllerGetRequestWithId_unauthorized_shouldReturn403() throws Exception {
    jwtToken = "";
    String url = BASE_URL + "/1";

    this.mvc.perform(buildGetRequest(url)).andExpect(status().isForbidden());
  }

  @Test
  public void testRoleControllerGetRequestWithId_authorizedButInvalidRoleId_shouldReturn404()
      throws Exception {
    when(service.getEntity(1L, 3L)).thenThrow(RoleNotFoundException.class);

    String url = BASE_URL + "/3";

    this.mvc.perform(buildGetRequest(url)).andExpect(status().isNotFound());
  }

  @Test
  public void testRoleControllerPostRequest_authorized_shouldReturn201() throws Exception {
    when(service.postEntity(eq(1L), any(Role.class)))
        .thenAnswer(invocation -> invocation.getArgument(1));

    JSONObject body = new JSONObject();
    body.put("project", 1L);
    body.put("name", "New Role");
    body.put("description", "New Desc.");
    body.put("authorities", Arrays.asList("GET", "POST", "PATCH"));

    this.mvc.perform(buildPostRequest(BASE_URL, body.toString())).andExpect(status().isCreated());
  }

  @Test
  public void testRoleControllerPostRequest_unauthorized_shouldReturn403() throws Exception {
    jwtToken = "";
    JSONObject body = new JSONObject();
    body.put("project", 1L);
    body.put("name", "New Role");
    body.put("description", "New Desc.");
    body.put("authorities", Arrays.asList("GET", "POST", "PATCH"));

    this.mvc.perform(buildPostRequest(BASE_URL, body.toString())).andExpect(status().isForbidden());
  }

  @Test
  public void testRoleControllerPostRequest_invalidRequest_shouldReturn400() throws Exception {
    this.mvc.perform(buildPostRequest(BASE_URL, "")).andExpect(status().isBadRequest());
  }

  @Test
  public void testRoleControllerPatchRequest_authorized_shouldReturn200() throws Exception {
    when(service.patchEntity(eq(1L), eq(1L), ArgumentMatchers.anyMap())).thenReturn(mockRole1);

    String url = BASE_URL + "/1";

    this.mvc.perform(buildPatchRequest(url, "{}")).andExpect(status().isOk());
    verify(service, times(1)).patchEntity(1L, 1L, Collections.emptyMap());
  }

  @Test
  public void testRoleControllerPatchRequest_unauthorized_shouldReturn403() throws Exception {
    jwtToken = "";
    String url = BASE_URL + "/1";

    this.mvc.perform(buildPatchRequest(url, "{}")).andExpect(status().isForbidden());
  }

  @Test
  public void testRoleControllerPatchRequest_authorizedButInvalidRoleId_shouldReturn404()
      throws Exception {
    when(service.patchEntity(eq(1L), eq(3L), ArgumentMatchers.anyMap()))
        .thenThrow(RoleNotFoundException.class);

    String url = BASE_URL + "/3";

    this.mvc.perform(buildPatchRequest(url, "{}")).andExpect(status().isNotFound());
  }

  @Test
  public void testRoleControllerDeleteRequest_authorized_shouldReturn200() throws Exception {
    String url = BASE_URL + "/1";

    this.mvc.perform(buildDeleteRequest(url)).andExpect(status().isOk());
  }

  @Test
  public void testRoleControllerDeleteRequest_unauthorized_shouldReturn403() throws Exception {
    jwtToken = "";
    String url = BASE_URL + "/1";

    this.mvc.perform(buildDeleteRequest(url)).andExpect(status().isForbidden());
  }

  @Test
  public void testRoleControllerDeleteRequest_authorizedButInvalidRoleId_shouldReturn404()
      throws Exception {
    doThrow(RoleNotFoundException.class).when(service).deleteEntity(1L, 3L);

    String url = BASE_URL + "/3";

    this.mvc.perform(buildDeleteRequest(url)).andExpect(status().isNotFound());
  }
}
