package com.adprog.datastorage.controller.stub;

import com.adprog.datastorage.models.UserDto;

public class UserDtoStub extends UserDto {

  private String username, password, matchingPassword, fullName, email;

  public UserDtoStub username(String username) {
    this.username = username;
    return this;
  }

  public UserDtoStub password(String password) {
    this.password = password;
    return this;
  }

  public UserDtoStub matchingPassword(String matchingPassword) {
    this.matchingPassword = matchingPassword;
    return this;
  }

  public UserDtoStub email(String email) {
    this.email = email;
    return this;
  }

  public UserDtoStub fullName(String fullName) {
    this.fullName = fullName;
    return this;
  }

  @Override
  public String getUsername() {
    return username;
  }

  @Override
  public String getPassword() {
    return password;
  }

  @Override
  public String getFullName() {
    return fullName;
  }

  @Override
  public String getEmail() {
    return email;
  }
}
