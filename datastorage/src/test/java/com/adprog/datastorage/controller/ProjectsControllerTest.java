package com.adprog.datastorage.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.adprog.datastorage.controller.stub.ProjectStub;
import com.adprog.datastorage.exception.ProjectNotFoundException;
import com.adprog.datastorage.models.*;
import com.adprog.datastorage.service.AuthService;
import com.adprog.datastorage.service.ProjectService;
import com.adprog.datastorage.service.UserService;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = ProjectsController.class)
public class ProjectsControllerTest {
  @Autowired private MockMvc mvc;

  @MockBean private UserService userService;

  @MockBean private AuthService authService;

  @MockBean private ProjectService service;

  private final String BASE_URL = "/api";

  private final String LOGIN_CREDENTIAL = "{ \"username\": \"admin\", \"password\": \"admin\" }";

  private String jwtToken;

  private Project project1, project2;

  private void mockEntities() {
    project1 =
        new ProjectStub()
            .id(1L)
            .title("First Project")
            .description("First Description")
            .creationDate(new Timestamp(0L))
            .lastUpdated(new Timestamp(0L));

    project2 =
        new ProjectStub()
            .id(2L)
            .title("Second Project")
            .description("Second Description")
            .creationDate(new Timestamp(0L))
            .lastUpdated(new Timestamp(0L));
  }

  private void loginAsAdmin() throws Exception {
    List<GrantedAuthority> adminAuthority =
        Arrays.asList(
            new SimpleGrantedAuthority("1_GET"),
            new SimpleGrantedAuthority("1_POST"),
            new SimpleGrantedAuthority("1_PATCH"),
            new SimpleGrantedAuthority("1_DELETE"),
            new SimpleGrantedAuthority("3_GET"),
            new SimpleGrantedAuthority("3_POST"),
            new SimpleGrantedAuthority("3_PATCH"),
            new SimpleGrantedAuthority("3_DELETE"));
    when(authService.loadUserByUsername("admin"))
        .thenReturn(
            new org.springframework.security.core.userdetails.User(
                "admin", "admin", true, true, true, true, adminAuthority));
    when(userService.getEntity("admin")).thenReturn(new User("admin", "admin", "admin", "admin"));

    this.mvc
        .perform(post("/login").contentType(MediaType.APPLICATION_JSON).content(LOGIN_CREDENTIAL))
        .andDo(print())
        .andDo(
            (i) -> {
              jwtToken = i.getResponse().getHeader("Authorization");
            });
  }

  private MockHttpServletRequestBuilder buildGetRequest(String url) {
    return get(url).contentType(MediaType.APPLICATION_JSON).header("Authorization", jwtToken);
  }

  private MockHttpServletRequestBuilder buildPostRequest(String url, String body) {
    return post(url)
        .contentType(MediaType.APPLICATION_JSON)
        .header("Authorization", jwtToken)
        .content(body);
  }

  private MockHttpServletRequestBuilder buildPatchRequest(String url, String body) {
    return patch(url)
        .contentType(MediaType.APPLICATION_JSON)
        .header("Authorization", jwtToken)
        .content(body);
  }

  private MockHttpServletRequestBuilder buildDeleteRequest(String url) {
    return delete(url).contentType(MediaType.APPLICATION_JSON).header("Authorization", jwtToken);
  }

  @BeforeEach
  public void setUp() throws Exception {
    mockEntities();
    loginAsAdmin();
  }

  @Test
  public void testProjectsControllerGetProjects_return200() throws Exception {
    when(service.getEntities()).thenReturn(Arrays.asList(project1, project2));
    String url = BASE_URL + "/projects";

    this.mvc.perform(buildGetRequest(url)).andExpect(status().isOk());
    verify(service, times(1)).getEntities();
  }

  @Test
  public void testProjectsControllerGetProject_validId_return200() throws Exception {
    when(service.getEntity(1L)).thenReturn(project1);
    String url = BASE_URL + "/projects/1";

    this.mvc.perform(buildGetRequest(url)).andExpect(status().isOk());
  }

  @Test
  public void testProjectsControllerGetProjectById_authorizedButProjectIsGone_return404()
      throws Exception {
    when(service.getEntity(3L)).thenThrow(ProjectNotFoundException.class);
    String url = BASE_URL + "/projects/3";

    this.mvc.perform(buildGetRequest(url)).andExpect(status().isNotFound());
    verify(service, times(1)).getEntity(3L);
  }

  @Test
  public void testProjectsControllerGetProjectById_unauthorized_return403() throws Exception {
    when(service.getEntity(4L)).thenThrow(ProjectNotFoundException.class);
    String url = BASE_URL + "/projects/4";

    this.mvc.perform(buildGetRequest(url)).andExpect(status().isForbidden());
  }

  @Test
  public void testProjectsControllerPostProjects_return201() throws Exception {
    when(service.postEntity(any(Project.class), any(User.class)))
        .thenAnswer(invocation -> invocation.getArgument(0));

    JSONObject content = new JSONObject();
    content.put("title", "New Project");
    content.put("description", "Brand new");
    String url = BASE_URL + "/projects";

    this.mvc.perform(buildPostRequest(url, content.toString())).andExpect(status().isCreated());
    verify(service, times(1)).postEntity(any(Project.class), any(User.class));
  }

  @Test
  public void testProjectsControllerPutProject_validId_return200() throws Exception {
    when(service.patchEntity(eq(1L), ArgumentMatchers.any())).thenReturn(project1);

    JSONObject content = new JSONObject();
    content.put("title", "New Project");
    content.put("description", "Brand new");
    String url = BASE_URL + "/projects/1";

    this.mvc.perform(buildPatchRequest(url, content.toString())).andExpect(status().isOk());
    verify(service, times(1)).patchEntity(eq(1L), ArgumentMatchers.any());
  }

  @Test
  public void testProjectsControllerPatchProject_authorizedButProjectIsGone_return404()
      throws Exception {
    when(service.patchEntity(eq(3L), ArgumentMatchers.any()))
        .thenThrow(ProjectNotFoundException.class);

    JSONObject content = new JSONObject();
    content.put("title", "New Project");
    content.put("description", "Brand new");
    String url = BASE_URL + "/projects/3";

    this.mvc.perform(buildPatchRequest(url, content.toString())).andExpect(status().isNotFound());
    verify(service, times(1)).patchEntity(eq(3L), ArgumentMatchers.any());
  }

  @Test
  public void testProjectsControllerPatchProject_unauthorized_return403() throws Exception {
    when(service.patchEntity(eq(4L), ArgumentMatchers.any()))
        .thenAnswer(invocation -> invocation.getArgument(1));

    JSONObject content = new JSONObject();
    content.put("title", "New Project");
    content.put("description", "Brand new");
    String url = BASE_URL + "/projects/4";

    this.mvc.perform(buildPatchRequest(url, content.toString())).andExpect(status().isForbidden());
  }

  @Test
  public void testProjectsControllerDeleteProjectById_validId_return200() throws Exception {
    String url = BASE_URL + "/projects/1";

    this.mvc.perform(buildDeleteRequest(url)).andExpect(status().isOk());
    verify(service, times(1)).deleteEntity(1L);
  }

  @Test
  public void testProjectsControllerDeleteProjectById_authorizedButProjectIsGone_return404()
      throws Exception {
    doThrow(ProjectNotFoundException.class).when(service).deleteEntity(3L);
    String url = BASE_URL + "/projects/3";

    this.mvc.perform(buildDeleteRequest(url)).andExpect(status().isNotFound());
    verify(service, times(1)).deleteEntity(3L);
  }

  @Test
  public void testProjectsControllerDeleteProjectById_unauthorized_return403() throws Exception {
    doThrow(ProjectNotFoundException.class).when(service).deleteEntity(4L);
    String url = BASE_URL + "/projects/4";

    this.mvc.perform(buildDeleteRequest(url)).andExpect(status().isForbidden());
  }
}
