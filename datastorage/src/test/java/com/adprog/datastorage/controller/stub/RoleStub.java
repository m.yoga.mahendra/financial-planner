package com.adprog.datastorage.controller.stub;

import com.adprog.datastorage.models.AuthorityType;
import com.adprog.datastorage.models.Project;
import com.adprog.datastorage.models.Role;
import com.adprog.datastorage.models.User;
import java.util.HashSet;
import java.util.Set;

public class RoleStub extends Role {
  private Long id;

  private Project project;

  private String name;

  private Set<User> users;

  private HashSet<AuthorityType> authorities;

  public RoleStub id(Long id) {
    this.id = id;
    return this;
  }

  public RoleStub name(String name) {
    this.name = name;
    return this;
  }

  public RoleStub users(Set<User> users) {
    this.users = users;
    return this;
  }

  public RoleStub project(Project project) {
    this.project = project;
    return this;
  }

  public RoleStub authorities(HashSet<AuthorityType> authorities) {
    this.authorities = authorities;
    return this;
  }

  @Override
  public Long getId() {
    return id;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public Set<User> getUsers() {
    return users;
  }

  @Override
  public HashSet<AuthorityType> getAuthorities() {
    return authorities;
  }

  @Override
  public Project getProject() {
    return project;
  }
}
