package com.adprog.datastorage.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.adprog.datastorage.models.User;
import com.adprog.datastorage.models.UserDto;
import com.adprog.datastorage.service.AuthService;
import com.adprog.datastorage.service.UserService;
import java.util.ArrayList;
import java.util.Arrays;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = UserController.class)
public class UserControllerTest {
  @Autowired private MockMvc mvc;

  @MockBean private AuthService authService;

  @MockBean private UserService service;

  private static final String BASE_URL = "/api/users";

  private final String LOGIN_CREDENTIAL = "{ \"username\": \"admin\", \"password\": \"admin\" }";

  private String jwtToken;

  User user1, user2;

  private void loginAsAdmin() throws Exception {
    when(authService.loadUserByUsername("admin"))
        .thenReturn(
            new org.springframework.security.core.userdetails.User(
                "admin", "admin", true, true, true, true, new ArrayList<>()));

    this.mvc
        .perform(post("/login").contentType(MediaType.APPLICATION_JSON).content(LOGIN_CREDENTIAL))
        .andDo(print())
        .andDo(
            (i) -> {
              jwtToken = i.getResponse().getHeader("Authorization");
            });
  }

  private MockHttpServletRequestBuilder buildGetRequest(String url) {
    return get(url).contentType(MediaType.APPLICATION_JSON).header("Authorization", jwtToken);
  }

  private MockHttpServletRequestBuilder buildPostRequest(String url, String body) {
    return post(url)
        .contentType(MediaType.APPLICATION_JSON)
        .header("Authorization", jwtToken)
        .content(body);
  }

  private MockHttpServletRequestBuilder buildPatchRequest(String url, String body) {
    return patch(url)
        .contentType(MediaType.APPLICATION_JSON)
        .header("Authorization", jwtToken)
        .content(body);
  }

  private MockHttpServletRequestBuilder buildDeleteRequest(String url) {
    return delete(url).contentType(MediaType.APPLICATION_JSON).header("Authorization", jwtToken);
  }

  private void mockEntities() {
    user1 = new User("admin", "admin@gmail.com", "admin", "Admin");
    user2 = new User("Cykes", "igo@gmail.com", "cykes", "Cykes");
  }

  @BeforeEach
  public void setUp() throws Exception {
    mockEntities();
    loginAsAdmin();
  }

  @Test
  public void testAuthControllerGetUsers_return200() throws Exception {
    when(service.getEntities()).thenReturn(Arrays.asList(user1, user2));

    this.mvc.perform(buildGetRequest(BASE_URL)).andExpect(status().isOk());
  }

  @Test
  public void testAuthControllerGetUser_validUsername_return200() throws Exception {
    when(service.getEntity("admin")).thenReturn(user1);
    String url = BASE_URL + "/admin";

    this.mvc.perform(buildGetRequest(BASE_URL)).andExpect(status().isOk());
  }

  @Test
  public void testAuthControllerGetUser_unauthorizedUsername_return403() throws Exception {
    when(service.getEntity("Cykes")).thenReturn(user2);
    String url = BASE_URL + "/Cykes";

    this.mvc.perform(buildGetRequest(url)).andExpect(status().isForbidden());
  }

  @Test
  public void testAuthControllerPostUser_validInput_return200() throws Exception {
    when(service.postEntity(ArgumentMatchers.any(UserDto.class)))
        .then(
            invocation -> {
              UserDto param = ((UserDto) invocation.getArgument(0));
              return param.getUser(new BCryptPasswordEncoder(11));
            });
    String url = BASE_URL;
    JSONObject body = new JSONObject();
    body.put("username", "JDoe");
    body.put("fullName", "John Doe");
    body.put("password", "doe");
    body.put("matchingPassword", "doe");
    body.put("email", "john@gmail.com");

    this.mvc.perform(buildPostRequest(url, body.toString())).andExpect(status().isOk());
  }

  @Test
  public void testAuthControllerPostUser_invalidInput_return400() throws Exception {
    JSONObject body = new JSONObject();
    body.put("username", "JDoe");
    body.put("fullName", "John Doe");
    body.put("password", "doe");
    body.put("email", "john@doe.com");

    this.mvc
        .perform(buildPostRequest(BASE_URL, body.toString()))
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testAuthControllerPostUser_noRequestBody_return400() throws Exception {
    this.mvc.perform(post(BASE_URL)).andExpect(status().isBadRequest());
  }

  @Test
  public void testAuthControllerDeleteUser_validUsername_return200() throws Exception {
    String url = BASE_URL + "/admin";
    this.mvc.perform(buildDeleteRequest(url)).andExpect(status().isOk());
  }

  @Test
  public void testAuthControllerDeleteUser_unauthorizedUsername_return403() throws Exception {
    String url = BASE_URL + "/Cykes";
    this.mvc.perform(buildDeleteRequest(url)).andExpect(status().isForbidden());
  }
}
