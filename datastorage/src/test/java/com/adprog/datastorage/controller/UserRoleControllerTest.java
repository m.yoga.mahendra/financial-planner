package com.adprog.datastorage.controller;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.adprog.datastorage.controller.stub.ProjectStub;
import com.adprog.datastorage.controller.stub.RoleStub;
import com.adprog.datastorage.controller.stub.UserStub;
import com.adprog.datastorage.exception.UserNotFoundException;
import com.adprog.datastorage.models.Project;
import com.adprog.datastorage.models.Role;
import com.adprog.datastorage.models.User;
import com.adprog.datastorage.service.AuthService;
import com.adprog.datastorage.service.UserRoleService;
import com.adprog.datastorage.service.UserService;
import java.util.*;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = UserRoleController.class)
public class UserRoleControllerTest {
  @Autowired private MockMvc mvc;

  @MockBean private UserService userService;

  @MockBean private AuthService authService;

  @MockBean private UserRoleService service;

  private final String BASE_URL = "/api/projects/1/roles/1/users";

  private String jwtToken;

  private void loginAsAdmin() throws Exception {
    List<GrantedAuthority> adminAuthority =
        Arrays.asList(
            new SimpleGrantedAuthority("1_GET"),
            new SimpleGrantedAuthority("1_POST"),
            new SimpleGrantedAuthority("1_PATCH"),
            new SimpleGrantedAuthority("1_DELETE"),
            new SimpleGrantedAuthority("1_ROLE"));
    when(authService.loadUserByUsername("admin"))
        .thenReturn(
            new org.springframework.security.core.userdetails.User(
                "admin", "admin", true, true, true, true, adminAuthority));
    when(userService.getEntity("admin")).thenReturn(new User("admin", "admin", "admin", "admin"));

    String LOGIN_CREDENTIAL = "{ \"username\": \"admin\", \"password\": \"admin\" }";
    this.mvc
        .perform(post("/login").contentType(MediaType.APPLICATION_JSON).content(LOGIN_CREDENTIAL))
        .andDo(print())
        .andDo(
            (i) -> {
              jwtToken = i.getResponse().getHeader("Authorization");
            });
  }

  private MockHttpServletRequestBuilder buildGetRequest(String url) {
    return get(url).contentType(MediaType.APPLICATION_JSON).header("Authorization", jwtToken);
  }

  private MockHttpServletRequestBuilder buildPostRequest(String url, String body) {
    return post(url)
        .contentType(MediaType.APPLICATION_JSON)
        .header("Authorization", jwtToken)
        .content(body);
  }

  private MockHttpServletRequestBuilder buildPatchRequest(String url, String body) {
    return patch(url)
        .contentType(MediaType.APPLICATION_JSON)
        .header("Authorization", jwtToken)
        .content(body);
  }

  private MockHttpServletRequestBuilder buildDeleteRequest(String url) {
    return delete(url).contentType(MediaType.APPLICATION_JSON).header("Authorization", jwtToken);
  }

  private Project mockProject;

  private Role mockRole;

  private User mockUser1, mockUser2, mockUser3;

  private void mockEntities() {
    mockProject = new ProjectStub().id(1L);

    RoleStub stub = new RoleStub();

    mockUser1 = new UserStub().username("admin").roles(Collections.singletonList(mockRole));

    mockUser2 = new UserStub().username("admin2").roles(Collections.singletonList(mockRole));

    mockUser3 = new UserStub().username("admin3").roles(Collections.emptyList());

    mockRole =
        stub.id(1L)
            .name("Role")
            .project(mockProject)
            .users(new HashSet<>(Arrays.asList(mockUser1, mockUser2)));
  }

  @BeforeEach
  public void setUp() throws Exception {
    mockEntities();
    loginAsAdmin();
  }

  @Test
  public void testUserRoleControllerGetUsers_authorized_shouldReturn200() throws Exception {
    when(service.getEntities(1L, 1L)).thenReturn(Arrays.asList(mockUser1, mockUser2));

    this.mvc.perform(buildGetRequest(BASE_URL)).andExpect(status().isOk());
  }

  @Test
  public void testUserRoleControllerGetUsers_unauthorized_shouldReturn403() throws Exception {
    jwtToken = "";

    this.mvc.perform(buildGetRequest(BASE_URL)).andExpect(status().isForbidden());
  }

  @Test
  public void testUserRoleControllerGetUserByUsername_authorized_shouldReturn200()
      throws Exception {
    when(service.getEntity(1L, 1L, "admin")).thenReturn(mockUser1);

    String url = BASE_URL + "/admin";

    this.mvc.perform(buildGetRequest(url)).andExpect(status().isOk());
  }

  @Test
  public void testUserRoleControllerGetUserByUsername_unauthorized_shouldReturn403()
      throws Exception {
    jwtToken = "";
    String url = BASE_URL + "/admin";

    this.mvc.perform(buildGetRequest(url)).andExpect(status().isForbidden());
  }

  @Test
  public void testUserRoleControllerGetUserByUsername_authorizedButInvalidInput_shouldReturn404()
      throws Exception {
    String url = BASE_URL + "/admin4";

    when(service.getEntity(1L, 1L, "admin4")).thenThrow(UserNotFoundException.class);
    this.mvc.perform(buildGetRequest(url)).andExpect(status().isNotFound());
  }

  @Test
  public void testUserRoleControllerAddUserToRole_authorized_shouldReturn201() throws Exception {
    when(service.postEntity(1L, 1L, "admin3")).thenReturn(mockRole);

    JSONObject body = new JSONObject();
    body.put("username", "admin3");

    this.mvc.perform(buildPostRequest(BASE_URL, body.toString())).andExpect(status().isCreated());
  }

  @Test
  public void testUserRoleControllerAddUserToRole_unauthorized_shouldReturn403() throws Exception {
    jwtToken = "";
    JSONObject body = new JSONObject();
    body.put("username", "admin3");

    this.mvc.perform(buildPostRequest(BASE_URL, body.toString())).andExpect(status().isForbidden());
  }

  @Test
  public void testUserRoleControllerAddUserToRole_authorizedButInvalidInput_shouldReturn404()
      throws Exception {
    JSONObject body = new JSONObject();
    body.put("username", "admin4");

    when(service.postEntity(1L, 1L, "admin4")).thenThrow(UserNotFoundException.class);
    this.mvc.perform(buildPostRequest(BASE_URL, body.toString())).andExpect(status().isNotFound());
  }

  @Test
  public void testUserRoleControllerDeleteUserFromRole_authorized_shouldReturn200()
      throws Exception {
    String url = BASE_URL + "/admin";

    this.mvc.perform(buildDeleteRequest(url)).andExpect(status().isOk());
  }

  @Test
  public void testUserRoleControllerDeleteUserFromRole_unauthorized_shouldReturn403()
      throws Exception {
    jwtToken = "";
    String url = BASE_URL + "/admin";

    this.mvc.perform(buildDeleteRequest(url)).andExpect(status().isForbidden());
  }

  @Test
  public void testUserRoleControllerDeleteUserFromRole_authorizedButInvalidInput_shouldReturn404()
      throws Exception {
    doThrow(UserNotFoundException.class).when(service).deleteEntity(1L, 1L, "admin4");

    String url = BASE_URL + "/admin4";

    this.mvc.perform(buildDeleteRequest(url)).andExpect(status().isNotFound());
  }
}
