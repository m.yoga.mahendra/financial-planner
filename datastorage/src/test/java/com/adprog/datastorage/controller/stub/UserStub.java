package com.adprog.datastorage.controller.stub;

import com.adprog.datastorage.models.Role;
import com.adprog.datastorage.models.User;
import java.util.List;

public class UserStub extends User {

  private String fullName, username, password, email;

  private List<Role> roles;

  public UserStub() {}

  public UserStub fullName(String fullName) {
    this.fullName = fullName;
    return this;
  }

  public UserStub username(String username) {
    this.username = username;
    return this;
  }

  public UserStub email(String email) {
    this.email = email;
    return this;
  }

  public UserStub password(String password) {
    this.password = password;
    return this;
  }

  public UserStub roles(List<Role> roles) {
    this.roles = roles;
    return this;
  }

  @Override
  public String getFullName() {
    return fullName;
  }

  @Override
  public String getUsername() {
    return username;
  }

  @Override
  public String getPassword() {
    return password;
  }

  @Override
  public String getEmail() {
    return email;
  }

  @Override
  public List<Role> getRoles() {
    return roles;
  }
}
