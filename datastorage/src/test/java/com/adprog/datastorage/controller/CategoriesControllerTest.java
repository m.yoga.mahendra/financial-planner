package com.adprog.datastorage.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.adprog.datastorage.controller.stub.CategoryStub;
import com.adprog.datastorage.controller.stub.ProjectStub;
import com.adprog.datastorage.exception.CategoryNotFoundException;
import com.adprog.datastorage.exception.ProjectNotFoundException;
import com.adprog.datastorage.models.Category;
import com.adprog.datastorage.models.Project;
import com.adprog.datastorage.models.User;
import com.adprog.datastorage.query.api.CategoryGetAllQueryBuilder;
import com.adprog.datastorage.service.AuthService;
import com.adprog.datastorage.service.CategoryService;
import com.adprog.datastorage.service.UserService;
import java.sql.Timestamp;
import java.util.*;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = CategoriesController.class)
public class CategoriesControllerTest {

  @Autowired private MockMvc mvc;

  @MockBean private AuthService authService;

  @MockBean private UserService userService;

  @MockBean private CategoryService service;

  private final String BASE_VALID_URL = "/api/projects/1";
  private final String BASE_UNAUTHORIZED_URL = "/api/projects/3";
  private final String BASE_INVALID_URL = "/api/projects/4";

  private String jwtToken;

  private Category cat1, cat2;

  private void mockEntities() {
    Project project1 =
        new ProjectStub()
            .id(1L)
            .title("Title")
            .description("Description")
            .creationDate(new Timestamp(0L))
            .lastUpdated(new Timestamp(0L));

    cat1 =
        new CategoryStub()
            .id(1L)
            .name("Sample Income")
            .description("Sample Income")
            .type("I")
            .project(project1);

    cat2 =
        new CategoryStub()
            .id(2L)
            .name("Sample Expense")
            .description("Sample Expense")
            .type("E")
            .project(project1);
  }

  private void loginAsAdmin() throws Exception {
    List<GrantedAuthority> adminAuthority =
        Arrays.asList(
            new SimpleGrantedAuthority("1_GET"),
            new SimpleGrantedAuthority("1_POST"),
            new SimpleGrantedAuthority("1_PATCH"),
            new SimpleGrantedAuthority("1_DELETE"),
            new SimpleGrantedAuthority("4_GET"),
            new SimpleGrantedAuthority("4_POST"),
            new SimpleGrantedAuthority("4_PATCH"),
            new SimpleGrantedAuthority("4_DELETE"));
    when(authService.loadUserByUsername("admin"))
        .thenReturn(
            new org.springframework.security.core.userdetails.User(
                "admin", "admin", true, true, true, true, adminAuthority));
    when(userService.getEntity("admin")).thenReturn(new User("admin", "admin", "admin", "admin"));

    String LOGIN_CREDENTIAL = "{ \"username\": \"admin\", \"password\": \"admin\" }";
    this.mvc
        .perform(post("/login").contentType(MediaType.APPLICATION_JSON).content(LOGIN_CREDENTIAL))
        .andDo(print())
        .andDo(
            (i) -> {
              jwtToken = i.getResponse().getHeader("Authorization");
            });
  }

  @BeforeEach
  public void setUp() throws Exception {
    mockEntities();
    loginAsAdmin();
  }

  private MockHttpServletRequestBuilder buildGetRequest(String url) {
    return get(url).contentType(MediaType.APPLICATION_JSON).header("Authorization", jwtToken);
  }

  private MockHttpServletRequestBuilder buildPostRequest(String url, String body) {
    return post(url)
        .contentType(MediaType.APPLICATION_JSON)
        .header("Authorization", jwtToken)
        .content(body);
  }

  private MockHttpServletRequestBuilder buildPatchRequest(String url, String body) {
    return patch(url)
        .contentType(MediaType.APPLICATION_JSON)
        .header("Authorization", jwtToken)
        .content(body);
  }

  private MockHttpServletRequestBuilder buildDeleteRequest(String url) {
    return delete(url).contentType(MediaType.APPLICATION_JSON).header("Authorization", jwtToken);
  }

  @Test
  public void testCategoriesControllerGetCategories_validProject_return200() throws Exception {
    when(service.getEntities(1L, new CategoryGetAllQueryBuilder(1L)))
        .thenReturn(Arrays.asList(cat1, cat2));
    String url = BASE_VALID_URL + "/categories";

    this.mvc.perform(buildGetRequest(url)).andExpect(status().isOk());
  }

  @Test
  public void testCategoriesControllerGetCategories_authorizedButProjectIsGone_return404()
      throws Exception {
    when(service.getEntities(eq(4L), any(CategoryGetAllQueryBuilder.class)))
        .thenThrow(ProjectNotFoundException.class);
    String url = BASE_INVALID_URL + "/categories";

    this.mvc.perform(buildGetRequest(url)).andExpect(status().isNotFound());
  }

  @Test
  public void testCategoriesControllerGetCategories_unauthorized_return403() throws Exception {
    when(service.getEntities(3L, new CategoryGetAllQueryBuilder(3L)))
        .thenThrow(ProjectNotFoundException.class);
    String url = BASE_UNAUTHORIZED_URL + "/categories";

    this.mvc.perform(buildGetRequest(url)).andExpect(status().isForbidden());
  }

  @Test
  public void testCategoriesControllerGetCategoryById_authorizedButProjectIsGone_return404()
      throws Exception {
    when(service.getEntity(4L, 1L)).thenThrow(ProjectNotFoundException.class);
    String url = BASE_INVALID_URL + "/categories/1";

    this.mvc.perform(buildGetRequest(url)).andExpect(status().isNotFound());
  }

  @Test
  public void testCategoriesControllerGetCategoryById_authorizedButCategoryIsGone_return404()
      throws Exception {
    when(service.getEntity(1L, 3L)).thenThrow(CategoryNotFoundException.class);
    String url = BASE_VALID_URL + "/categories/3";

    this.mvc.perform(buildGetRequest(url)).andExpect(status().isNotFound());
  }

  @Test
  public void testCategoriesControllerGetCategoryById_unauthorized_return403() throws Exception {
    when(service.getEntity(3L, 1L)).thenThrow(ProjectNotFoundException.class);
    String url = BASE_UNAUTHORIZED_URL + "/categories/1";

    this.mvc.perform(buildGetRequest(url)).andExpect(status().isForbidden());
  }

  @Test
  public void testCategoriesControllerGetCategoryById_validCategory_return200() throws Exception {
    when(service.getEntity(1L, 1L)).thenReturn(cat1);
    String url = BASE_VALID_URL + "/categories/1";

    this.mvc.perform(buildGetRequest(url)).andExpect(status().isOk());
  }

  @Test
  public void testCategoriesControllerPostCategory_validProject_return201() throws Exception {
    when(service.postEntity(eq(1L), any(Category.class)))
        .thenAnswer(invocation -> invocation.getArgument(1));
    String url = BASE_VALID_URL + "/categories";

    JSONObject body = new JSONObject();
    body.put("project", 1L);
    body.put("name", "Renamed Category");
    body.put("description", "Replaced description");
    body.put("type", 'I');
    this.mvc.perform(buildPostRequest(url, body.toString())).andExpect(status().isCreated());
  }

  @Test
  public void testCategoriesControllerPostCategory_authorizedButProjectIsGone_return404()
      throws Exception {
    when(service.postEntity(eq(4L), any(Category.class))).thenThrow(ProjectNotFoundException.class);
    String url = BASE_INVALID_URL + "/categories";

    JSONObject body = new JSONObject();
    body.put("project", 1L);
    body.put("name", "Renamed Category");
    body.put("description", "Replaced description");
    body.put("type", 'I');
    this.mvc.perform(buildPostRequest(url, body.toString())).andExpect(status().isNotFound());
  }

  @Test
  public void testCategoriesControllerPostCategory_unauthorized_return403() throws Exception {
    when(service.postEntity(eq(3L), any(Category.class))).thenThrow(ProjectNotFoundException.class);
    String url = BASE_UNAUTHORIZED_URL + "/categories";

    JSONObject body = new JSONObject();
    body.put("project", 1L);
    body.put("name", "Renamed Category");
    body.put("description", "Replaced description");
    body.put("type", 'I');
    this.mvc.perform(buildPostRequest(url, body.toString())).andExpect(status().isForbidden());
  }

  @Test
  public void testCategoriesControllerPatchCategory_validCategory_return200() throws Exception {
    when(service.patchEntity(eq(1L), eq(1L), ArgumentMatchers.any())).thenReturn(cat1);
    String url = BASE_VALID_URL + "/categories/1";

    JSONObject body = new JSONObject();
    body.put("project", 1L);
    body.put("name", "Renamed Category");
    body.put("description", "Replaced description");
    body.put("type", 'I');
    this.mvc.perform(buildPatchRequest(url, body.toString())).andExpect(status().isOk());
  }

  @Test
  public void testCategoriesControllerPatchCategoryById_authorizedButProjectIsGone_return404()
      throws Exception {
    when(service.patchEntity(eq(4L), eq(1L), ArgumentMatchers.any()))
        .thenThrow(CategoryNotFoundException.class);
    String url = BASE_INVALID_URL + "/categories/1";

    JSONObject body = new JSONObject();
    body.put("name", "Renamed Category");
    body.put("description", "Replaced description");
    body.put("type", 'I');
    this.mvc.perform(buildPatchRequest(url, body.toString())).andExpect(status().isNotFound());
  }

  @Test
  public void testCategoriesControllerPatchCategoryById_authorizedButCategoryIsGone_return404()
      throws Exception {
    when(service.patchEntity(eq(1L), eq(3L), ArgumentMatchers.any()))
        .thenThrow(CategoryNotFoundException.class);
    String url = BASE_VALID_URL + "/categories/3";

    JSONObject body = new JSONObject();
    body.put("name", "Renamed Category");
    body.put("description", "Replaced description");
    body.put("type", "I");
    this.mvc.perform(buildPatchRequest(url, body.toString())).andExpect(status().isNotFound());
  }

  @Test
  public void testCategoriesControllerPatchCategoryById_unauthorized_return404() throws Exception {
    when(service.patchEntity(eq(3L), eq(1L), ArgumentMatchers.any()))
        .thenThrow(ProjectNotFoundException.class);
    String url = BASE_UNAUTHORIZED_URL + "/categories/1";

    JSONObject body = new JSONObject();
    body.put("name", "Renamed Category");
    body.put("description", "Replaced description");
    body.put("type", "I");
    this.mvc.perform(buildPatchRequest(url, body.toString())).andExpect(status().isForbidden());
  }

  @Test
  public void testCategoriesControllerDeleteCategoryById_validCategory_return200()
      throws Exception {
    when(service.getEntity(1L, 1L)).thenReturn(cat1);
    String url = BASE_VALID_URL + "/categories/1";
    this.mvc.perform(buildDeleteRequest(url)).andExpect(status().isOk());
  }

  @Test
  public void testCategoriesControllerDeleteCategoryById_authorizedButProjectIsGone_return404()
      throws Exception {
    doThrow(ProjectNotFoundException.class).when(service).deleteEntity(4L, 1L);
    String url = BASE_INVALID_URL + "/categories/1";
    this.mvc.perform(buildDeleteRequest(url)).andExpect(status().isNotFound());
  }

  @Test
  public void testCategoriesControllerDeleteCategoryById_authorizedButCategoryIsGone_return404()
      throws Exception {
    doThrow(CategoryNotFoundException.class).when(service).deleteEntity(1L, 3L);
    String url = BASE_VALID_URL + "/categories/3";

    this.mvc.perform(buildDeleteRequest(url)).andExpect(status().isNotFound());
  }

  @Test
  public void testCategoriesControllerDeleteCategoryById_unauthorized_return403() throws Exception {
    doThrow(ProjectNotFoundException.class).when(service).deleteEntity(3L, 3L);
    String url = BASE_UNAUTHORIZED_URL + "/categories/3";

    this.mvc.perform(buildDeleteRequest(url)).andExpect(status().isForbidden());
  }
}
