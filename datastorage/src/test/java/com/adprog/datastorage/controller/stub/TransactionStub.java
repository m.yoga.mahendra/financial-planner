package com.adprog.datastorage.controller.stub;

import com.adprog.datastorage.models.Category;
import com.adprog.datastorage.models.Project;
import com.adprog.datastorage.models.Transaction;
import java.sql.Date;
import java.sql.Timestamp;

public class TransactionStub extends Transaction {

  private Project project;

  private Category category;

  private String title, description;
  private Long id, amount;

  private Date transactionDate;

  private Timestamp submitDate;

  public TransactionStub id(Long id) {
    this.id = id;
    return this;
  }

  public TransactionStub project(Project project) {
    this.project = project;
    return this;
  }

  public TransactionStub category(Category category) {
    this.category = category;
    return this;
  }

  public TransactionStub title(String title) {
    this.title = title;
    return this;
  }

  public TransactionStub description(String description) {
    this.description = description;
    return this;
  }

  public TransactionStub amount(Long amount) {
    this.amount = amount;
    return this;
  }

  public TransactionStub transactionDate(Date transactionDate) {
    this.transactionDate = transactionDate;
    return this;
  }

  public TransactionStub submitDate(Timestamp submitDate) {
    this.submitDate = submitDate;
    return this;
  }

  @Override
  public Long getId() {
    return id;
  }

  @Override
  public Project getProject() {
    return project;
  }

  @Override
  public Category getCategory() {
    return category;
  }

  @Override
  public String getTitle() {
    return title;
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public Date getTransactionDate() {
    return transactionDate;
  }

  @Override
  public Timestamp getSubmitDate() {
    return submitDate;
  }

  @Override
  public Long getAmount() {
    return amount;
  }
}
