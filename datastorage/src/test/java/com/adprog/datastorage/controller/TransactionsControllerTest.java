package com.adprog.datastorage.controller;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.adprog.datastorage.controller.stub.CategoryStub;
import com.adprog.datastorage.controller.stub.ProjectStub;
import com.adprog.datastorage.controller.stub.TransactionStub;
import com.adprog.datastorage.exception.ProjectNotFoundException;
import com.adprog.datastorage.exception.TransactionNotFoundException;
import com.adprog.datastorage.models.Category;
import com.adprog.datastorage.models.Project;
import com.adprog.datastorage.models.Transaction;
import com.adprog.datastorage.models.User;
import com.adprog.datastorage.query.api.TransactionGetAllQueryBuilder;
import com.adprog.datastorage.service.AuthService;
import com.adprog.datastorage.service.TransactionService;
import com.adprog.datastorage.service.UserService;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = TransactionsController.class)
public class TransactionsControllerTest {
  @Autowired private MockMvc mvc;

  @MockBean private AuthService authService;

  @MockBean private UserService userService;

  @MockBean private TransactionService service;

  private final String BASE_VALID_URL = "/api/projects/1";
  private final String BASE_UNAUTHORIZED_URL = "/api/projects/3";
  private final String BASE_INVALID_URL = "/api/projects/4";

  private String jwtToken;

  private Project mockProject;

  private Category income;

  private Transaction tra1, tra2;

  private void mockEntities() {
    mockProject =
        new ProjectStub()
            .id(1L)
            .title("Project")
            .description("Desc")
            .creationDate(new Timestamp(0L))
            .lastUpdated(new Timestamp(0L));

    income =
        new CategoryStub()
            .id(1L)
            .name("Income")
            .description("Income")
            .type("I")
            .project(mockProject);

    tra1 =
        new TransactionStub()
            .id(1L)
            .project(mockProject)
            .category(income)
            .title("Tra1")
            .description("Desc1")
            .amount(100L)
            .transactionDate(Date.valueOf("2019-01-01"))
            .submitDate(new Timestamp(0L));

    tra2 =
        new TransactionStub()
            .id(2L)
            .project(mockProject)
            .category(income)
            .title("Tra2")
            .description("Desc2")
            .amount(200L)
            .transactionDate(Date.valueOf("2020-01-01"))
            .submitDate(new Timestamp(0L));
  }

  private void loginAsAdmin() throws Exception {
    List<GrantedAuthority> adminAuthority =
        Arrays.asList(
            new SimpleGrantedAuthority("1_GET"),
            new SimpleGrantedAuthority("1_POST"),
            new SimpleGrantedAuthority("1_PATCH"),
            new SimpleGrantedAuthority("1_DELETE"),
            new SimpleGrantedAuthority("4_GET"),
            new SimpleGrantedAuthority("4_POST"),
            new SimpleGrantedAuthority("4_PATCH"),
            new SimpleGrantedAuthority("4_DELETE"));

    when(authService.loadUserByUsername("admin"))
        .thenReturn(
            new org.springframework.security.core.userdetails.User(
                "admin", "admin", true, true, true, true, adminAuthority));
    when(userService.getEntity("admin")).thenReturn(new User("admin", "admin", "admin", "admin"));

    String LOGIN_CREDENTIAL = "{ \"username\": \"admin\", \"password\": \"admin\" }";
    this.mvc
        .perform(post("/login").contentType(MediaType.APPLICATION_JSON).content(LOGIN_CREDENTIAL))
        .andDo(print())
        .andDo(
            (i) -> {
              jwtToken = i.getResponse().getHeader("Authorization");
            });
  }

  private MockHttpServletRequestBuilder buildGetRequest(String url) {
    return get(url).contentType(MediaType.APPLICATION_JSON).header("Authorization", jwtToken);
  }

  private MockHttpServletRequestBuilder buildPostRequest(String url, String body) {
    return post(url)
        .contentType(MediaType.APPLICATION_JSON)
        .header("Authorization", jwtToken)
        .content(body);
  }

  private MockHttpServletRequestBuilder buildPatchRequest(String url, String body) {
    return patch(url)
        .contentType(MediaType.APPLICATION_JSON)
        .header("Authorization", jwtToken)
        .content(body);
  }

  private MockHttpServletRequestBuilder buildDeleteRequest(String url) {
    return delete(url).contentType(MediaType.APPLICATION_JSON).header("Authorization", jwtToken);
  }

  @BeforeEach
  public void setUp() throws Exception {
    mockEntities();
    loginAsAdmin();
  }

  @Test
  public void testTransactionsControllerGetTransactions_validProject_return200() throws Exception {
    when(service.getEntities(1L, new TransactionGetAllQueryBuilder(1L)))
        .thenReturn(Arrays.asList(tra1, tra2));
    String url = BASE_VALID_URL + "/transactions";

    this.mvc.perform(buildGetRequest(url)).andExpect(status().isOk());
    verify(service, times(1)).getEntities(eq(1L), any(TransactionGetAllQueryBuilder.class));
  }

  @Test
  public void testTransactionsControllerGetTransactions_authorizedButProjectIsGone_return404()
      throws Exception {
    when(service.getEntities(eq(4L), any(TransactionGetAllQueryBuilder.class)))
        .thenThrow(ProjectNotFoundException.class);
    String url = BASE_INVALID_URL + "/transactions";

    this.mvc.perform(buildGetRequest(url)).andExpect(status().isNotFound());
  }

  @Test
  public void testTransactionsControllerGetTransactions_unauthorized_return403() throws Exception {
    when(service.getEntities(3L, new TransactionGetAllQueryBuilder(3L)))
        .thenThrow(ProjectNotFoundException.class);
    String url = BASE_UNAUTHORIZED_URL + "/transactions";

    this.mvc.perform(buildGetRequest(url)).andExpect(status().isForbidden());
  }

  @Test
  public void testTransactionsControllerGetTransactionById_validTransaction_return200()
      throws Exception {
    when(service.getEntity(1L, 1L)).thenReturn(tra1);
    String url = BASE_VALID_URL + "/transactions/1";

    this.mvc.perform(buildGetRequest(url)).andExpect(status().isOk());
    verify(service, times(1)).getEntity(1L, 1L);
  }

  @Test
  public void
      testTransactionsControllerGetTransactionById_authorizedButTransactionIsGone_return404()
          throws Exception {
    when(service.getEntity(1L, 3L)).thenThrow(TransactionNotFoundException.class);
    String url = BASE_VALID_URL + "/transactions/3";

    this.mvc.perform(buildGetRequest(url)).andExpect(status().isNotFound());
    verify(service, times(1)).getEntity(1L, 3L);
  }

  @Test
  public void testTransactionsControllerGetTransactionById_authorizedButProjectIsGone_return404()
      throws Exception {
    when(service.getEntity(4L, 1L)).thenThrow(ProjectNotFoundException.class);
    String url = BASE_INVALID_URL + "/transactions/1";
    this.mvc.perform(buildGetRequest(url)).andExpect(status().isNotFound());
    verify(service, times(1)).getEntity(4L, 1L);
  }

  @Test
  public void testTransactionsControllerGetTransactionById_unauthorized_return403()
      throws Exception {
    when(service.getEntity(3L, 1L)).thenThrow(ProjectNotFoundException.class);
    String url = BASE_UNAUTHORIZED_URL + "/transactions/1";

    this.mvc.perform(buildGetRequest(url)).andExpect(status().isForbidden());
  }

  @Test
  public void testTransactionsControllerPostTransactionById_validInput_return201()
      throws Exception {
    when(service.postEntity(eq(1L), ArgumentMatchers.any()))
        .then(
            invocation -> {
              Transaction result = invocation.getArgument(1);
              Project project = result.getProject();
              Long categoryId = result.getCategory().getId();
              result.setCategory(new CategoryStub().id(categoryId).project(project));
              return result;
            });
    String url = BASE_VALID_URL + "/transactions";
    JSONObject body = new JSONObject();
    body.put("title", "New Transaction");
    body.put("description", "New!");
    body.put("amount", 30000L);
    body.put("project", "1");
    body.put("category", "1");
    body.put("transactionDate", "2020-01-01");

    this.mvc.perform(buildPostRequest(url, body.toString())).andExpect(status().isCreated());
    verify(service, times(1)).postEntity(eq(1L), ArgumentMatchers.any());
  }

  @Test
  public void testTransactionsControllerPostTransactionById_authorizedButProjectIsGone_return404()
      throws Exception {
    when(service.postEntity(eq(4L), ArgumentMatchers.any(Transaction.class)))
        .thenThrow(ProjectNotFoundException.class);
    String url = BASE_INVALID_URL + "/transactions";

    JSONObject body = new JSONObject();
    body.put("title", "New Transaction");
    body.put("description", "New!");
    body.put("amount", 30000L);
    body.put("project", "1");
    body.put("category", "1");
    body.put("transactionDate", "2020-03-03");

    this.mvc.perform(buildPostRequest(url, body.toString())).andExpect(status().isNotFound());
    verify(service, times(1)).postEntity(eq(4L), ArgumentMatchers.any(Transaction.class));
  }

  @Test
  public void testTransactionsControllerPostTransactionById_unauthorized_return403()
      throws Exception {
    when(service.postEntity(eq(3L), ArgumentMatchers.any(Transaction.class)))
        .thenThrow(ProjectNotFoundException.class);
    String url = BASE_UNAUTHORIZED_URL + "/transactions";

    JSONObject body = new JSONObject();
    body.put("title", "New Transaction");
    body.put("description", "New!");
    body.put("amount", 30000L);
    body.put("project", "1");
    body.put("category", "1");
    body.put("transactionDate", "2020-02-02");

    this.mvc.perform(buildPostRequest(url, body.toString())).andExpect(status().isForbidden());
  }

  @Test
  public void testTransactionsControllerPatchTransaction_validTransaction_return200()
      throws Exception {
    when(service.patchEntity(eq(1L), eq(1L), ArgumentMatchers.anyMap())).thenReturn(tra1);
    String url = BASE_VALID_URL + "/transactions/1";

    JSONObject body = new JSONObject();
    body.put("title", "New Transaction");
    body.put("description", "New!");
    body.put("amount", 30000L);
    body.put("category", "1");

    this.mvc.perform(buildPatchRequest(url, body.toString())).andExpect(status().isOk());
    verify(service, times(1)).patchEntity(eq(1L), eq(1L), ArgumentMatchers.anyMap());
  }

  @Test
  public void testTransactionsControllerPatchTransaction_authorizedButProjectIsGone_return404()
      throws Exception {
    when(service.patchEntity(eq(4L), eq(1L), ArgumentMatchers.anyMap()))
        .thenThrow(ProjectNotFoundException.class);
    String url = BASE_INVALID_URL + "/transactions/1";

    JSONObject body = new JSONObject();
    body.put("title", "New Transaction");
    body.put("description", "New!");
    body.put("amount", 30000L);
    body.put("category", "1");

    this.mvc.perform(buildPatchRequest(url, body.toString())).andExpect(status().isNotFound());
    verify(service, times(1)).patchEntity(eq(4L), eq(1L), ArgumentMatchers.anyMap());
  }

  @Test
  public void testTransactionsControllerPatchTransaction_authorizedButTransactionIsGone_return404()
      throws Exception {
    when(service.patchEntity(eq(1L), eq(3L), ArgumentMatchers.anyMap()))
        .thenThrow(TransactionNotFoundException.class);
    String url = BASE_VALID_URL + "/transactions/3";

    JSONObject body = new JSONObject();
    body.put("title", "New Transaction");
    body.put("description", "New!");
    body.put("amount", 30000L);
    body.put("category", "1");

    this.mvc.perform(buildPatchRequest(url, body.toString())).andExpect(status().isNotFound());
    verify(service, times(1)).patchEntity(eq(1L), eq(3L), ArgumentMatchers.anyMap());
  }

  @Test
  public void testTransactionsControllerPatchTransaction_unauthorized_return403() throws Exception {
    when(service.patchEntity(eq(3L), eq(1L), ArgumentMatchers.anyMap()))
        .thenThrow(ProjectNotFoundException.class);
    String url = BASE_UNAUTHORIZED_URL + "/transactions/1";

    JSONObject body = new JSONObject();
    body.put("title", "New Transaction");
    body.put("description", "New!");
    body.put("amount", 30000L);
    body.put("category", "1");

    this.mvc.perform(buildPatchRequest(url, body.toString())).andExpect(status().isForbidden());
  }

  @Test
  public void testTransactionsControllerDeleteTransactionById_validTransaction_return200()
      throws Exception {
    String url = BASE_VALID_URL + "/transactions/1";

    this.mvc.perform(buildDeleteRequest(url)).andExpect(status().isOk());
    verify(service, times(1)).deleteEntity(1L, 1L);
  }

  @Test
  public void testTransactionsControllerDeleteTransactionById_authorizedButProjectIsGone_return404()
      throws Exception {
    doThrow(ProjectNotFoundException.class).when(service).deleteEntity(4L, 1L);
    String url = BASE_INVALID_URL + "/transactions/1";

    this.mvc.perform(buildDeleteRequest(url)).andExpect(status().isNotFound());
    verify(service, times(1)).deleteEntity(4L, 1L);
  }

  @Test
  public void
      testTransactionsControllerDeleteTransactionById_authorizedButTransactionIsGone_return404()
          throws Exception {
    doThrow(ProjectNotFoundException.class).when(service).deleteEntity(1L, 3L);
    String url = BASE_VALID_URL + "/transactions/3";

    this.mvc.perform(buildDeleteRequest(url)).andExpect(status().isNotFound());
    verify(service, times(1)).deleteEntity(1L, 3L);
  }

  @Test
  public void testTransactionsControllerDeleteTransactionById_unauthorized_return403()
      throws Exception {
    doThrow(ProjectNotFoundException.class).when(service).deleteEntity(3L, 1L);
    String url = BASE_UNAUTHORIZED_URL + "/transactions/1";

    this.mvc.perform(buildDeleteRequest(url)).andExpect(status().isForbidden());
  }
}
