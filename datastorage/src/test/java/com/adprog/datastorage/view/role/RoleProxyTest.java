package com.adprog.datastorage.view.role;

import static org.assertj.core.api.Assertions.assertThat;

import com.adprog.datastorage.controller.stub.RoleStub;
import com.adprog.datastorage.controller.stub.UserStub;
import com.adprog.datastorage.models.AuthorityType;
import com.adprog.datastorage.models.Project;
import com.adprog.datastorage.models.Role;
import java.util.Collections;
import java.util.HashSet;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class RoleProxyTest {

  private static Role mock;

  @BeforeAll
  public static void setUp() {
    mock =
        new RoleStub()
            .id(1L)
            .name("Mock Role")
            .users(
                new HashSet<>(
                    Collections.singletonList(new UserStub().username("admin").fullName("Admin"))))
            .project(new Project(10L))
            .authorities(new HashSet<>(Collections.singleton(AuthorityType.GET)));
  }

  @Test
  public void testRoleViewProxy_shouldReturnCorrectJSONObject() {
    JSONObject result = new RoleViewProxy(mock).toSerializableObject();
    assertThat(result.get("name")).isEqualTo("Mock Role");
    assertThat(result.get("project").toString())
        .isEqualTo(new Project(10L).getProjectHref().toString());
  }

  @Test
  public void testRoleDetailProxy_shouldReturnCorrectJSONObject() {
    JSONObject result = new RoleDetailProxy(mock).toSerializableObject();
    assertThat(result.get("name")).isEqualTo("Mock Role");
    assertThat(result.get("project").toString())
        .isEqualTo(new Project(10L).getProjectHref().toString());

    System.out.println(result.toString());

    assertThat(result.has("authorities"));

    JSONArray users = (JSONArray) result.get("users");
    JSONObject user1 = (JSONObject) users.get(0);

    assertThat(user1.get("username")).isEqualTo("admin");
    assertThat(user1.get("fullName")).isEqualTo("Admin");
  }
}
