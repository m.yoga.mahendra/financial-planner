package com.adprog.datastorage.view.category;

import static org.assertj.core.api.Assertions.assertThat;

import com.adprog.datastorage.controller.stub.CategoryStub;
import com.adprog.datastorage.models.Category;
import com.adprog.datastorage.models.Project;
import java.util.Arrays;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class CategoryListProxyTest {

  private static List<Category> mock;

  @BeforeAll
  public static void setUp() {
    Category mock1, mock2;
    mock1 =
        new CategoryStub()
            .id(1L)
            .name("First Cat")
            .description("First Desc.")
            .type("I")
            .project(new Project(1L));
    mock2 =
        new CategoryStub()
            .id(2L)
            .name("Second Cat")
            .description("Second Desc.")
            .type("E")
            .project(new Project(1L));

    mock = Arrays.asList(mock1, mock2);
  }

  @Test
  public void testProxyCategoryList_shouldProduceCorrectJSONObject() {
    JSONArray result = new CategoryListProxy(mock).toSerializableArray();

    JSONObject res1 = (JSONObject) result.get(0);
    assertThat(res1.get("id")).isEqualTo(1L);
    assertThat(res1.get("name")).isEqualTo("First Cat");
    assertThat(res1.get("type")).isEqualTo("I");
    assertThat(res1.get("href")).isEqualTo("/api/projects/1/categories/1");

    JSONObject res2 = (JSONObject) result.get(1);
    assertThat(res2.get("id")).isEqualTo(2L);
    assertThat(res2.get("name")).isEqualTo("Second Cat");
    assertThat(res2.get("type")).isEqualTo("E");
    assertThat(res2.get("href")).isEqualTo("/api/projects/1/categories/2");
  }
}
