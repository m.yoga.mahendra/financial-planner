package com.adprog.datastorage.view.role;

import static org.assertj.core.api.Assertions.assertThat;

import com.adprog.datastorage.controller.stub.RoleStub;
import com.adprog.datastorage.models.Project;
import com.adprog.datastorage.models.Role;
import java.util.Arrays;
import java.util.Collection;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class RoleCollectionProxyTest {

  private static Collection<Role> mock;

  @BeforeAll
  public static void setUp() {
    mock =
        Arrays.asList(
            new RoleStub().id(12L).name("Mock Role 1").project(new Project(3L)),
            new RoleStub().id(13L).name("Mock Role 2").project(new Project(4L)));
  }

  @Test
  public void testRoleCollectionProxyTest_shouldReturnCorrectJSONObject() {
    JSONArray result = new RoleCollectionProxy(mock).toSerializableArray();
    JSONObject role1 = (JSONObject) result.get(0);
    JSONObject role2 = (JSONObject) result.get(1);

    assertThat(role1.get("name")).isEqualTo("Mock Role 1");
    assertThat(role1.get("project").toString())
        .isEqualTo(new Project(3L).getProjectHref().toString());

    assertThat(role2.get("name")).isEqualTo("Mock Role 2");
    assertThat(role2.get("project").toString())
        .isEqualTo(new Project(4L).getProjectHref().toString());
  }
}
