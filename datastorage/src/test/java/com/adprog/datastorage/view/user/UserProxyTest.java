package com.adprog.datastorage.view.user;

import static org.assertj.core.api.Assertions.assertThat;

import com.adprog.datastorage.controller.stub.ProjectStub;
import com.adprog.datastorage.controller.stub.RoleStub;
import com.adprog.datastorage.controller.stub.UserStub;
import com.adprog.datastorage.models.AuthorityType;
import com.adprog.datastorage.models.Role;
import com.adprog.datastorage.models.User;
import java.util.Collections;
import java.util.HashSet;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class UserProxyTest {

  private static User mock;

  @BeforeAll
  public static void setUp() {
    Role mockRole =
        new RoleStub()
            .id(1L)
            .project(new ProjectStub().id(1L))
            .authorities(new HashSet<>(Collections.singleton(AuthorityType.GET)))
            .users(Collections.singleton(mock))
            .name("Mock Role");
    mock =
        new UserStub()
            .username("admin")
            .email("admin@gmail.com")
            .password("admin")
            .fullName("Admin")
            .roles(Collections.singletonList(mockRole));
  }

  @Test
  public void testUserViewProxy_shouldReturnCorrectJSONObject() {
    JSONObject result = new UserViewProxy(mock).toSerializableObject();
    assertThat(result.get("username")).isEqualTo("admin");
    assertThat(result.get("fullName")).isEqualTo("Admin");
    assertThat(result.get("href")).isEqualTo("/api/users/admin");
    assertThat(result.has("password")).isFalse(); // Password should not be exposed!
  }

  @Test
  public void testUserDetailProxy_shouldReturnCorrectJSONObject() {
    JSONObject result = new UserDetailsProxy(mock).toSerializableObject();
    assertThat(result.get("username")).isEqualTo("admin");
    assertThat(result.get("fullName")).isEqualTo("Admin");
    assertThat(result.get("email")).isEqualTo("admin@gmail.com");

    JSONArray roles = (JSONArray) result.get("roles");
    assertThat(roles.toList()).hasSize(1);

    JSONObject roleRepr = (JSONObject) roles.get(0);
    JSONObject roleProject = (JSONObject) roleRepr.get("project");
    assertThat(roleProject.get("id")).isEqualTo(1L);
    assertThat(roleProject.get("href")).isEqualTo("/api/projects/1");
    assertThat(roleRepr.get("name")).isEqualTo("Mock Role");
  }
}
