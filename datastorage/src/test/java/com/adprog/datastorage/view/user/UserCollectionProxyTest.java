package com.adprog.datastorage.view.user;

import static org.assertj.core.api.Assertions.assertThat;

import com.adprog.datastorage.controller.stub.UserStub;
import com.adprog.datastorage.models.User;
import java.util.Arrays;
import java.util.Collection;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class UserCollectionProxyTest {

  private static Collection<User> mock;

  @BeforeAll
  public static void setUp() {
    mock =
        Arrays.asList(
            new UserStub().username("admin").fullName("Admin"),
            new UserStub().username("cykes123").fullName("Cykes"));
  }

  @Test
  public void testUserCollectionProxyTest_shouldReturnCorrectJSONObject() {
    JSONArray result = new UserCollectionProxy(mock).toSerializableArray();

    JSONObject user1 = (JSONObject) result.get(0);
    assertThat(user1.get("username")).isEqualTo("admin");
    assertThat(user1.get("fullName")).isEqualTo("Admin");

    JSONObject user2 = (JSONObject) result.get(1);
    assertThat(user2.get("username")).isEqualTo("cykes123");
    assertThat(user2.get("fullName")).isEqualTo("Cykes");
  }
}
