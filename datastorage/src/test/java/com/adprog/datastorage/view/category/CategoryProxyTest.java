package com.adprog.datastorage.view.category;

import static org.assertj.core.api.Assertions.assertThat;

import com.adprog.datastorage.controller.stub.CategoryStub;
import com.adprog.datastorage.models.Category;
import com.adprog.datastorage.models.Project;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class CategoryProxyTest {

  private static Category mock;

  @BeforeAll
  static void setUp() {
    mock =
        new CategoryStub()
            .id(1L)
            .name("Mock Category")
            .description("Mock Desc.")
            .type("I")
            .project(new Project(1L));
  }

  @Test
  public void testProxyCategoryView_shouldProduceCorrectJSONObject() {
    JSONObject result = new CategoryViewProxy(mock).toSerializableObject();
    assertThat(result.get("id")).isEqualTo(1L);
    assertThat(result.get("name")).isEqualTo("Mock Category");
    assertThat(result.get("type")).isEqualTo("I");
  }

  @Test
  public void testProxyCategoryDetail_shouldProduceCorrectJSONObject() {
    JSONObject result = new CategoryDetailProxy(mock).toSerializableObject();
    assertThat(result.get("id")).isEqualTo(1L);
    assertThat(result.get("name")).isEqualTo("Mock Category");
    assertThat(result.get("type")).isEqualTo("I");
    assertThat(result.get("description")).isEqualTo("Mock Desc.");

    JSONObject projectRef = (JSONObject) result.get("project");
    assertThat(projectRef.get("id")).isEqualTo(1L);
    assertThat(projectRef.get("href")).isEqualTo("/api/projects/1");
  }
}
