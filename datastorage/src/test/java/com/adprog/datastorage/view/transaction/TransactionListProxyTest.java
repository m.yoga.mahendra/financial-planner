package com.adprog.datastorage.view.transaction;

import static org.assertj.core.api.Assertions.assertThat;

import com.adprog.datastorage.controller.stub.CategoryStub;
import com.adprog.datastorage.controller.stub.ProjectStub;
import com.adprog.datastorage.controller.stub.TransactionStub;
import com.adprog.datastorage.models.Category;
import com.adprog.datastorage.models.Project;
import com.adprog.datastorage.models.Transaction;
import java.util.Arrays;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class TransactionListProxyTest {

  private static List<Transaction> mock;

  @BeforeAll
  public static void setUp() {
    Project pRef = new ProjectStub().id(1L).title("Mock Project");
    Category cRef = new CategoryStub().id(1L).name("Income").project(pRef);
    mock =
        Arrays.asList(
            new TransactionStub()
                .id(1L)
                .project(pRef)
                .category(cRef)
                .title("Mock Transaction")
                .amount(20000L),
            new TransactionStub()
                .id(2L)
                .project(pRef)
                .category(cRef)
                .title("Mock Transaction 2")
                .amount(10000L));
  }

  @Test
  public void testProxyTransactionView() {
    JSONArray result = new TransactionListProxy(mock).toSerializableArray();
    JSONObject res1, res2, projectRef, categoryRef;

    res1 = (JSONObject) result.get(0);
    assertThat(res1.get("id")).isEqualTo(1L);
    assertThat(res1.get("title")).isEqualTo("Mock Transaction");
    assertThat(res1.get("amount")).isEqualTo(20000L);
    assertThat(res1.get("href")).isEqualTo("/api/projects/1/transactions/1");

    projectRef = (JSONObject) res1.get("project");
    assertThat(projectRef.get("href")).isEqualTo("/api/projects/1");

    categoryRef = (JSONObject) res1.get("category");
    assertThat(categoryRef.get("href")).isEqualTo("/api/projects/1/categories/1");

    res2 = (JSONObject) result.get(1);
    assertThat(res2.get("id")).isEqualTo(2L);
    assertThat(res2.get("title")).isEqualTo("Mock Transaction 2");
    assertThat(res2.get("amount")).isEqualTo(10000L);
    assertThat(res2.get("href")).isEqualTo("/api/projects/1/transactions/2");

    projectRef = (JSONObject) res2.get("project");
    assertThat(projectRef.get("href")).isEqualTo("/api/projects/1");

    categoryRef = (JSONObject) res2.get("category");
    assertThat(categoryRef.get("href")).isEqualTo("/api/projects/1/categories/1");
  }
}
