package com.adprog.datastorage.view.project;

import static org.assertj.core.api.Assertions.assertThat;

import com.adprog.datastorage.controller.stub.ProjectStub;
import com.adprog.datastorage.models.Project;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class ProjectListProxyTest {

  private static List<Project> mock;

  @BeforeAll
  static void setUp() {
    Project mock1 =
        new ProjectStub()
            .id(1L)
            .title("Mock Title")
            .description("Mock Description")
            .creationDate(new Timestamp(0L))
            .lastUpdated(new Timestamp(0L));

    Project mock2 =
        new ProjectStub()
            .id(2L)
            .title("Mock Title 2")
            .description("Mock Description 2")
            .creationDate(new Timestamp(10L))
            .lastUpdated(new Timestamp(10L));

    mock = Arrays.asList(mock1, mock2);
  }

  @Test
  public void testProxyProjectList_shouldProduceCorrectJSONObject() {
    JSONArray result = new ProjectListProxy(mock).toSerializableArray();

    JSONObject project;
    project = (JSONObject) result.get(0);
    assertThat(project.get("id")).isEqualTo(1L);
    assertThat(project.get("title")).isEqualTo("Mock Title");
    assertThat(project.get("description")).isEqualTo("Mock Description");
    assertThat(project.get("href")).isEqualTo("/api/projects/1");

    project = (JSONObject) result.get(1);
    assertThat(project.get("id")).isEqualTo(2L);
    assertThat(project.get("title")).isEqualTo("Mock Title 2");
    assertThat(project.get("description")).isEqualTo("Mock Description 2");
    assertThat(project.get("href")).isEqualTo("/api/projects/2");
  }
}
