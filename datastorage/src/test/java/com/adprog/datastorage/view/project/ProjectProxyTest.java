package com.adprog.datastorage.view.project;

import static org.assertj.core.api.Assertions.assertThat;

import com.adprog.datastorage.controller.stub.ProjectStub;
import com.adprog.datastorage.models.Project;
import java.sql.Timestamp;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class ProjectProxyTest {

  private static Project mock;

  @BeforeAll
  static void setUp() {
    mock =
        new ProjectStub()
            .id(1L)
            .title("Mock Title")
            .description("Mock Description")
            .creationDate(new Timestamp(0L))
            .lastUpdated(new Timestamp(0L));
  }

  @Test
  public void testProxyProjectView_shouldProduceCorrectJSONObject() {
    JSONObject result = new ProjectViewProxy(mock).toSerializableObject();
    assertThat(result.get("id")).isEqualTo(1L);
    assertThat(result.get("title")).isEqualTo("Mock Title");
    assertThat(result.get("description")).isEqualTo("Mock Description");
  }

  @Test
  public void testProxyProjectDetail_shouldProduceCorrectJSONObject() {
    JSONObject result = new ProjectDetailProxy(mock).toSerializableObject();
    assertThat(result.get("id")).isEqualTo(1L);
    assertThat(result.get("title")).isEqualTo("Mock Title");
    assertThat(result.get("description")).isEqualTo("Mock Description");
    assertThat(result.get("creationDate")).isEqualTo(new Timestamp(0L));
    assertThat(result.get("lastUpdated")).isEqualTo(new Timestamp(0L));
  }
}
