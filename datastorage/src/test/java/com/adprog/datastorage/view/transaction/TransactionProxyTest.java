package com.adprog.datastorage.view.transaction;

import static org.assertj.core.api.Assertions.assertThat;

import com.adprog.datastorage.controller.stub.CategoryStub;
import com.adprog.datastorage.controller.stub.ProjectStub;
import com.adprog.datastorage.controller.stub.TransactionStub;
import com.adprog.datastorage.models.Category;
import com.adprog.datastorage.models.Project;
import com.adprog.datastorage.models.Transaction;
import java.sql.Date;
import java.sql.Timestamp;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class TransactionProxyTest {
  private static Transaction mock;

  @BeforeAll
  public static void setUp() {
    Project pRef = new ProjectStub().id(1L).title("Mock Project");
    Category cRef = new CategoryStub().id(1L).name("Income").project(pRef);
    mock =
        new TransactionStub()
            .id(1L)
            .project(pRef)
            .category(cRef)
            .title("Mock Transaction")
            .description("Desc.")
            .amount(20000L)
            .transactionDate(Date.valueOf("2020-01-01"))
            .submitDate(new Timestamp(0L));
  }

  @Test
  public void testProxyTransactionView() {
    JSONObject result = new TransactionViewProxy(mock).toSerializableObject();
    assertThat(result.get("id")).isEqualTo(1L);
    assertThat(result.get("title")).isEqualTo("Mock Transaction");
    assertThat(result.get("amount")).isEqualTo(20000L);

    JSONObject projectRef = (JSONObject) result.get("project");
    assertThat(projectRef.get("href")).isEqualTo("/api/projects/1");

    JSONObject categoryRef = (JSONObject) result.get("category");
    assertThat(categoryRef.get("href")).isEqualTo("/api/projects/1/categories/1");
  }

  @Test
  public void testProxyTransactionDetails() {
    JSONObject result = new TransactionDetailProxy(mock).toSerializableObject();
    System.out.println(result.toString());
    assertThat(result.get("id")).isEqualTo(1L);
    assertThat(result.get("title")).isEqualTo("Mock Transaction");
    assertThat(result.get("description")).isEqualTo("Desc.");
    assertThat(result.get("amount")).isEqualTo(20000L);
    assertThat(result.get("transactionDate")).isEqualTo(Date.valueOf("2020-01-01"));
    assertThat(result.get("submitDate")).isEqualTo(new Timestamp(0L));

    JSONObject projectRef = (JSONObject) result.get("project");
    assertThat(projectRef.get("id")).isEqualTo(1L);
    assertThat(projectRef.get("title")).isEqualTo("Mock Project");
    assertThat(projectRef.get("href")).isEqualTo("/api/projects/1");

    JSONObject categoryRef = (JSONObject) result.get("category");
    assertThat(categoryRef.get("id")).isEqualTo(1L);
    assertThat(categoryRef.get("name")).isEqualTo("Income");
    assertThat(categoryRef.get("href")).isEqualTo("/api/projects/1/categories/1");
  }
}
