package com.adprog.datastorage;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DataStorageIntegrationTests {

  @Test
  void contextLoads() {
    DataStorageApplication.main(new String[] {});
  }
}
