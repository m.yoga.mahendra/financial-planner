# Financial Planner

[![pipeline status](https://gitlab.com/m.yoga.mahendra/financial-planner/badges/master/pipeline.svg)](https://gitlab.com/m.yoga.mahendra/financial-planner/-/commits/master)

[![coverage report](https://gitlab.com/m.yoga.mahendra/financial-planner/badges/master/coverage.svg)](https://gitlab.com/m.yoga.mahendra/financial-planner/-/commits/master)

Repository for project *Financial Planner*.

*Financial Planner* is a web application that manages your income/expense data, and present the analysis of your cash flow as a series of graphs and charts. Users can create and manage several journals, and each journal can be accessed by several users.

Included in this project is a REST API Webservice that can be accessed by public, so you do not need to depend on the web interface to manage your data on our service. For more information, check out the Data Storage module [here](https://gitlab.com/m.yoga.mahendra/financial-planner/-/tree/master/datastorage).

Currently, every journal is public, but we are looking forward to implement authentication methods and customizable user role for each project.

This project is created for the final assignment of *Advanced Programming* course (CSCM602023) at the Faculty of Computer Science, University of Indonesia (https://www.cs.ui.ac.id)

Members: 

1. Inigo Ramli (1806133742) - [@IgoRamli](https://gitlab.com/IgoRamli)
2. Rony Agus Vian (1806205691)
3. Audilla Putri Ferialdi (1806186755)
4. Muhamad Yoga Mahendra (1806205256) - [@m.yoga.mahendra](https://gitlab.com/m.yoga.mahendra)
5. Muhammad Destara Syanandi (1806186856)

