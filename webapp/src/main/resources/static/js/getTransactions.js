/*jslint browser */
/*global window */
/*global $*/
$(document).ready(function () {
    "use strict";
    fetch_data({});
    $("#submit").on("click", function () {
        // Get list of checked categories
        var categoryChoice = document.getElementsByName("categories");
        var categories = "";

        for (var checkbox of categoryChoice) {
            if (checkbox.checked) {
                if (categories.length > 0) {
                    categories += ",";
                }
                categories += checkbox.value;
            }
        }

        var startDate = document.getElementById("startDate").value;
        var endDate = document.getElementById("endDate").value;
        var data = {};
        data["limit"] = 10;
        if (startDate !== "") {
            data["startDate"] = startDate;
        }
        if (endDate !== "") {
            data["endDate"] = endDate;
        }
        data["categories"] = categories;
        console.log(data);
        fetch_data(data);
    });
});

function fetch_data(query) {
    var list = document.getElementById("transaction-list");
    var clearedList = document.getElementById("transaction-list").cloneNode(false);
    list.parentNode.replaceChild(clearedList, list);

    // Link: http://financial-planner-adpro.herokuapp.com/projects/{projectId}/category-list
    var paths = window.location.href.toString().split("/");
    var pid = paths[paths.length - 2];
    var authToken = window.localStorage.getItem("Authorization");
    $.ajax({
        method: "GET",
        url: "http://financial-planner-datastorage.herokuapp.com/api/projects/" + pid + "/transactions/",
        async: true,
        crossDomain: true,
        headers: {
            "Authorization": authToken
        },
        data: query,
        success: function (data) {
            var json = JSON.parse(data);
            $.each(json, function (i, value) {
                var entry = document.createElement("li");
                entry.setAttribute("class", "list-group-item");

                var titleEntry = document.createElement("div");
                titleEntry.setAttribute("class", "trans");
                titleEntry.innerHTML = value.title;
                $(entry).append(titleEntry);
                $(entry).append(document.createElement("span"));

                var amountEntry = document.createElement("div");
                amountEntry.setAttribute("class", "amount");
                amountEntry.innerHTML = value.amount;
                $(entry).append(amountEntry);
                $(entry).append(document.createElement("span"));

                var dateEntry = document.createElement("div");
                dateEntry.setAttribute("class", "amount");
                dateEntry.innerHTML = value.transactionDate;
                $(entry).append(dateEntry);
                $(entry).append(document.createElement("span"));

                var editAction = document.createElement("a");
                var urlEdit = "/projects/" + pid + "/transactions/" + value.id + "/edit";
                editAction.setAttribute("href", urlEdit);
                editAction.innerHTML = "Edit Transaction";
                $(entry).append(editAction);
                $(entry).append(document.createElement("span"));

                var deleteAction = document.createElement("button");
                deleteAction.setAttribute("value", value.id);
                deleteAction.innerHTML = "Delete Transaction";
                $(entry).append(deleteAction);

                $("#transaction-list").append(entry);
            });
        },
        error: function (xhr, resp, text) {
            window.alert("ERROR: Tidak bisa mengakses daftar transaksi.");
            window.console.log(xhr, resp, text);
        }
    });
}
