/*jslint browser */
/*global window */
/*global $*/
$(document).ready(function () {
    "use strict";
    // Link: http://financial-planner-adpro.herokuapp.com/projects/{projectId}/xyz
    var paths = window.location.href.toString().split("/");
    var pid = paths[paths.length - 2];
    var authToken = window.localStorage.getItem("Authorization");
    $("#deleterole").on("click", function () {
        var roleid = document.getElementById("roleid").value;
        var username = document.getElementById("username").value;
        $.ajax({
            type: "DELETE",
            url: "http://financial-planner-datastorage0.herokuapp.com/api/projects/" + pid + "/roles/" + roleid + "/users/" + username,
            dataType: "json",
            async: true,
            crossDomain: true,
            contentType: "application/json; charset=UTF-8",
            headers: {
                "Authorization": authToken
            },
            success: function (result) {
                window.alert("User " + username + " telah kehilangan role.");
                window.console.log(result);
            },
            error: function (xhr, resp, text) {
                window.alert("User " + username + " gagal kehilangan role.");
                window.console.log(xhr, resp, text);
            }
        });
    });
});