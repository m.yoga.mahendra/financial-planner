/*jslint browser */
/*global window */
/*global $*/
$(document).ready(function () {
    "use strict";
    var token = window.localStorage.getItem("Authorization");
    $.ajax({
        type: "GET",
        url: "http://financial-planner-datastorage.herokuapp.com/api/projects",
        dataType: "json",
        async: true,
        crossDomain: true,
        contentType: "application/json; charset=UTF-8",
        headers: {
            "Authorization": token
        },
        statusCode: {
            "200": function (data, result, ignore) { // Unmodified from home.js
                var str = "";
                for (var key in data) {
                    var link = "/projects/" + data[key]["id"];
                    str += "<tr>" +
                        "<td>" + data[key]["title"] + "</td>" +
                        "<td>" + data[key]["description"] + "</td>" +
                        "<td>" + "<a href=" + link + " class='button'>Click Here</a>" + "<td>" +
                        "</tr>";
                }
                $("#home_table").prepend(str);
            },
            "400": function (xhr, resp, text) {     // Bad request
                window.alert("Bad Request. Please input the correct data.");
                window.console.log(xhr, resp, text);
            },
            "403": function (xhr, resp, text) {     // User is not authorized
                window.console.log(xhr, resp, text);
                window.location.replace("/403");
            },
            "404": function (xhr, resp, text) {     // Data does not exist
                window.console.log(xhr, resp, text);
                window.location.replace("/404");
            },
            "500": function (xhr, resp, text) {     // Internal Server Error
                window.alert("Internal Server Error. Please contact Yoga Mahendra(Developer)");
                window.console.log(xhr, resp, text);
            }
        }
    });
});