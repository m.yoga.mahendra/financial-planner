/*jslint browser */
/*global window */
function checkAuth() {
    "use strict";
    var authToken = window.localStorage.getItem("Authorization");
    if (authToken === null || authToken === "") {
        window.location.replace("/login");
    } else {
        window.location.replace("/home");
    }
}
window.onload = function () {
    "use strict";
    checkAuth();
};