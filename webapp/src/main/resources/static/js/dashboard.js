/*jslint browser */
/*global window */
/*global $*/
function loadBasicInfo() {
    "use strict";
    // Link: http://financial-planner-adpro.herokuapp.com/projects/{projectId}
    var paths = window.location.href.toString().split("/");
    var pid = paths[paths.length - 1];
    var authToken = window.localStorage.getItem("Authorization");
    $.ajax({
        type: "POST",
        url: "http://financial-planner-analytics.herokuapp.com/api/" + pid + "/basic-info",
        dataType: "json",
        async: true,
        crossDomain: true,
        contentType: "application/json; charset=UTF-8",
        headers: {
            "Authorization": authToken
        },
        success: function (data) {
            var info = JSON.parse(data);
            document.getElementById("projectName").innerHTML = info["Project Name"];
            document.getElementById("projectDesc").innerHTML = info["Project Description"];
            document.getElementById("projectTE").innerHTML = info["Total Project Expense"];
            document.getElementById("projectTI").innerHTML = info["Total Project Income"];
            document.getElementById("projectDEI").innerHTML = info["Total Project E/I Difference"];
            document.getElementById("projectStatus").innerHTML = info["Project Status"];
        },
        error: function (xhr, resp, text) {
            window.alert("ERROR: Basic Info gagal dimuat, karena " + resp.message);
            window.console.log(xhr, resp, text);
        }
    });
}

window.onload = function () {
    "use strict";
    loadBasicInfo();
};
