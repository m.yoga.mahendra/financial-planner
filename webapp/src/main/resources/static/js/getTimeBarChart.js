/*jslint browser */
/*global window */
/*global $*/
$(document).ready(function () {
    "use strict";
    // Link: http://financial-planner-adpro.herokuapp.com/projects/{projectId}/xyz
    var paths = window.location.href.toString().split("/");
    var pid = paths[paths.length - 2];
    var authToken = window.localStorage.getItem("Authorization");
    var body = {"type": "bar"};
    $.ajax({
        method: "GET",
        url: "http://financial-planner-analytics.herokuapp.com/api/projects/api/" + pid + "/graph/times",
        async: true,
        crossDomain: true,
        headers: {
            "Authorization": authToken
        },
        data: body,
        success: function (data) {
            $.each(data, function (key, value) {
                var chart = document.createElement("img");
                chart.src = "data:image/jpeg;base64," + value.imageB64;
                $("#piechart").append(chart);
                // or use this: document.querySelector('#piechart').innerHTML = chart.outerHTML;
            });
        },
        error: function (xhr, resp, text) {
            window.alert("ERROR: Gagal mendapatkan Bar Chart times.");
            window.console.log(xhr, resp, text);
        }
    });
});