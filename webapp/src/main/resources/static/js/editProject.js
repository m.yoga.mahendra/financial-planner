/*jslint browser */
/*global window */
/*global $*/
$(document).ready(function () {
    "use strict";
    // Link: http://financial-planner-adpro.herokuapp.com/projects/{projectId}
    var paths = window.location.href.toString().split("/");
    var pid = paths[paths.length - 1];
    var authToken = window.localStorage.getItem("Authorization");
    $("#submit").on("click", function () {
        var title = document.getElementById("name").value;
        var description = document.getElementById("desc").value;
        var data = {};
        if (title !== "") {
            data.title = title;
        }
        if (description !== "") {
            data.description = description;
        }
        $.ajax({
            type: "PATCH",
            url: "http://financial-planner-datastorage0.herokuapp.com/api/projects/" + pid,
            dataType: "json",
            async: true,
            crossDomain: true,
            data: data,
            contentType: "application/json; charset=UTF-8",
            headers: {
                "Authorization": authToken
            },
            success: function (result) {
                window.alert("Proyek berhasil diupdate.");
                window.console.log(result);
            },
            error: function (xhr, resp, text) {
                window.alert("ERROR: Proyek gagal diupdate.");
                window.console.log(xhr, resp, text);
            }
        });
    });
});