/*jslint browser */
/*global window */
/*global $*/
$(document).ready(function () {
    "use strict";
    $("#submit").on("click", function () {
        var uname = document.getElementById("username").value;
        var pwd = document.getElementById("password").value;
        var data = {
            "username": uname,
            "password": pwd
        };
        $.ajax({
            type: "POST",
            url: "http://financial-planner-datastorage.herokuapp.com/login",
            dataType: "text",
            async: true,
            crossDomain: true,
            data: JSON.stringify(data),
            contentType: "application/json; charset=UTF-8",
            headers: {
                "Access-Control-Expose-Headers": "Authorization"
            },
            statusCode: {
                "200": function (data, result, ignore) {
                    window.alert("Login Successful");
                    window.localStorage.setItem("Authorization", data);
                    window.console.log(result);
                    window.location.replace("/home");
                },
                "401": function (xhr, resp, text) {
                    window.alert("Login Failed : " + resp.message);
                    window.console.log(xhr, resp, text);
                }
            }
        });
    });
});