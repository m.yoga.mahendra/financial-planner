/*jslint browser */
/*global window */
/*global $*/
$(document).ready(function () {
    "use strict";
    var list = document.getElementById("category-list");
    var clearedList = document.getElementById("category-list").cloneNode(false);
    list.parentNode.replaceChild(clearedList, list);

    // Link: http://financial-planner-adpro.herokuapp.com/projects/{projectId}/log
    var paths = window.location.href.toString().split("/");
    var pid = paths[paths.length - 2];
    var authToken = window.localStorage.getItem("Authorization");
    $.ajax({
        method: "GET",
        url: "http://financial-planner-datastorage.herokuapp.com/api/projects/" + pid + "/categories/",
        async: true,
        crossDomain: true,
        headers: {
            "Authorization": authToken
        },
        data: {},
        success: function (data) {
            var json = JSON.parse(data);
            $.each(json, function (i, value) {
                var entry = document.createElement("li");
                entry.setAttribute("class", "list-group-item");

                var titleEntry = document.createElement("div");
                titleEntry.setAttribute("class", "name");
                titleEntry.innerHTML = value.title;
                $(entry).append(titleEntry);
                $(entry).append(document.createElement("span"));

                var descEntry = document.createElement("div");
                descEntry.setAttribute("class", "description");
                descEntry.innerHTML = value.amount;
                $(entry).append(descEntry);
                $(entry).append(document.createElement("span"));

                var editAction = document.createElement("a");
                var urlEdit = "/projects/" + pid + "/category/" + value.id + "/edit";
                editAction.setAttribute("href", urlEdit);
                editAction.innerHTML = "Edit Transaction";
                $(entry).append(editAction);
                $(entry).append(document.createElement("span"));

                var deleteAction = document.createElement("button");
                deleteAction.setAttribute("value", value.id);
                deleteAction.innerHTML = "Delete Transaction";
                $(entry).append(deleteAction);

                $("#category-list").append(entry);
            });
        },
        error: function (xhr, resp, text) {
            window.alert("ERROR: Tidak bisa mengakses daftar transaksi.");
            window.console.log(xhr, resp, text);
        }
    });
});

