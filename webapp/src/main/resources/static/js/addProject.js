/*jslint browser */
/*global window */
/*global $*/
$(document).ready(function () {
    "use strict";
    $("#proj_submit").on("click", function () {
        var authToken = window.localStorage.getItem("Authorization");
        var title = document.getElementById("title").value;
        var desc = document.getElementById("description").value;
        var data = {
            "title": title,
            "description": desc
        };
        $.ajax({
            type: "POST",
            url: "http://financial-planner-datastorage.herokuapp.com/api/projects",
            dataType: "json",
            async: true,
            crossDomain: true,
            data: JSON.stringify(data),
            contentType: "application/json; charset=UTF-8",
            headers: {
                "Authorization": authToken
            },
            success: function (result) {
                window.alert("Proyek " + title + " berhasil dibuat.");
                window.console.log(result);
            },
            error: function (xhr, resp, text) {
                window.alert("ERROR: Proyek " + title + " gagal dibuat.");
                window.console.log(xhr, resp, text);
            }
        });
    });
});