/*jslint browser */
/*global window */
/*global $*/
$(document).ready(function () {
    "use strict";
    // Link: http://financial-planner-adpro.herokuapp.com/projects/{projectId}
    var paths = window.location.href.toString().split("/");
    var pid = paths[paths.length - 1];
    var authToken = window.localStorage.getItem("Authorization");
    $("#button3").on("click", function () {
        $.ajax({
            type: "DELETE",
            url: "http://financial-planner-datastorage.herokuapp.com/api/projects/" + pid,
            dataType: "json",
            async: true,
            crossDomain: true,
            contentType: "application/json; charset=UTF-8",
            headers: {
                "Authorization": authToken
            },
            success: function (result) {
                window.alert("Proyek berhasil dihapus.");
                window.console.log(result);
                window.location.replace("/home");
            },
            error: function (xhr, resp, text) {
                window.alert("ERROR: Proyek gagal dihapus.");
                window.console.log(xhr, resp, text);
            }
        });
    });
});