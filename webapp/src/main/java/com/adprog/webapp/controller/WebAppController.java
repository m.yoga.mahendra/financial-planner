package com.adprog.webapp.controller;

// import java.net.URI;
// import java.net.http.HttpRequest;
// import java.net.http.HttpResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class WebAppController {

  @RequestMapping(value = "/", method = RequestMethod.GET)
  public String landingPage() {
    return "landing";
  }

  @RequestMapping(value = "/register", method = RequestMethod.GET) // panggil register datastorage
  public String registerPage() {

    return "register";
  }

  @RequestMapping(path = "/login", method = RequestMethod.GET) // panggil login datastorage
  public String loginPage() {

    return "login";
  }

  @RequestMapping(value = "/home", method = RequestMethod.GET) // panggil getprojects datastorage
  public String selectProjectPage() {

    return "home";
  }

  @RequestMapping(
      value = "/projects/{projectId}",
      method = RequestMethod.GET) // panggil ajax call piechart ke analytics
  public String dashboardPage(Model model, @PathVariable("projectId") Long projectId) {

    return "dashboard";
  }

  @RequestMapping(
      value = "/projects/{projectId}/log",
      method = RequestMethod.GET) // panggil ajax call piechart ke analytics
  public String logPage(Model model, @PathVariable("projectId") Long projectId) {
    model.addAttribute("projectId", projectId);

    return "log";
  }

  @RequestMapping(
      value = "/projects/{projectId}/category-list",
      method = RequestMethod.GET) // panggil ajax call piechart ke analytics
  public String categoryList(Model model, @PathVariable("projectId") Long projectId) {
    model.addAttribute("projectId", projectId);

    return "categorylist";
  }

  @RequestMapping(value = "/projects/{projectId}/contributor", method = RequestMethod.GET)
  public String contributorPage(Model model, @PathVariable("projectId") Long projectId) {

    return "contributor";
  }

  @RequestMapping(value = "/projects/{projectId}/role", method = RequestMethod.GET)
  public String rolePage(Model model, @PathVariable("projectId") Long projectId) {

    return "newRole";
  }

  @RequestMapping(value = "/add-project", method = RequestMethod.GET)
  public String createProject(Model model) {
    return "addProject";
  }

  @RequestMapping(value = "/projects/{projectId}/add-category", method = RequestMethod.GET)
  public String createCategories(Model model) {
    return "addCategories";
  }

  @RequestMapping(path = "/projects/{projectId}/add-transaction", method = RequestMethod.GET)
  public String createExpensePage(Model model, @PathVariable("projectId") Long projectId) {
    return "addTrans";
  }

  @RequestMapping(
      value = "/projects/{projectId}/transactions/{transactionId}/edit",
      method = RequestMethod.GET)
  public String editTransaction(
      Model model,
      @PathVariable("projectId") Long projectId,
      @PathVariable("transactionId") Long transactionId) {
    return "edittransaction";
  }

  @RequestMapping(
      value = "/projects/{projectId}/categories/{categoryId}/edit",
      method = RequestMethod.GET)
  public String editCategories(
      Model model,
      @PathVariable("projectId") Long projectId,
      @PathVariable("categoryId") Long categoryId) {
    return "editCategories";
  }

  @RequestMapping(value = "/projects/{projectId}/edit", method = RequestMethod.GET)
  public String editProject(Model model) {
    return "editProject";
  }

  @RequestMapping(value = "/403", method = RequestMethod.GET)
  public String error403() {
    return "403";
  }

  @RequestMapping(value = "/404", method = RequestMethod.GET)
  public String error404() {
    return "404";
  }
}
