package com.adprog.webapp.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(controllers = WebAppController.class)
public class ControllerTest {
  @Autowired private MockMvc mockMvc;

  @Test
  public void landingURLCanBeAccessed() throws Exception {
    mockMvc.perform(get("/")).andExpect(status().isOk()).andExpect(view().name("landing"));
  }

  @Test
  public void registerURLCanBeAccessed() throws Exception {
    System.out.println(System.getenv("USERNAME"));
    mockMvc.perform(get("/register")).andExpect(status().isOk()).andExpect(view().name("register"));
  }

  @Test
  public void loginURLCanBeAccessed() throws Exception {
    mockMvc.perform(get("/login")).andExpect(status().isOk()).andExpect(view().name("login"));
  }

  @Test
  public void homeURLCanBeAccessed() throws Exception {
    mockMvc.perform(get("/home")).andExpect(status().isOk()).andExpect(view().name("home"));
  }

  @Test
  public void dashboardURLCanBeAccessed() throws Exception {
    mockMvc.perform(get("/projects/1")).andExpect(status().isOk());
  }

  @Test
  public void addProjectURLCanBeAccessed() throws Exception {
    mockMvc.perform(get("/add-project")).andExpect(status().isOk());
  }

  @Test
  public void editProjectURLCanBeAccessed() throws Exception {
    mockMvc.perform(get("/projects/1/edit")).andExpect(status().isOk());
  }

  @Test
  public void projectLogURLCanBeAccessed() throws Exception {
    mockMvc.perform(get("/projects/1/log")).andExpect(status().isOk());
  }

  @Test
  public void projectContributorURLCanBeAccessed() throws Exception {
    mockMvc.perform(get("/projects/1/contributor")).andExpect(status().isOk());
  }

  @Test
  public void projectRoleURLCanBeAccessed() throws Exception {
    mockMvc.perform(get("/projects/1/role")).andExpect(status().isOk());
  }

  @Test
  public void categoryAddURLCanBeAccessed() throws Exception {
    mockMvc.perform(get("/projects/1/add-category")).andExpect(status().isOk());
  }

  @Test
  public void categoryListURLCanBeAccessed() throws Exception {
    mockMvc.perform(get("/projects/1/category-list")).andExpect(status().isOk());
  }

  @Test
  public void categoryEditURLCanBeAccessed() throws Exception {
    mockMvc.perform(get("/projects/1/categories/1/edit")).andExpect(status().isOk());
  }

  @Test
  public void transactionAddURLCanBeAccessed() throws Exception {
    mockMvc.perform(get("/projects/1/add-transaction")).andExpect(status().isOk());
  }

  @Test
  public void transactionEditURLCanBeAccessed() throws Exception {
    mockMvc.perform(get("/projects/1/transactions/1/edit")).andExpect(status().isOk());
  }

  @Test
  public void page403URLCanBeAccessed() throws Exception {
    mockMvc.perform(get("/403")).andExpect(status().isOk());
  }

  @Test
  public void page404URLCanBeAccessed() throws Exception {
    mockMvc.perform(get("/404")).andExpect(status().isOk());
  }
}
