package com.adprog.webapp;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class WebappIntegrationTests {

  @Test
  void contextLoads() {
    WebappApplication.main(new String[] {});
  }
}
