/*jslint browser */
/*global window */
/*global $*/
$(document).ready(function () {
    "use strict";
    // Link: http://financial-planner-adpro.herokuapp.com/projects/{projectId}/xyz
    var paths = window.location.href.toString().split("/");
    var pid = paths[paths.length - 2];
    var authToken = window.localStorage.getItem("Authorization");
    $("#submit").on("click", function () {
        var name = document.getElementById("cname").value;
        var description = document.getElementById("cdesc").value;
        var type = document.getElementById("ctype").value;
        var data = {
            "name": name,
            "description": description,
            "type": type,
            "project": pid
        };
        $.ajax({
            type: "POST",
            url: "http://financial-planner-datastorage0.herokuapp.com/api/projects/" + pid + "/categories/",
            dataType: "json",
            async: true,
            crossDomain: true,
            data: data,
            contentType: "application/json; charset=UTF-8",
            headers: {
                "Authorization": authToken
            },
            success: function (result) {
                window.alert("Kategori " + name + " berhasil dibuat.");
                window.console.log(result);
            },
            error: function (xhr, resp, text) {
                window.alert("ERROR: Kategori " + name + " gagal dibuat.");
                window.console.log(xhr, resp, text);
            }
        });
    });
});