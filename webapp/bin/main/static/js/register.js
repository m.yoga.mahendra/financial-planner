/*jslint browser */
/*global window */
/*global $*/
$(document).ready(function () {
    "use strict";
    $("#submit").on("click", function () {
        var uname = document.getElementById("username").value;
        var email = document.getElementById("email").value;
        var fullname = document.getElementById("fullname").value;
        var pwd = document.getElementById("password").value;
        var confpwd = document.getElementById("confpass").value;
        var data = {
            "username": uname,
            "email": email,
            "fullName": fullname,
            "password": pwd,
            "matchingPassword": confpwd
        };
        $.ajax({
            type: "POST",
            url: "http://financial-planner-datastorage.herokuapp.com/api/users",
            dataType: "text",
            async: true,
            crossDomain: true,
            data: JSON.stringify(data),
            contentType: "application/json; charset=UTF-8",
            headers: {
                "Access-Control-Expose-Headers": "Authorization"
            },
            statusCode: {
                "200": function (data, result, ignore) {
                    window.alert("Register Successful");
                    window.console.log(result);
                    window.location.replace("/login");
                },
                "401": function (xhr, resp, text) {
                    window.alert("Register Failed because: " + resp.message);
                    window.console.log(xhr, resp, text);
                }
            }
        });
    });
});