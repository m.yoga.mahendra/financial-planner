/*jslint browser */
/*global window */
/*global $*/
$(document).ready(function () {
    "use strict";
    // Link: http://financial-planner-adpro.herokuapp.com/projects/{projectId}
    var paths = window.location.href.toString().split("/");
    var pid = paths[paths.length - 1];
    var authToken = window.localStorage.getItem("Authorization");
    var body = {"type": "pie"};
    $.ajax({
        type: "GET",
        url: "http://financial-planner-analytics.herokuapp.com/api/" + pid + "/graph/categories",
        async: true,
        crossDomain: true,
        headers: {
            "Authorization": authToken
        },
        data: body,
        success: function (data) {
            window.alert("halo ali eva");
            $.each(data, function (key, value) {
                document.getElementById("piechart").setAttribute(
                    "src",
                    "data:image/jpeg;base64," + value.imageB64
                );
                // var chart = document.createElement("img");
                // chart.src = "data:image/jpeg;base64," + value.imageB64;
                // $("#piechart").append(chart);
                // $("#piechart").text("Halo");

                // or use this: document.querySelector('#piechart').innerHTML = chart.outerHTML;
            });
        },
        error: function (xhr, resp, text) {
            window.alert("ERROR: Gagal mendapatkan Pie Chart.");
            window.console.log(xhr, resp, text);
        }
    });
});