/*jslint browser */
/*global window */
/*global $*/
$(document).ready(function () {
    "use strict";
    // Link: http://financial-planner-adpro.herokuapp.com/projects/{projectId}/xyz
    var paths = window.location.href.toString().split("/");
    var pid = paths[paths.length - 2];
    var authToken = window.localStorage.getItem("Authorization");
    $.ajax({
        method: "GET",
        url: "http://financial-planner-datastorage.herokuapp.com/api/projects/" + pid + "/roles/",
        async: true,
        crossDomain: true,
        headers: {
            "Authorization": authToken
        },
        success: function (data) {
            $.each(data, function (key, value) {
                var entry = document.createElement("li");
                entry.setAttribute("class", "list-group-item");
                entry.innerHTML(value.name);

                $("#role-list").append(entry);
            });
        },
        error: function (xhr, resp, text) {
            window.alert("Tidak bisa mengakses daftar roles.");
            window.console.log(xhr, resp, text);
        }
    });
});