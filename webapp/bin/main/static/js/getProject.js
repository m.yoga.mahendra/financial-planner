/*jslint browser */
/*global window */
/*global $*/
$(document).ready(function () {
    "use strict";
    var authToken = window.localStorage.getItem("Authorization");
    $.ajax({
        method: "GET",
        url: "http://financial-planner-datastorage.herokuapp.com/api/projects/",
        async: true,
        crossDomain: true,
        headers: {
            "Authorization": authToken
        },
        success: function (data) {
            var json = JSON.parse(data);
            $.each(json, function (i, value) { // Belum diedit dari getTransactions
                var entry = document.createElement("li");
                entry.setAttribute("class", "list-group-item");

                var titleEntry = document.createElement("div");
                titleEntry.setAttribute("class", "trans");
                titleEntry.innerHTML = value.title;
                $(entry).append(titleEntry);

                var amountEntry = document.createElement("div");
                amountEntry.setAttribute("class", "amount");
                amountEntry.innerHTML = value.amount;
                $(entry).append(amountEntry);

                window.console.log("(" + value.title + ", " + value.amount + ")");

                $("#transaction-list").append(entry);
            });
        },
        error: function (xhr, resp, text) {
            window.alert("Tidak bisa mengakses daftar proyek.");
            window.console.log(xhr, resp, text);
        }
    });
});