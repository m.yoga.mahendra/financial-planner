/*jslint browser */
/*global window */
/*global $*/
$(document).ready(function () {
    "use strict";
    // Link: http://financial-planner-adpro.herokuapp.com/projects/{projectId}/xyz
    var paths = window.location.href.toString().split("/");
    var pid = paths[paths.length - 2];
    var authToken = window.localStorage.getItem("Authorization");
    loadCategory (pid, authToken);

    $("#submit").on("click", function () {
        var title = document.getElementById("tname").value;
        var description = document.getElementById("tdesc").value;
        var amount = document.getElementById("amount").value;
        var transdate = document.getElementById("tdate").value;
        var category = document.getElementById("category").value;
        var data = {
            "title": title,
            "description": description,
            "amount": amount,
            "transactionDate": transdate,
            "project": pid,
            "category": category
        };
        console.log(data);
        $.ajax({
            type: "POST",
            url: "http://financial-planner-datastorage.herokuapp.com/api/projects/" + pid + "/transactions/",
            dataType: "json",
            async: true,
            crossDomain: true,
            data: JSON.stringify(data),
            contentType: "application/json; charset=UTF-8",
            headers: {
                "Authorization": authToken
            },
            success: function (result) {
                window.alert("Transaksi berhasil ditambahkan");
                window.console.log(result);
            },
            error: function (xhr, resp, text) {
                window.alert("ERROR: Transaksi gagal ditambahkan");
                window.console.log(xhr, resp, text);
            }
        });
    });
});

function loadCategory (projectId, authToken) {
    $.ajax({
        method: "GET",
        url: "http://financial-planner-datastorage.herokuapp.com/api/projects/" + projectId + "/categories/",
        async: true,
        crossDomain: true,
        headers: {
            "Authorization": authToken
        },
        success: function (data) {
            var json = JSON.parse(data);
            $.each(json, function (key, value) {
                var option = document.createElement("option");
                option.innerHTML = value.name;
                option.setAttribute("value", value.id);
                $("#category").append(option);
            });
        },
        error: function (xhr, resp, text) {
            window.alert("Tidak bisa mengakses daftar transaksi.");
            window.console.log(xhr, resp, text);
        }
    });
}