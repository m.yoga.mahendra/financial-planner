/*jslint browser */
/*global window */
/*global $*/
$(document).ready(function () {
    "use strict";
    // Link: http://financial-planner-adpro.herokuapp.com/projects/{projectId}/xyz
    var paths = window.location.href.toString().split("/");
    var pid = paths[paths.length - 2];
    var authToken = window.localStorage.getItem("Authorization");
    $("#cat_del").on("click", function () {
        var cid = document.getElementById("cat_id").value;
        $.ajax({
            type: "DELETE",
            url: "http://financial-planner-datastorage.herokuapp.com/api/projects/" + pid + "/categories/" + cid,
            dataType: "json",
            async: true,
            crossDomain: true,
            contentType: "application/json; charset=UTF-8",
            headers: {
                "Authorization": authToken
            },
            success: function (result) {
                window.alert("Kategori berhasil dihapus.");
                window.console.log(result);
            },
            error: function (xhr, resp, text) {
                window.alert("ERROR: Kategori gagal dihapus.");
                window.console.log(xhr, resp, text);
            }
        });
    });
});