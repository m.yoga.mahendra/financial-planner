/*jslint browser */
/*global window */
/*global $*/
$(document).ready(function () {
    "use strict";
    // Link: http://financial-planner-adpro.herokuapp.com/projects/{projectId}/contributor
    var paths = window.location.href.toString().split("/");
    var pid = paths[paths.length - 2];
    var authToken = window.localStorage.getItem("Authorization");
    fetch_roles(pid, authToken);
});

function fetch_roles (projectId, authToken) {
    $.ajax({
        method: "GET",
        url: "http://financial-planner-datastorage.herokuapp.com/api/projects/" + projectId + "/roles/",
        async: true,
        crossDomain: true,
        headers: {
            "Authorization": authToken
        },
        success: function (data) {
            json = JSON.parse(data);

            $.each(json, function (i, value) {
                // Add role to role-list
                var entry = document.createElement("li");
                entry.setAttribute("class", "list-group-item");
                entry.innerHTML = value.name;
                $("#role-list").append(entry);

                fetch_user_role(projectId, value.id, value.name, authToken);
            });
        },
        error: function (xhr, resp, text) {
            window.alert("Tidak bisa mengakses daftar roles.");
            window.console.log(xhr, resp, text);
        }
    });
}

function fetch_user_role(projectId, roleId, roleName, authToken) {
    $.ajax({
        method: "GET",
        url: "http://financial-planner-datastorage.herokuapp.com/api/projects/" + projectId + "/roles/" + roleId + "/users",
        async: true,
        crossDomain: true,
        headers: {
            "Authorization": authToken
        },
        success: function (data) {
            json = JSON.parse(data);
            $.each(json, function (key, value) {

                var row1 = document.createElement("li");
                row1.setAttribute("class", "list-group-item");

                var row2 = document.createElement("div");
                row2.setAttribute("class", "flex-container");

                var row21 = document.createElement("div");
                row21.setAttribute("style", "flex-grow: 1");
                row21.innerHTML = value.fullName;

                var row22 = document.createElement("div");
                row22.setAttribute("style", "flex-grow: 8");
                row22.innerHTML = ":";

                var row23 = document.createElement("div");
                row23.setAttribute("style", "flex-grow: 1");
                row23.innerHTML = roleName;

                $(row2).append(row21);
                $(row2).append(row22);
                $(row2).append(row23);

                $(row1).append(row2);

                $("#user-list").append(row1);
            });
        },
        error: function (xhr, resp, text) {
            window.alert("ERROR: Tidak bisa mengakses daftar roles.");
            window.console.log(xhr, resp, text);
        }
    });
}