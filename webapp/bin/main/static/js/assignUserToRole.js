/*jslint browser */
/*global window */
/*global $*/
$(document).ready(function () {
    "use strict";
    // Link: http://financial-planner-adpro.herokuapp.com/projects/{projectId}/roles/roleid/users
    var paths = window.location.href.toString().split("/");
    var pid = paths[paths.length - 4];
    var rid = paths[paths.length - 2];
    var authToken = window.localStorage.getItem("Authorization");
    $("#assignuser").on("click", function () {
        var data = {
            "name": name
        };
        $.ajax({
            type: "POST",
            url: "http://financial-planner-datastorage0.herokuapp.com/api/projects/" + pid + "/roles/" + rid + "/users",
            dataType: "json",
            async: true,
            crossDomain: true,
            data: data,
            contentType: "application/json; charset=UTF-8",
            headers: {
                "Authorization": authToken
            },
            success: function (result) {
                window.alert("User " + name + " berhasil ditambahkan ke role");
                window.console.log(result);
            },
            error: function (xhr, resp, text) {
                window.alert("ERROR: User " + name + " gagal ditambahkan ke role.");
                window.console.log(xhr, resp, text);
            }
        });
    });
});