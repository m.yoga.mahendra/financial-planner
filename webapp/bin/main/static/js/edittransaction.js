/*jslint browser */
/*global window */
/*global $*/
$(document).ready(function () {
    "use strict";
    // Link: http://financial-planner-adpro.herokuapp.com/projects/{projectId}/transactions/{transId}
    var paths = window.location.href.toString().split("/");
    var pid = paths[paths.length - 3];
    var tid = paths[paths.length - 1];
    var authToken = window.localStorage.getItem("Authorization");
    $.ajax({
        method: "GET",
        url: "http://financial-planner-datastorage.herokuapp.com/api/projects/" + pid + "/categories/",
        async: true,
        crossDomain: true,
        headers: {
            "Authorization": authToken
        },
        success: function (data) {
            $.each(data, function (key, value) {
                var option = document.createElement("option");
                option.text = value.name;
                option.value = value.id;
                $("#categorychoice").append(option);
            });
        },
        error: function (xhr, resp, text) {
            window.alert("ERROR: Tidak bisa mengakses daftar kategori.");
            window.console.log(xhr, resp, text);
        }
    });
    $("#submit").on("click", function () {
        var title = document.getElementById("tname").value;
        var description = document.getElementById("tdesc").value;
        var amount = document.getElementById("amount").value;
        var transdate = document.getElementById("tdate").value;
        var category = document.getElementById("ctrans").value;
        var data = {};
        if (title !== "") {
            data.title = title;
        }
        if (description !== "") {
            data.description = description;
        }
        if (amount !== "") {
            data.amount = amount;
        }
        if (transdate !== "") {
            data.transactionDate = transdate;
        }
        if (category !== "") {
            data.category = category;
        }
        $.ajax({
            type: "PATCH",
            url: "http://financial-planner-datastorage0.herokuapp.com/api/projects/" + pid + "/transactions/" + tid,
            dataType: "json",
            async: true,
            crossDomain: true,
            data: data,
            contentType: "application/json; charset=UTF-8",
            headers: {
                "Authorization": authToken
            },
            success: function (result) {
                window.alert("Transaksi " + name + " berhasil diupdate.");
                window.console.log(result);
            },
            error: function (xhr, resp, text) {
                window.alert("ERROR: Transaksi " + name + " gagal diupdate.");
                window.console.log(xhr, resp, text);
            }
        });
    });
});