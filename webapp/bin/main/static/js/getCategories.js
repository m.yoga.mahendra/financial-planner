/*jslint browser */
/*global window */
/*global $*/
$(document).ready(function () {
    "use strict";
    // Link: http://financial-planner-adpro.herokuapp.com/projects/{projectId}/log
    var paths = window.location.href.toString().split("/");
    var pid = paths[paths.length - 2];
    var authToken = window.localStorage.getItem("Authorization");
    $.ajax({
        method: "GET",
        url: "http://financial-planner-datastorage.herokuapp.com/api/projects/" + pid + "/categories/",
        async: true,
        crossDomain: true,
        headers: {
            "Authorization": authToken
        },
        success: function (data) {
            var json = JSON.parse(data);
            $.each(json, function (i, value) {
                var outer = document.createElement("div");
                outer.setAttribute("class", "checkbox");

                var entry = document.createElement("label");
                entry.innerHTML = value.name + " ";

                var input = document.createElement("input");
                input.setAttribute("type", "checkbox");
                input.setAttribute("name", "categories");
                input.setAttribute("value", value.id);

                $(entry).append(input);
                $(outer).append(entry);
                $("#category-list").prepend(outer);
            });
        },
        error: function (xhr, resp, text) {
            window.alert("ERROR: Tidak bisa mengakses daftar kategori.");
            window.console.log(xhr, resp, text);
        }
    });
});