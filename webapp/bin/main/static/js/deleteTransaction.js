/*jslint browser */
/*global window */
/*global $*/
$(document).ready(function () {
    "use strict";
    // Link: http://financial-planner-adpro.herokuapp.com/projects/{projectId}/xyz
    var paths = window.location.href.toString().split("/");
    var pid = paths[paths.length - 2];
    var authToken = window.localStorage.getItem("Authorization");
    $("#trans_del").on("click", function () {
        var tid = document.getElementById("trans_id").value;
        $.ajax({
            type: "DELETE",
            url: "http://financial-planner-datastorage.herokuapp.com/api/projects/" + pid + "/transactions/" + tid,
            dataType: "json",
            async: true,
            crossDomain: true,
            contentType: "application/json; charset=UTF-8",
            headers: {
                "Authorization": authToken
            },
            success: function (result) {
                window.alert("Transaksi berhasil dihapus.");
                window.console.log(result);
            },
            error: function (xhr, resp, text) {
                window.alert("ERROR: Transaksi gagal dihapus.");
                window.console.log(xhr, resp, text);
            }
        });
    });
});