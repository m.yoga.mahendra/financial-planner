/*jslint browser */
/*global window */
/*global $*/
$(document).ready(function () {
    "use strict";
    // Link: http://financial-planner-adpro.herokuapp.com/projects/{projectId}/xyz
    var paths = window.location.href.toString().split("/");
    var pid = paths[paths.length - 2];
    var authToken = window.localStorage.getItem("Authorization");
    $("#createroles").on("click", function () {
        var name = document.getElementById("rname").value;
        var description = document.getElementById("rdesc").value;
        var authorities = [];
        var create = document.getElementById("post").value;
        var update = document.getElementById("patch").value;
        var delet = document.getElementById("delete").value;
        var read = document.getElementById("read").value;
        var roles = document.getElementById("roles").value;
        var data = {
            "project": pid,
            "name": name,
            "description": description
        };
        if (read !== "") {
            authorities.push("GET");
        }
        if (create !== "") {
            authorities.push("POST");
        }
        if (update !== "") {
            authorities.push("PATCH");
        }
        if (delet !== "") {
            authorities.push("DELETE");
        }
        if (roles !== "") {
            authorities.push("ROLES");
        }
        data.authorities = authorities;
        $.ajax({
            type: "POST",
            url: "http://financial-planner-datastorage.herokuapp.com/api/projects/" + pid + "/roles",
            dataType: "json",
            async: true,
            crossDomain: true,
            data: data,
            contentType: "application/json; charset=UTF-8",
            headers: {
                "Authorization": authToken
            },
            success: function (result) {
                window.alert("Role " + name + " berhasil dibuat.");
                window.console.log(result);
            },
            error: function (xhr, resp, text) {
                window.alert("ERROR: Role " + name + " gagal dibuat.");
                window.console.log(xhr, resp, text);
            }
        });
    });
});