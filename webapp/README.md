# Finance Planner Web Application

Link: (http://financial-planner-adpro.herokuapp.com)

This module handles everything about our web application. Currently it is being run using Spring Boot Framework.

Main contributor:
1. Audilla Putri Ferialdi (1806186755)
2. Muhammad Destara Syanandi (1806186856)